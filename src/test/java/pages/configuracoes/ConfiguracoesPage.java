package pages.configuracoes;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.GeralPage;

public class ConfiguracoesPage extends GeralPage {

    @FindBy(id = "Mais opções")
    public MobileElement btnMaisOpcoes;
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout")
    public MobileElement btnSair;
    @FindBy(id = "button1")
    public MobileElement btnConfirmarSair;
    @FindBy(id = "button2")
    public MobileElement btnCancelarSair;
    @FindBy(id = "full_name")
    public MobileElement labelNomeCliente;
    @FindBy(id = "current_active_line")
    public MobileElement labelLinhaCliente;

    public ConfiguracoesPage(AndroidDriver<MobileElement> driver1) {
        driver = driver1;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void clicarBtnMaisOpcoes() {
        btnMaisOpcoes.click();
    }

    public void clicarBtnSair() {
        btnSair.click();
    }

    public void clicarBtnConfirmarSair() {
        btnConfirmarSair.click();
    }

    public void clicarBtnCancelarSair() {
        btnCancelarSair.click();
    }

    public void validaLinhaCliente() {
        esperaElementoFicarVisivel(labelNomeCliente);
        esperaElementoFicarVisivel(labelLinhaCliente);
    }

}
