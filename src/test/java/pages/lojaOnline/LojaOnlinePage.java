package pages.lojaOnline;

import api.model.Pessoa;
import com.sun.org.apache.bcel.internal.generic.NEW;
import enums.CONTEXTO;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import io.appium.java_client.touch.ActionOptions;
import io.appium.java_client.touch.TapOptions;
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;

import java.util.List;

import static reporter.Reporter.addScreenCapture;
import static utils.MobileWebDriver.setContext;

public class LojaOnlinePage extends GeralPage {

    public LojaOnlinePage(AndroidDriver<MobileElement> driver) {

        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    @FindBy(id = "yescartdelete")
    public MobileElement btnContinuarCarrinho;

    //@FindBy(css = "#address_to_select > div:nth-child(1) > div:nth-child(3)")

    @FindBy(id = "address_to_select")
    public MobileElement selecionandoEndereco;

    @FindBy(id = "Serviços")
    public MobileElement btnServicos;

    @FindBy(id = "Navegar para cima")
    public MobileElement btnVoltar;

    @FindBy(id = "autoTestProduct1")
    public  MobileElement btnVerDetalhes;

    @FindBy(xpath = "//a[contains(text(), 'Comprar')]")
    public WebElement btnComprar;

    @FindBy(id = "autoTestProduct2")
    public MobileElement btnVerDetalhes2;

    public void validarTelaPlanoControle() {

        MobileElement textoPlano = buscaElementoPorClassETextPartial("android.view.View", "Vivo controle");
        Assert.assertTrue(textoPlano.isDisplayed());
        System.out.println("Plano controle confirmado");
        btnVoltar.click();
    }

    public void selecionarPlanoPos() {

        MobileElement btnContinuarNovoPlano = buscaElementoPorClassETextPartial("android.widget.Button", "Continuar");
        btnContinuarNovoPlano.click();
        System.out.println("Cliquei no botão");
    }

    public void validarTelaPlanoPrePago() {

        MobileElement btnDetalhePlano = buscaElementoPorClassETextPartial("android.view.View", "Detalhe do plano");
        btnDetalhePlano.click();

        MobileElement txtMeuPlano = buscaElementoPorClassETextPartial("android.view.View", "Meu plano");
        Assert.assertEquals(txtMeuPlano.getText(),"Meu plano");

        MobileElement txtPre = buscaElementoPorClassETextPartial("android.view.View", "PRÉ");
        Assert.assertTrue(txtPre.isDisplayed());
    }

    public void selecionarPlanoControle() {

        System.out.print("Funcionalidade não implementada");

//        arrastarParaBaixo();
//        MobileElement btnAssineja = buscaElementoPorClassETextPartial("android.widget.Button", "Assine já");
//        btnAssineja.click();
    }

//    public void validarTelaDeConclusaoComAquisicaoPendente() {
//
//
//    }

    public void concluirAquisicao() {

        System.out.println("MÉTODO NÃO IMPLEMENTADO");
    }

    public void fecharAplicativo() {

        System.out.println("MÉTODO NÃO IMPLEMENTADO");
    }

    public void reabrindoOAplicativo() {

        System.out.println("MÉTODO NÃO IMPLEMENTADO");
    }

    public void validoQueOPlanoContinuaNoCarrinho() {

        System.out.println("MÉTODO NÃO IMPLEMENTADO");
    }

    public void confirmarTrocarPlano() {

        setContext(CONTEXTO.WEBVIEW);

        esperaElementoFicarVisivel(btnContinuarCarrinho);
        btnContinuarCarrinho.click();

        setContext(CONTEXTO.NATIVE_APP);

    }

    public void confirmoDadosCompra() {

        MobileElement resumoCompra = buscaElementoPorClassEText("android.view.View", "Resumo da Compra");
        resumoCompra.click();
    }

    public void validarResumoCompra() {

        MobileElement planoPos = buscaElementoPorClassETextPartial("android.view.View", "VIVO_POS");
        Assert.assertTrue(planoPos.isDisplayed());
        System.out.println("Validei a tela");
    }

    public void selecionandoEndereco() {

        esperaElementoFicarVisivel(selecionandoEndereco);
        List<MobileElement> list = selecionandoEndereco.findElementsByClassName("android.view.View");
        MobileElement icone = null;
        for (MobileElement element : list) {
            System.out.println(element.getText());
            if (element.getText().length() == 0) {
                icone = element;
            }
        }

        assert icone != null;
        icone.click();


    }

    public void clicarContinuar() {

        MobileElement btnContinuar = buscaElementoPorClassETextPartial("android.widget.Button", "Continuar");
        btnContinuar.click();
        System.out.println("Cliquei no botão");
    }

    public void concluirCompra() {

        MobileElement btnConcluir = buscaElementoPorClassETextPartial("android.view.View", "Concluir compra");
        btnConcluir.click();
        System.out.println("Cliquei no botão");
    }

    public void clicarBotaoVoltar() {

        btnVoltar.click();
    }

    public void digitaDataNascimento() {

        MobileElement element = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[3]/android.view.View[8]/android.widget.EditText"));
        element.sendKeys("10/10/1990");

    }


    public void realizadoCadastroNaLojaVirtual() {

        System.out.println(driver.getContext());
//        setContext(CONTEXTO.WEBVIEW);
//
//        WebElement btnSelecionarEndereco;
//        esperaElementoFicarVisivel(btnSelecionarEndereco);
//        btnSelecionarEndereco.click();
//
//        setContext(CONTEXTO.NATIVE_APP);
    }

    public void confirmarEscolhaDoPlano() {

        MobileElement btnSim = buscaElementoPorClassETextPartial("android.widget.Button", "SIM");
        btnSim.click();
    }

    public void clicoEmTrocardeAparelho() {

        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        scrollDown();
        MobileElement btnSim = buscaElementoPorClassETextPartial("android.view.View", "Troca de aparelho");
        btnSim.click();

    }

    public void clicoEmVerDetalhes() {

        esperaElementoFicarClicavel(btnVerDetalhes);
        btnVerDetalhes.click();

    }




    public void clicoEmComprar() {

        setContext(CONTEXTO.WEBVIEW);
        moverAteElementoWeb(btnComprar);
        btnComprar.click();
    }

//    public void clicoEmComprar() {
//
//        meioScrollDown();
//
//        System.out.println(driver.getContext());
//        setContext(CONTEXTO.WEBVIEW);
//
//        WebElement btnComprar;
//        esperaElementoFicarVisivel(btnComprar);
//        btnComprar.click();
//
//        setContext(CONTEXTO.NATIVE_APP);
//    }
}
