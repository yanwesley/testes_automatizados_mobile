package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class TabMenuPage extends GeralPage {

    @FindBy(id = "navigation_account")
    public WebElement btnMeuPlano;

    @FindBy(id = "navigation_explore")
    public WebElement btnDescubra;

    @FindBy(id = "br.com.vivo.enterprise:id/navigation_assistant")
    public WebElement btnAura;

    @FindBy(id = "navigation_conversations")
    public WebElement btnConversas;

    @FindBy(id = "navigation_settings")
    public WebElement btnConfiguracoes;

    @FindBy(id = "action_reload")
    public WebElement btnReload;

    @FindBy(css = "#sourceContainer > ul > li > ul > li.ant-tree-treenode-switcher-open > ul > li > ul > li > ul > li > ul > li.ant-tree-treenode-switcher-open > ul > li:nth-child(1) > ul > li.ant-tree-treenode-switcher-close > span.ant-tree-node-content-wrapper.ant-tree-node-content-wrapper-normal > span")
    public WebElement textTitle;

    @FindBy(id = "button1")
    public MobileElement btnAceitarAgenda;

    @FindBy(id = "data")
    public MobileElement dataAtualizacao;

    public TabMenuPage(AndroidDriver<MobileElement> driver1) {
        driver = driver1;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void clicarBtnMeuPlano() {
        btnMeuPlano.click();
    }

    public void clicarBtnDescubra() {
        btnDescubra.click();
    }

    public void clicarBtnAura() {
        esperaElementoFicarClicavel(btnAura);
        btnAura.click();
    }

    public void clicarBtnConversas() {
        btnConversas.click();
    }

    public void clicarBtnConfiguracoes() {
        btnConfiguracoes.click();
    }

    public void clicarBtnReload() {
        btnReload.click();
    }

    public String getTitle() {
        return textTitle.getText();
    }

    public void validaPage() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(btnAceitarAgenda));
            btnAceitarAgenda.click();
        } catch (Exception e) {
        }
        try {
            esperaElementoFicarVisivel(dataAtualizacao);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        Assert.assertTrue(btnMeuPlano.isDisplayed() &&
                btnDescubra.isDisplayed() &&
                btnConversas.isDisplayed() &&
                btnConfiguracoes.isDisplayed());
    }

}

