package pages;


// Page espec�fica apenas dos m�todos de login extendendo os m�todos que foram criados na page geral

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage extends GeralPage {

    @FindBy(id = "start_now")
    public MobileElement btnComecarAgora;

    @FindBy(id = "linha")
    public MobileElement inputLinha;

    @FindBy(id = "btSubmit")
    public MobileElement btnContinuar;

    @FindBy(id = "otp")
    public MobileElement inputOpt;

    public LoginPage(AndroidDriver<MobileElement> driver1) {
        driver = driver1;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void clicarBtnComecarAgora() {
        btnComecarAgora.click();
    }

    public void validarTelaWelcome() {

        Assert.assertTrue(btnComecarAgora.isDisplayed());
    }

    public void validarTelaDeIdentificacao() {
        Assert.assertTrue(btnContinuar.isDisplayed() && inputLinha.isDisplayed());
    }

    public void preencherNumeroLinha(String linha) {
        inputLinha.sendKeys(linha);
    }

    public void clicarBtnContinuar() {
        btnContinuar.click();
    }

    public void validaTelaDeSenha() {
        Assert.assertTrue(inputOpt.isDisplayed());
    }

    public void preencherSenha(String senha) {
        inputOpt.sendKeys(senha);
    }

}