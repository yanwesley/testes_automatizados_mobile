package pages;

import enums.DIRECTION;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.List;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;

public class GeralPage {
    private static final int TIME_WAIT = 400;
    public AndroidDriver<MobileElement> driver;

    public MobileElement buscaElementoPorClassEText(String classe, String textoDoElemento) {
        Wait<AndroidDriver<MobileElement>> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(TIME_WAIT))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);

        return wait.until((driver1) -> {
            List<MobileElement> list = driver1.findElementsByClassName(classe);
            MobileElement elementAux = null;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getText().equals(textoDoElemento)) {
                    elementAux = (list.get(i));
                    return elementAux;
                }
            }
            return elementAux;
        });

    }

    public void moverAteElemento(MobileElement element) {

        Actions actions = new Actions(driver);
        actions.moveToElement(element, 10, 10);
        actions.perform();
    }

    public MobileElement buscaElementoPorClassETextPartial(String classe, String textoDoElemento) {
        Wait<AndroidDriver<MobileElement>> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(TIME_WAIT))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);

        return wait.until((driver1) -> {
            List<MobileElement> list = driver1.findElementsByClassName(classe);
            MobileElement elementAux = null;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getText().contains(textoDoElemento)) {
                    elementAux = (list.get(i));
                    return elementAux;
                }
            }
            return elementAux;
        });

    }

    public MobileElement buscaElementoPorClassETextPartial(String classe, String textoDoElemento1, String textoElemento2) {
        Wait<AndroidDriver<MobileElement>> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(TIME_WAIT))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);

        return wait.until((driver1) -> {
            List<MobileElement> list = driver1.findElementsByClassName(classe);
            MobileElement elementAux = null;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getText().contains(textoDoElemento1))
                    if (list.get(i).getText().contains(textoElemento2)) {
                        elementAux = (list.get(i));
                        return elementAux;
                    }
            }
            return elementAux;
        });
    }

    public MobileElement validaTextoEmDoisElementosPorClasse(String classe, String textoDoElemento1, String textoElemento2) {
        Wait<AndroidDriver<MobileElement>> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(TIME_WAIT))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);

        return wait.until((driver1) -> {
            List<MobileElement> list = driver1.findElementsByClassName(classe);
            MobileElement elementAux = null;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getText().contains(textoDoElemento1))
                    if (list.get(i + 1).getText().contains(textoElemento2)) {
                        elementAux = (list.get(i));
                        return elementAux;
                    }
            }
            return elementAux;
        });
    }


    public int buscaQuantidadeDeElementoPorClassETextPartial(String classe, String textoDoElemento) {

        List<MobileElement> list = driver.findElementsByClassName(classe);

        int cont = 0;

        for (MobileElement element : list) {
            if (element.getText().contains(textoDoElemento)) {
                cont++;
            }
        }

        return cont;

    }


    public void clickBtnFabPdfEdit() {
        MobileElement btn = driver.findElement(By.id("edit_fab"));
        btn.click();
    }

    public void btnFabPdfEditIsDisplayed() {
        MobileElement btn = driver.findElement(By.id("edit_fab"));
        esperaElementoFicarClicavel(btn);
    }

    public void validarBtnAdicionarAoDrive() {
        MobileElement btnAdicionarAoDrive = driver.findElement(By.id("Adicionar ao Drive"));
        esperaElementoFicarClicavel(btnAdicionarAoDrive);
        try {
            Thread.sleep(3000);
            scrollDown();
            scrollDown();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void validarBtnAdicionarAoDriveSemScroll() {
        MobileElement btnAdicionarAoDrive = driver.findElement(By.id("Adicionar ao Drive"));
        esperaElementoFicarClicavel(btnAdicionarAoDrive);
        try {
            Thread.sleep(3000);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    public void scroll(double inicio, double fim) {
        Dimension size = driver.manage().window().getSize();
        long duration = 200;

        int x = size.width / 2;

        int start_y = (int) (size.height * inicio);
        int end_y = (int) (size.height * fim);
        new TouchAction(driver).press(point(x, start_y))
                .waitAction(waitOptions(Duration.ofMillis(duration)))
                .moveTo(point(x, end_y))
                .release()
                .perform();
    }

    public void scrollDown() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        scroll(0.8, 0.2);
    }

    public void meioScrollDown() {
        scroll(0.7, 0.4);
    }

    public void scrollUp() {
        scroll(0.5, 0.9);
    }

    public void swipe(DIRECTION direction) {
        Dimension size = driver.manage().window().getSize();
        long duration = 200;

        int startX = 0;
        int endX = 0;
        int startY = 0;
        int endY = 0;

        switch (direction) {
            case RIGHT:
                startY = size.height / 2;
                startX = (int) (size.width * 0.70);
                endX = (int) (size.width * 0.05);
                new TouchAction(driver)
                        .press(point(startX, startY))
                        .waitAction(waitOptions(Duration.ofMillis(duration)))
                        .moveTo(point(endX, startY))
                        .release()
                        .perform();
                break;

            case LEFT:
                startY = size.height / 2;
                startX = (int) (size.width * 0.05);
                endX = (int) (size.width * 0.90);
                new TouchAction(driver)
                        .press(point(startX, startY))
                        .waitAction(waitOptions(Duration.ofMillis(duration)))
                        .moveTo(point(endX, startY))
                        .release()
                        .perform();

                break;

            case UP:
                endY = (int) (size.height * 0.70);
                startY = (int) (size.height * 0.30);
                startX = (size.width / 2);
                new TouchAction(driver)
                        .press(point(startX, startY))
                        .waitAction(waitOptions(Duration.ofMillis(duration)))
                        .moveTo(point(endX, startY))
                        .release()
                        .perform();
                break;


            case DOWN:
                startY = (int) (size.height * 0.70);
                endY = (int) (size.height * 0.30);
                startX = (size.width / 2);
                new TouchAction(driver)
                        .press(point(startX, startY))
                        .waitAction(waitOptions(Duration.ofMillis(duration)))
                        .moveTo(point(startX, endY))
                        .release()
                        .perform();
                break;

        }
    }

    public void esperaCarregar() {
        try {
            Wait<AndroidDriver<MobileElement>> wait = new FluentWait<>(driver)
                    .withTimeout(Duration.ofSeconds(20))
                    .pollingEvery(Duration.ofSeconds(5))
                    .ignoring(NoSuchElementException.class);

            MobileElement foo = wait.until(driver -> driver.findElement(By.id("loading_progress_bar")));
            if (foo != null) {
                WebDriverWait wait2 = new WebDriverWait(driver, TIME_WAIT);
                wait2.until(ExpectedConditions.invisibilityOf(foo));
            }
        } catch (Exception e) {
        }
    }

    public MobileElement esperarElementoVisivelFluentWait(String classe, String textoDoElemento) {
        Wait<AndroidDriver<MobileElement>> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(TIME_WAIT))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);

        MobileElement foo = wait.until((driver) -> {
            MobileElement temp = buscaElementoPorClassEText(classe, textoDoElemento);
            return temp;
        });

        return foo;
    }

    public void esperaElementoFicarVisivel(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, TIME_WAIT);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void esperaElementoFicarClicavel(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, TIME_WAIT);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void esperaTextoNoElemento(MobileElement element, String texto) {
        WebDriverWait wait = new WebDriverWait(driver, TIME_WAIT);
        wait.until(ExpectedConditions.textToBePresentInElement(element, texto));
    }

    public String doisDiasParaFrente() {
        DateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        return dateFormat.format((calendar.getTime()));
    }

    public void clickPorPosicao(int x, int y) {
        TouchAction teste = new TouchAction(driver);
        teste.tap(point(x, y))
                .release()
                .perform();
    }

    public void fecharTeclado() {
        driver.hideKeyboard();
    }


    public void moverAteElementoWeb(WebElement element) {

        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void fluentComEspera(MobileElement element){

        Wait<AndroidDriver<MobileElement>> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(TIME_WAIT))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);

        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

//    public void arrastarParaBaixo() {
//
//        try{
//            Thread.sleep(58000);
//        }catch(Exception e){
//
//        }
//
//        (new TouchAction(driver)).press(PointOption.point(367, 638))
//                .moveTo(PointOption.point(367, 598))
//                .release();

}






