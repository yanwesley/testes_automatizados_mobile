package pages.meuPlano.saldo;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

public class MeuPlanoSaldoPage extends GeralPage {

    @FindBy(id = "filter")
    public MobileElement btnFiltroExtrato;

    public MeuPlanoSaldoPage(AndroidDriver<MobileElement> driver1) {
        driver = driver1;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void validaSaldoDisponivel() {
        MobileElement labelSaldo = buscaElementoPorClassETextPartial("android.view.View", "Seu saldo de recarga é de R$");
        Assert.assertTrue(labelSaldo.isEnabled());
    }

    public void validaSaldoBloqueado() {
        MobileElement labelSaldo = buscaElementoPorClassETextPartial("android.view.View", "Faça uma recarga para desbloquear seu saldo de R$");
        Assert.assertTrue(labelSaldo.isEnabled());
    }

    public void clicarEmVerExtratoDetalhado() {
        scroll(0.9, 0.7);
        MobileElement btnVerExtratoDetalhado = buscaElementoPorClassEText("android.view.View", "Ver extrato detalhado \uEA3A");
        Assert.assertTrue(btnVerExtratoDetalhado.isEnabled());
        Reporter.addScreenCapture("Cliquei para ver os Extrato detalhado");
        btnVerExtratoDetalhado.click();
    }

    public void validarTelaDeExtrato() {
        MobileElement labelExtrato = buscaElementoPorClassEText("android.view.View", "Extrato");
        Assert.assertTrue(labelExtrato.isEnabled());
        esperaElementoFicarVisivel(btnFiltroExtrato);
        Assert.assertTrue(btnFiltroExtrato.isEnabled());
        Reporter.addScreenCapture("Validando tela de Extrato");
        scrollDown();
        Reporter.addScreenCapture("Validando tela de Extrato");
        scrollDown();
        MobileElement labelSujeitoAlteracao = buscaElementoPorClassEText("android.view.View", "Sujeito a alteração até o final do dia");
        Assert.assertTrue(labelSujeitoAlteracao.isEnabled());
        Reporter.addScreenCapture("Validando tela de Extrato");
    }

    public void validarTelaDeExtratoPreComSaldoBloqueado() {
        MobileElement labelExtrato = buscaElementoPorClassEText("android.view.View", "Extrato");
        Assert.assertTrue(labelExtrato.isEnabled());
        esperaElementoFicarVisivel(btnFiltroExtrato);
        Assert.assertTrue(btnFiltroExtrato.isEnabled());
        Reporter.addScreenCapture("Validando tela de Extrato");
    }
}
