package pages.meuPlano.consumo;

import enums.DIRECTION;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

import java.util.List;

public class MeuPlanoConsumoPage extends GeralPage {

    @FindBy(id = "linhaNova")
    public MobileElement inputLinhaParaCompartilhar;

    @FindBy(id = "continue")
    public MobileElement btnContinue;

    @FindBy(id = "confirmaOk")
    public MobileElement btnConfirmarOK;

// Contexto WebView

    @FindBy(css = "#output > div.card.border-0.mb-3 > div")
    public MobileElement cardConsumoWebView;

    @FindBy(css = "#output > div.card.border-0.mb-3 > div > div > ul > li:nth-child(1)")
    public MobileElement labelValorConsumoWebView;

    @FindBy(css = "#output > div.card.border-0.mb-3 > div > div > ul > li:nth-child(2)")
    public MobileElement labelValorDisponivelWebView;

    @FindBy(id = "data_renovacao_async")
    public MobileElement btnMaisDetalhes;

    public MeuPlanoConsumoPage(AndroidDriver<MobileElement> driver1) {
        driver = driver1;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void clicarEnvieSuaInternetDisponivel() {
        swipe(DIRECTION.DOWN);
        MobileElement btnEnvieSuaInternetDisponivel = buscaElementoPorClassEText("android.view.View", "Envie sua internet disponível \uEA3A");
        Assert.assertTrue(btnEnvieSuaInternetDisponivel.isEnabled());
        Reporter.addScreenCapture("Cliquei para compartilhar");
        btnEnvieSuaInternetDisponivel.click();
    }

    public void preencherCompartilhamento(String numero) {
        String msgValidacao = "Você realmente deseja compartilhar 50 MB para " + numero + "? A validade da internet compartilhada será até às 23:59h do dia seguinte.";
        esperaElementoFicarVisivel(inputLinhaParaCompartilhar);
        inputLinhaParaCompartilhar.click();
        inputLinhaParaCompartilhar.sendKeys(numero);
        Reporter.addScreenCapture("Preenchi o numero");
        btnContinue.click();
        MobileElement labelConfirmarQauntidadeDeMB = buscaElementoPorClassETextPartial("android.view.View", "Quanto de internet para");
        Assert.assertTrue(labelConfirmarQauntidadeDeMB.isEnabled());
        Reporter.addScreenCapture("Tela para selecionar quantidade de MB");
        MobileElement option50MB = buscaElementoPorClassEText("android.view.View", "50MB \uEA3A");
        Assert.assertTrue(option50MB.isEnabled());
        Reporter.addScreenCapture("Seleciono a opção de 50MB");
        option50MB.click();
        MobileElement msgConfirmacao = buscaElementoPorClassETextPartial("android.view.View", "Você realmente deseja compartilhar 50 MB");
        Assert.assertEquals(msgConfirmacao.getText(), msgValidacao);
        Reporter.addScreenCapture("Validando tela de confirmação");
        btnConfirmarOK.click();
    }

    public void validaCompartilhamentoComSucesso(String numero) {
        String msgValidacao = "Você enviou 50 MB para o número " + numero + ". \n" +
                "A franquia será válida até amanhã as 23h59. Seu amigo receberá um SMS com a confirmação.";

        MobileElement msgConfirmacao = buscaElementoPorClassETextPartial("android.view.View", "Você enviou 50 MB para o número");
        Assert.assertEquals(msgConfirmacao.getText(), msgValidacao);
        Reporter.addScreenCapture("Validando tela de sucesso ao compartilhar.");

        MobileElement btnOkObrigado = buscaElementoPorClassEText("android.view.View", "Ok, obrigado");
        Assert.assertTrue(btnOkObrigado.isEnabled());
        btnOkObrigado.click();
    }

    public void validaExibicaoDataRenovacaoRenovaEmDdMm() {
        MobileElement labelRenovaEm = buscaElementoPorClassETextPartial("android.view.View", "Renova em");
        Assert.assertTrue(labelRenovaEm.isEnabled());
        Reporter.addScreenCapture("Validado exibição no formato : " + labelRenovaEm.getText());
    }

    public void validaConsumoMultivivo() {
        MobileElement labelMeuConsumo = buscaElementoPorClassETextPartial("android.view.View", "Meu consumo");
        Assert.assertTrue(labelMeuConsumo.isEnabled());

        MobileElement labelConsumoDosDependentes = buscaElementoPorClassETextPartial("android.view.View", "Consumo dos dependentes");
        Assert.assertTrue(labelConsumoDosDependentes.isEnabled());

        MobileElement labelDisponivel = buscaElementoPorClassETextPartial("android.view.View", "Disponível para consumo");
        Assert.assertTrue(labelDisponivel.isEnabled());

        MobileElement labelVejaADistribuicao = buscaElementoPorClassETextPartial("android.view.View", "Veja a distribuição por linha");
        Assert.assertTrue(labelVejaADistribuicao.isEnabled());
    }

    public void clicarEmDetalhamentoDoConsumoDeInternet() {
        MobileElement btnDetalhesDoConsumo = buscaElementoPorClassEText("android.view.View", "Detalhes do consumo de minha internet \uEA3A");
        Reporter.addScreenCapture("Cliquei para acessar os detalhes");
        btnDetalhesDoConsumo.click();
    }

    public void validaExibibicaoDeTooltipSemConsumo() {
        MobileElement labelTitulo = buscaElementoPorClassEText("android.view.View", "Detalhamento do consumo de internet");
        Assert.assertTrue(labelTitulo.isEnabled());

        MobileElement labelMesAtual = buscaElementoPorClassEText("android.view.View", "Mês Atual");
        Assert.assertTrue(labelMesAtual.isEnabled());

        MobileElement tooltipMesAtual = buscaElementoPorClassEText("android.view.View", "Você ainda não usou seus dados este mês");
        Assert.assertTrue(tooltipMesAtual.isEnabled());

        Reporter.addScreenCapture("Validando tooltip do mês atual");

        MobileElement labelMesAnterior = buscaElementoPorClassEText("android.view.View", "Mês Anterior");
        Assert.assertTrue(labelMesAnterior.isEnabled());
        labelMesAnterior.click();

        MobileElement tooltipMesAnterior = buscaElementoPorClassEText("android.view.View", "Você ainda não usou seus dados este mês");
        Assert.assertTrue(tooltipMesAnterior.isEnabled());

        Reporter.addScreenCapture("Validando tooltip do mês anterior");

    }

    public void validaExibibicaoDeTooltipSemConsumo7_15_30_dias() {
        MobileElement labelTitulo = buscaElementoPorClassEText("android.view.View", "Detalhamento do consumo de internet");
        Assert.assertTrue(labelTitulo.isEnabled());

        MobileElement label7Dias = buscaElementoPorClassEText("android.view.View", "7 dias");
        Assert.assertTrue(label7Dias.isEnabled());

        MobileElement tooltip7Dias = buscaElementoPorClassEText("android.view.View", "Você ainda não usou seus dados por tanto tempo");
        Assert.assertTrue(tooltip7Dias.isEnabled());

        Reporter.addScreenCapture("Validando tooltip do 7 dias");

        MobileElement label15Dias = buscaElementoPorClassEText("android.view.View", "15 dias");
        Assert.assertTrue(label15Dias.isEnabled());
        label15Dias.click();

        MobileElement tooltip15Dias = buscaElementoPorClassEText("android.view.View", "Você ainda não usou seus dados por tanto tempo");
        Assert.assertTrue(tooltip15Dias.isEnabled());

        Reporter.addScreenCapture("Validando tooltip 15 dias");

        MobileElement label30Dias = buscaElementoPorClassEText("android.view.View", "30 dias");
        Assert.assertTrue(label30Dias.isEnabled());
        label30Dias.click();

        MobileElement tooltip30Dias = buscaElementoPorClassEText("android.view.View", "Você ainda não usou seus dados por tanto tempo");
        Assert.assertTrue(tooltip30Dias.isEnabled());

        Reporter.addScreenCapture("Validando tooltip 30 dias");
    }

    public void clicarEmDetalhamentoDoConsumoDeInternetPos() {
        scrollDown();
        scrollDown();
        scroll(0.3, 0.4);
        MobileElement btnDetalhesDoConsumo = buscaElementoPorClassEText("android.view.View", "Detalhes do consumo de minha internet \uEA3A");
        Reporter.addScreenCapture("Cliquei para acessar os detalhes");
        btnDetalhesDoConsumo.click();
    }

    public void validaQueNaoEstaExibindoDataDeRenovacao() {
        MobileElement labelRenova = null;
        try {
            labelRenova = buscaElementoPorClassETextPartial("android.view.View", "Renova em");
        } catch (Exception ignored) {

        }

        Assert.assertNull(labelRenova);
    }

    public void validaExibicaoDoLayoutDeConsumo() {

        MobileElement labelRenovaEm = buscaElementoPorClassETextPartial("android.view.View", "Renova em");
        Assert.assertTrue(labelRenovaEm.isEnabled());

        MobileElement labelConsumoValue = buscaElementoPorClassETextPartial("android.view.View", "Consumo");
        Assert.assertTrue(labelConsumoValue.isEnabled());

        MobileElement labelDisponivelValue = buscaElementoPorClassETextPartial("android.view.View", "Disponível");
        Assert.assertTrue(labelDisponivelValue.isEnabled());

//        setContext(CONTEXTO.WEBVIEW);
//        esperaElementoFicarVisivel(cardConsumoWebView);
//        esperaElementoFicarVisivel(labelValorConsumoWebView);
//        esperaElementoFicarVisivel(labelValorDisponivelWebView);
//        setContext(CONTEXTO.NATIVE_APP);
    }

    public void validaLayoutSemConsumoDaFranquia() {
        MobileElement labelConsumoValue = validaTextoEmDoisElementosPorClasse("android.view.View", "Consumo", "0%");
        Assert.assertTrue(labelConsumoValue.isEnabled());

        MobileElement labelDisponivelValue = validaTextoEmDoisElementosPorClasse("android.view.View", "Disponível", "100%");
        Assert.assertTrue(labelDisponivelValue.isEnabled());

    }

    public void clicarNoIconeMaisDetalhes() {
        esperaElementoFicarVisivel(btnMaisDetalhes);
        List<MobileElement> list = btnMaisDetalhes.findElementsByClassName("android.view.View");
        MobileElement icone = null;
        for (MobileElement element : list) {
            System.out.println(element.getText());
            if (element.getText().length() == 0) {
                icone = element;
            }
        }

        assert icone != null;
        icone.click();
    }

    public void validaModalDeMaisDetalhes() {
        MobileElement labelTitulo = buscaElementoPorClassEText("android.view.View", "Minhas datas");
        Assert.assertTrue(labelTitulo.isEnabled());

        MobileElement labelFechamento = buscaElementoPorClassEText("android.view.View", "Fechamento da conta");
        Assert.assertTrue(labelFechamento.isEnabled());

        MobileElement labelRenovacao = buscaElementoPorClassETextPartial("android.view.View", "Renovação de internet,");
        Assert.assertTrue(labelRenovacao.isEnabled());

        MobileElement labelVencimento = buscaElementoPorClassETextPartial("android.view.View", "Vencimento da conta");
        Assert.assertTrue(labelVencimento.isEnabled());

        MobileElement btnOkEntendi = buscaElementoPorClassETextPartial("android.view.View", "Ok, entendi");
        Assert.assertTrue(btnOkEntendi.isEnabled());
    }

    public int validaExibicaoDoArredondamentoConformeOTipo(String unidadeMedida) {
        return buscaQuantidadeDeElementoPorClassETextPartial("android.view.View", unidadeMedida);
    }

    public void validaLayoutConsumoTotalDaFranquia() {
        MobileElement labelConsumoValue = validaTextoEmDoisElementosPorClasse("android.view.View", "Consumo", "100%");
        Assert.assertTrue(labelConsumoValue.isEnabled());

        MobileElement labelDisponivelValue = validaTextoEmDoisElementosPorClasse("android.view.View", "Disponível", "0%");
        Assert.assertTrue(labelDisponivelValue.isEnabled());
    }
}
