package pages.meuPlano.servicos;

import enums.CONTEXTO;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

import static utils.MobileWebDriver.setContext;

public class MinhaPromocaoPage extends GeralPage {

    @FindBy(id = "cancelapromo")
    public MobileElement btnConfirmaCancelarPromo;

    @FindBy(id = "naocancela")
    public MobileElement btnNaoCancelarPromo;

    @FindBy(id = "confirmaOk")
    public MobileElement btnConfirmaOk;

    @FindBy(id = "termoAceiteContratacao")
    public MobileElement checkboxTermoDeAceite;

    @FindBy(css = "#termoAceiteRenovacao")
    public MobileElement checkboxAceitoAntecipacao;

    @FindBy(css = "#modal > div > div.modal-actions-btns > div.btn.btn-modal.btn-renovar.btn-green.btn-text-align")
    public MobileElement btnConfirmarAntecipacao;

    @FindBy(css = "#promotion > div > h1")
    public MobileElement labelOutrasPromotion;

    @FindBy(css = "#promotion > div.swiper-container.swiper-container-horizontal.swiper-container-android > div.swiper-wrapper > div.card.item.swiper-slide.swiper-slide-active > div.carrousel-btn-bottom > a.btn.btn-green.btn-text-align")
    public MobileElement btnContratarPromocao;


    @FindBy(css = "#modal > div > div.modal-actions-btns > div.btn.btn-modal.btn-contratar.btn-green.btn-text-align")
    public MobileElement btnConfirmarContratacao;

    @FindBy(css = "#modal > div > div.modal-title > h4")
    public MobileElement labelTituloModal;

    @FindBy(css = "#modal")
    public MobileElement modal;

    public MinhaPromocaoPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void validarPageMinhaPromocao() {
        MobileElement labelPromocoes = buscaElementoPorClassEText("android.view.View", "Promoções");
        Assert.assertTrue(labelPromocoes.isEnabled());
    }

    public void clicarRecarregarAgora() {
        scroll(0.9, 0.6);
        Reporter.addScreenCapture("Exibiu botão para Recarregar");
        MobileElement btnRecarregarAgora = buscaElementoPorClassEText("android.view.View", "Recarregar agora");
        btnRecarregarAgora.click();
    }

    public void validarPageMinhaPromocaoParaPre() {
        MobileElement labelPromocoes = buscaElementoPorClassEText("android.view.View", "Promoções");
        Assert.assertTrue(labelPromocoes.isEnabled());

        MobileElement labelMinhaPromocao = buscaElementoPorClassEText("android.view.View", "Minha promoção");
        Assert.assertTrue(labelMinhaPromocao.isEnabled());

        MobileElement labelRenovaEm = buscaElementoPorClassETextPartial("android.view.View", "Renova em");
        Assert.assertTrue(labelRenovaEm.isEnabled());
        String textoRenova = labelRenovaEm.getText();
        Assert.assertTrue(textoRenova.contains("se houver saldo"), textoRenova);

        MobileElement labelIntenet = buscaElementoPorClassEText("android.view.View", "Internet");
        Assert.assertTrue(labelIntenet.isEnabled());

        MobileElement labelSMS = buscaElementoPorClassEText("android.view.View", "SMS");
        Assert.assertTrue(labelSMS.isEnabled());

        MobileElement labelMinPOutrasOperadoras = buscaElementoPorClassEText("android.view.View", "Min para outras operadoras");
        Assert.assertTrue(labelMinPOutrasOperadoras.isEnabled());

        MobileElement labelOutrosBeneficios = buscaElementoPorClassEText("android.view.View", "Outros benefícios");
        Assert.assertTrue(labelOutrosBeneficios.isEnabled());

        Reporter.addScreenCapture("Validando tela de minhas promoções.");

        scroll(0.8, 0.5);


        MobileElement btnAnteciparRenovacao = buscaElementoPorClassEText("android.view.View", "Antecipar renovação");
        Assert.assertTrue(btnAnteciparRenovacao.isEnabled());

        MobileElement btnEntendaPromocao = buscaElementoPorClassETextPartial("android.view.View", "Entenda a promoção");
        Assert.assertTrue(btnEntendaPromocao.isEnabled());

        MobileElement btnConhecaOutrasPromocoes = buscaElementoPorClassETextPartial("android.view.View", "Conheça outras promoções");
        Assert.assertTrue(btnConhecaOutrasPromocoes.isEnabled());

        MobileElement labelTireSuasDuvidas = buscaElementoPorClassETextPartial("android.view.View", "Tire suas dúvidas");
        Assert.assertTrue(labelTireSuasDuvidas.isEnabled());

        Reporter.addScreenCapture("Validando tela de minhas promoções.");

    }

    public void cliclarEntendaAPromocao() {
        MobileElement btnEntendaAPromocao = buscaElementoPorClassETextPartial("android.view.View", "Entenda a promoção");
        btnEntendaAPromocao.click();
    }

    public void validaDetalhesDaPromocaoContratada() {
        MobileElement labelTitle = buscaElementoPorClassETextPartial("android.view.View", "Entenda a promoção");
        Assert.assertTrue(labelTitle.isEnabled());

        MobileElement labelOqueEstaIncluso = buscaElementoPorClassEText("android.view.View", "O que está incluso");
        Assert.assertTrue(labelOqueEstaIncluso.isEnabled());

        MobileElement labelValor = buscaElementoPorClassEText("android.view.View", "Valor");
        Assert.assertTrue(labelValor.isEnabled());

        MobileElement labelInternet = buscaElementoPorClassEText("android.view.View", "Internet");
        Assert.assertTrue(labelInternet.isEnabled());

        MobileElement labelSMS = buscaElementoPorClassEText("android.view.View", "SMS");
        Assert.assertTrue(labelSMS.isEnabled());

        MobileElement btnAnteciparRenovacao = buscaElementoPorClassEText("android.view.View", "Antecipar renovação");
        Assert.assertTrue(btnAnteciparRenovacao.isEnabled());

        Reporter.addScreenCapture("Validando detalhes do plano.");
        scrollDown();

        MobileElement btnTudoSobreAInternet = buscaElementoPorClassEText("android.view.View", "Tudo sobre a Internet");
        Assert.assertTrue(btnTudoSobreAInternet.isEnabled());

        MobileElement btnCanceleSuaPromocao = buscaElementoPorClassETextPartial("android.view.View", "Cancele sua promoção");
        Assert.assertTrue(btnCanceleSuaPromocao.isEnabled());

        Reporter.addScreenCapture("Validando detalhes do plano.");

    }

    public void cancelarPromocao() {
        MobileElement btnCanceleSuaPromocao = buscaElementoPorClassETextPartial("android.view.View", "Cancele sua promoção");
        Assert.assertTrue(btnCanceleSuaPromocao.isEnabled());
        btnCanceleSuaPromocao.click();


    }

    public void validaModalDeCancelamentoDePromocao() {
        MobileElement title = buscaElementoPorClassEText("android.view.View", "Tem certeza que deseja desativar sua promoção?");
        Assert.assertTrue(title.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "Ao cancelar você deixa de aproveitar benefícios exclusivos");
        Assert.assertTrue(labelDescricao.isEnabled());

        Assert.assertTrue(btnConfirmaCancelarPromo.isEnabled());
        Assert.assertTrue(btnNaoCancelarPromo.isEnabled());
    }

    public void clicarConfirmarCancelamento() {
        esperaElementoFicarClicavel(btnConfirmaCancelarPromo);
        btnConfirmaCancelarPromo.click();

    }

    public void validaModalOkAgoraSoAguardarParaCancelamento() {
        MobileElement title = buscaElementoPorClassEText("android.view.View", "Ok, você vai receber um SMS de confirmação");
        Assert.assertTrue(title.isEnabled());
        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "Os benefícios estarão disponíveis até o final do período de validade.");
        Assert.assertTrue(labelDescricao.isEnabled());
        Assert.assertTrue(btnConfirmaOk.isEnabled());
    }

    public void clicarEmConhecaOutrasPromocoes() {
        MobileElement btnConhecaOutrasPromocoes = buscaElementoPorClassETextPartial("android.view.View", "Conheça outras promoções");
        Assert.assertTrue(btnConhecaOutrasPromocoes.isEnabled());
        btnConhecaOutrasPromocoes.click();
    }

    public void validaPageOutrasPromocoes() {
        setContext(CONTEXTO.WEBVIEW);
        esperaElementoFicarVisivel(labelOutrasPromotion);
        Assert.assertTrue(labelOutrasPromotion.isDisplayed());
        Assert.assertEquals(labelOutrasPromotion.getText(), "Outras promoções");
        setContext(CONTEXTO.NATIVE_APP);
        Reporter.addScreenCapture("Validando página de Outras promoções.");
    }

    public void contratarNovaPromocao() {
        scroll(0.8, 0.5);
        setContext(CONTEXTO.WEBVIEW);
        esperaElementoFicarVisivel(btnContratarPromocao);
        esperaElementoFicarClicavel(btnContratarPromocao);
        Assert.assertEquals(btnContratarPromocao.getText(), "Contratar promoção");
        btnContratarPromocao.click();
        setContext(CONTEXTO.NATIVE_APP);
        Reporter.addScreenCapture("Cliquei para contratar o pacote.");
    }

    public void confirmarContratacaoDeNovaPromocao() {
        esperaElementoFicarVisivel(checkboxTermoDeAceite);
        esperaElementoFicarClicavel(checkboxTermoDeAceite);
        checkboxTermoDeAceite.click();
        Reporter.addScreenCapture("Validando confirmação.");
        setContext(CONTEXTO.WEBVIEW);
        esperaElementoFicarClicavel(btnConfirmarContratacao);
        btnConfirmarContratacao.click();
        setContext(CONTEXTO.NATIVE_APP);
        Reporter.addScreenCapture("Cliquei para confirmar.");


    }

    public void validaModalDeConfirmacaoDeContratacao() {
        setContext(CONTEXTO.WEBVIEW);
        esperaElementoFicarVisivel(labelTituloModal);
        String tituloModal = "Ok, agora é só aguardar";
        esperaTextoNoElemento(labelTituloModal, tituloModal);
        Assert.assertEquals(labelTituloModal.getText(), tituloModal);
        setContext(CONTEXTO.NATIVE_APP);
    }

    public void clicarEmAnteciparRenovacao() {
        MobileElement btnAnteciparRenovacao = buscaElementoPorClassEText("android.view.View", "Antecipar renovação");
        Assert.assertTrue(btnAnteciparRenovacao.isEnabled());
        btnAnteciparRenovacao.click();

    }

    public void validaModalDeConfirmacaoDeAntecipacao() {
        setContext(CONTEXTO.WEBVIEW);

        esperaElementoFicarVisivel(modal);

        String modalTexto = "Antecipar renovação da Vivo Pré Turbo R$ 8,90 15 DIAS BASICO\n" +
                "Renove agora por R$8,90/15 dias e comece a usar. A validade da promoção será contada a partir de hoje.\n" +
                "Termos e condições de uso\n" +
                "Benefícios da Promoção válidos por 15 dias por R$8,90: 1GB para navegar na internet 4G, SMS e ligações para fixo e celular ilimitado para qualquer Vivo do Brasil,usando 15 e WhatsApp ilimitado para envio de mensagens, vídeos e fotos.\n" +
                "Benefício de internet com velocidade de até 5 Mbps. Caso termine de usar sua franquia antes da data de renovação, o serviço será interrompido. Cadastros válidos até 15/12/2018. O benefício será renovado automaticamente por 15 dias quando for pago o valor de R$8,90, cobrado a cada 15 dias. \n" +
                "Bônus de Internet Vivo Pré Turbo 1GB: Recarregue R$35 ou mais no mês e ganhe 1 GB de bônus de internet, válido por 7 (sete) dias. Basta participar da Vivo Pré Turbo (todas as versões, exceto Vivo Pré Turbo 500MB e Vivo Pré Turbo Voz) e atingir R$35 em recargas no mês, não precisa de cadastro. É possível ganhar 1(um) bônus por mês.\n" +
                "Declaro que li e aceito os termos de antencipação da promoção.\n" +
                "Confirmar antecipaçãoCancelar";

//        modal.getText();
        System.out.println("texto modal " + modalTexto);
        Assert.assertEquals(modal.getText(), modalTexto);
//                modalTexto.contains("Antecipar renovação da") &&
//                        modalTexto.contains("Renove agora") &&
//                        modalTexto.contains("Termos e condições de uso") &&
//                        modalTexto.contains("Declaro que li e aceito os termos de antecipação da promoção.") &&
//                        modalTexto.contains("Confirmar antecipação") &&
//                        modalTexto.contains("Cancelar")
//        );

        setContext(CONTEXTO.NATIVE_APP);
//        MobileElement title = buscaElementoPorClassETextPartial("android.view.View", "Antecipar renovação da");
//        Assert.assertTrue(title.isEnabled());

//        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Renove agora");
//        Assert.assertTrue(labelDescricao.isEnabled());
//
//        MobileElement labelTermos = buscaElementoPorClassEText("android.view.View", "Termos e condições de uso");
//        Assert.assertTrue(labelTermos.isEnabled());
//        MobileElement labelDeclaracao = buscaElementoPorClassEText("android.view.View", "Declaro que li e aceito os termos de antecipação da promoção.");
//        Assert.assertTrue(labelDeclaracao.isEnabled());
//
//        MobileElement btnConfirmar = buscaElementoPorClassEText("android.view.View", "Confirmar antecipação");
//        Assert.assertTrue(btnConfirmar.isEnabled());
//
//        MobileElement btnCancelar = buscaElementoPorClassEText("android.view.View", "Cancelar");
//        Assert.assertTrue(btnCancelar.isEnabled());
    }

    public void confirmarAntecipacaoDaPromocao() {
        setContext(CONTEXTO.WEBVIEW);
        esperaElementoFicarClicavel(checkboxAceitoAntecipacao);
        checkboxAceitoAntecipacao.click();

        esperaElementoFicarClicavel(btnConfirmarAntecipacao);
        btnConfirmarAntecipacao.click();
        setContext(CONTEXTO.NATIVE_APP);
    }

    public void validaModalOkVoceVaiReceberUmSMSDeConfirmacao() {
        MobileElement title = buscaElementoPorClassETextPartial("android.view.View", "Ok, agora é só aguardar");
        Assert.assertTrue(title.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "Um SMS será enviado para confirmar a antecipação da promoção.");
        Assert.assertTrue(labelDescricao.isEnabled());

        MobileElement btnOk = buscaElementoPorClassEText("android.view.View", "Ok, obrigado");
        Assert.assertTrue(btnOk.isEnabled());

    }
}
