package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;

public class DetalhesPlanoPage extends GeralPage {

    public DetalhesPlanoPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    public void validaTelaMeuPlano(String planoEsperado) {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Meu plano");
        Assert.assertTrue(btn.isEnabled());
        MobileElement btn1 = buscaElementoPorClassEText("android.view.View", planoEsperado);
        Assert.assertTrue(btn1.isEnabled());
        MobileElement btn2 = buscaElementoPorClassEText("android.view.View", "Franquias e tarifas");
        Assert.assertTrue(btn2.isEnabled());
    }

}
