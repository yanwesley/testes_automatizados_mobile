package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;

public class RecargaPage extends GeralPage {

    public RecargaPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    public void validaTelaDeRecarga() {
        MobileElement labelRecargaVivo = buscaElementoPorClassEText("android.view.View", "Recarga Vivo");
        Assert.assertTrue(labelRecargaVivo.isEnabled());
        MobileElement labelValor = buscaElementoPorClassEText("android.view.View", "Qual valor?");
        Assert.assertTrue(labelValor.isEnabled());
        MobileElement opcaoOutroNumero = buscaElementoPorClassEText("android.view.View", "Recarregar outro número");
        Assert.assertTrue(opcaoOutroNumero.isEnabled());
    }
}
