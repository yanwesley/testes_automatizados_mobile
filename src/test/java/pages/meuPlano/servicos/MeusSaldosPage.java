package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import pages.GeralPage;

public class MeusSaldosPage extends GeralPage {

    public MeusSaldosPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
    }

    public void validaSaldoDisponivel() {
        MobileElement labelSaldo = buscaElementoPorClassETextPartial("android.view.View", "Seu saldo de recarga é de R$");
        Assert.assertTrue(labelSaldo.isEnabled());
    }

    public void validaSaldoBloqueado() {
        MobileElement labelExpirado = buscaElementoPorClassEText("android.view.View", "Seu saldo expirou e está bloqueado.");
        Assert.assertTrue(labelExpirado.isEnabled());
        MobileElement labelSaldo = buscaElementoPorClassETextPartial("android.view.View", "Faça uma recarga para desbloquear seu saldo de R$");
        Assert.assertTrue(labelSaldo.isEnabled());
    }
}
