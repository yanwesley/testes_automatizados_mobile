package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

public class LancamentosFuturosPage extends GeralPage {

    @FindBy(id = "accordion1")
    public WebElement btnAccordion1;

    @FindBy(id = "accordion2")
    public WebElement btnAccordion2;

    public LancamentosFuturosPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void validaExibicaoDoCardFechado() {
        MobileElement title = buscaElementoPorClassEText("android.view.View", "Lançamentos Futuros");
        Assert.assertTrue(title.isEnabled());

        MobileElement labelDate = buscaElementoPorClassEText("android.view.View", "PRÓXIMOS 7 DIAS");
        Assert.assertTrue(labelDate.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "Confira abaixo os valores que serão debitados quando houver saldo disponível.");
        Assert.assertTrue(labelDescricao.isEnabled());

        MobileElement labelLancamentosAnteriores = buscaElementoPorClassEText("android.view.View", "LANÇAMENTOS ANTERIORES");
        Assert.assertTrue(labelLancamentosAnteriores.isEnabled());

    }

    public void clicarExpandirDetalhes() {
        esperaElementoFicarClicavel(btnAccordion1);
        btnAccordion1.click();

    }

    public void validaExibicaoDoCardAberto() {
        MobileElement labelDebito = buscaElementoPorClassEText("android.view.View", "Débito");
        Assert.assertTrue(labelDebito.isEnabled());

        MobileElement labelContratacao = buscaElementoPorClassEText("android.view.View", "Contratação");
        Assert.assertTrue(labelContratacao.isEnabled());
        Reporter.addScreenCapture("Validando exibição do card expandindo");
        meioScrollDown();
        MobileElement labelRenovacao = buscaElementoPorClassEText("android.view.View", "Renovação");
        Assert.assertTrue(labelRenovacao.isEnabled());

    }

    public void clicarExpandirDetalhesPromocao() {
        esperaElementoFicarClicavel(btnAccordion2);
        btnAccordion2.click();
    }

    public void validaExibicaoDoLabelHojeSeHouverSaldo() {
        scrollDown();
        MobileElement labelRenovacao = buscaElementoPorClassEText("android.view.View", "Hoje, se houver saldo");
        Assert.assertTrue(labelRenovacao.isEnabled());
    }
}
