package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;

public class MeuPlanoServicosPage extends GeralPage {

    public MeuPlanoServicosPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    public void clicarBtnDetalheDoPlano() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Detalhe do plano");
        btn.click();
    }

    public void clicarBtnTrocaDePlano() {
        scrollDown();
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Troca de plano");
        btn.click();
    }

    public void clicarBtnTrocaDeAparelho() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Troca de aparelho");
        btn.click();
    }

    public void clicarBtnPacotes() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Pacotes");
        btn.click();
    }

    public void clicarBtnVivoValoriza() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Vivo Valoriza");
        btn.click();
    }

    public void clicarBtnRoamingTravel() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Roaming - Vivo Travel");
        btn.click();
    }

    public void clicarBtnFacaUmaRecarga() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Faça uma recarga");
        btn.click();
    }

    public void clicarBtnSuporteTecnico() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Suporte técnico");
        btn.click();
    }

    public void clicarBtnConfigurarAparelho() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Configurar aparelho");
        btn.click();
    }

    public void clicarBtnVivoAppStore() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Vivo App Store");
        btn.click();
    }

    public void clicarBtnAgendarAtendimento() {

        scrollDown();
        scrollDown();
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Agendar atendimento");
        btn.click();
    }

    public void clicarBtnNumeroPortocolo() {
        scrollDown();
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Número de protocolo");
        btn.click();
    }

    public void naoDeveExibirAOpcaoVivoValoriza() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Vivo Valoriza");
        Assert.assertNull(btn);
    }

    public void clicaMinhaPromocao() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Minha promoção");
        btn.click();
    }

    public void clicarBtnMeusSaldos() {
        scroll(0.7, 0.5);
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Meus saldos");
        btn.click();
    }

    public void clicarBtnLancamentosFuturos() {
        scrollDown();
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Lançamentos futuros");
        btn.click();
    }

    public void clicarBtnPlanoAnterior() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Plano anterior");
        btn.click();
    }
}