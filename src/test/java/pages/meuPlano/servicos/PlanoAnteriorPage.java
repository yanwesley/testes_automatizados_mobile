package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;

public class PlanoAnteriorPage extends GeralPage {

    public PlanoAnteriorPage(AndroidDriver<MobileElement> driver) {
        super();
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    public void validaExibicaoDeFaturasDoPlanoAnterior() {
        MobileElement labelHistoricoDeContas = buscaElementoPorClassEText("android.view.View", "Histórico de Contas");
        Assert.assertTrue(labelHistoricoDeContas.isEnabled());
        MobileElement labelContas = buscaElementoPorClassEText("android.view.View", "Contas já pagas");
        Assert.assertTrue(labelContas.isEnabled());
    }
}
