package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

public class AgendamentosPage extends GeralPage {

    @FindBy(id = "cidadesEstadoAtual")
    public MobileElement labelCidadeEstadoSelecionado;

    @FindBy(id = "map")
    public MobileElement mapa;

    @FindBy(id = "loja")
    public MobileElement selectLoja;

    @FindBy(id = "data")
    public MobileElement selectData;

    @FindBy(id = "hora")
    public MobileElement selectHora;

    @FindBy(id = "assunto")
    public MobileElement selectAssunto;

    @FindBy(id = "AgendaOk")
    public MobileElement btnOkObrigadoAgendado;

    @FindBy(id = "modalAgenda")
    public MobileElement modalAgenda;

    @FindBy(id = "linkAgendamentos")
    public MobileElement btnMeusAgendamentos;

    @FindBy(id = "modalCancelamento")
    public MobileElement modalCancelamento;

    @FindBy(id = "simcancela")
    public MobileElement btnConfirmarCancelamento;

    @FindBy(id = "confirmaOk")
    public MobileElement btnConfirmaOk;

    public AgendamentosPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    public void validaExibicaoDeLojasProximasPorGeolocalizacao() {

        String cidadeEstadoEsperado = " São Paulo - SP";

        MobileElement labelTitle = buscaElementoPorClassEText("android.view.View", "Agendamento em loja");
        Assert.assertTrue(labelTitle.isEnabled());

        Assert.assertEquals(labelCidadeEstadoSelecionado.getText(), cidadeEstadoEsperado);

        Assert.assertTrue(mapa.isEnabled());

    }

    public void preencherDadosAgendamento() {
        String loja = "Shopping Market Place";
        String hora = "09:00";
        String assunto = "Funcionamento de aparelho";

        scrollDown();
        esperaElementoFicarClicavel(selectLoja);
        selectLoja.click();
        MobileElement optionShoppingMarketPlace = buscaElementoPorClassEText("android.widget.CheckedTextView", loja);
        Reporter.addScreenCapture("Cliquei para selecionar a loja.");
        optionShoppingMarketPlace.click();
        esperaTextoNoElemento(selectLoja, loja);

        esperaElementoFicarClicavel(selectData);
        selectData.click();
        MobileElement optionDepoisDeAmanha = buscaElementoPorClassEText("android.widget.CheckedTextView", doisDiasParaFrente());
        Reporter.addScreenCapture("Cliquei para selecionar o dia.");
        optionDepoisDeAmanha.click();
        esperaTextoNoElemento(selectData, doisDiasParaFrente());


        esperaElementoFicarClicavel(selectHora);
        selectHora.click();
        MobileElement optionHora = buscaElementoPorClassEText("android.widget.CheckedTextView", hora);
        Reporter.addScreenCapture("Cliquei para selecionar a hora.");
        optionHora.click();

        esperaTextoNoElemento(selectHora, hora);
        esperaElementoFicarClicavel(selectAssunto);
        selectAssunto.click();
        MobileElement optionAssunto = buscaElementoPorClassEText("android.widget.CheckedTextView", assunto);
        Reporter.addScreenCapture("Cliquei para selecionar o assunto.");
        optionAssunto.click();
        esperaTextoNoElemento(selectAssunto, assunto);

        Reporter.addScreenCapture("Preenchi todos os campos");

        MobileElement btnAgendar = buscaElementoPorClassEText("android.view.View", "Agendar");
        btnAgendar.click();

    }

    public void validaAgendamentoComSucesso() {
        esperaElementoFicarVisivel(modalAgenda);
        esperaElementoFicarVisivel(btnOkObrigadoAgendado);
        Assert.assertEquals(btnOkObrigadoAgendado.getText(), "Ok, obrigado");
        MobileElement modalTitle = buscaElementoPorClassEText("android.view.View", "Agendamento realizado!");
        Assert.assertTrue(modalTitle.isEnabled());

        MobileElement modalDescricao = buscaElementoPorClassEText("android.view.View", "Anote o código de atendimento");
        Assert.assertTrue(modalDescricao.isEnabled());
    }

    public void validaMeusAgendamentos() {
        MobileElement titleMeusAgendamentos = buscaElementoPorClassEText("android.view.View", "Meus Agendamentos");
        Assert.assertTrue(titleMeusAgendamentos.isEnabled());

        MobileElement labelAgendado = buscaElementoPorClassEText("android.view.View", "Agendado");
        Assert.assertTrue(labelAgendado.isEnabled());

    }

    public void clicarBtnMeusAgendamentos() {
        scrollDown();
        scrollDown();
        esperaElementoFicarClicavel(btnMeusAgendamentos);
        btnMeusAgendamentos.click();
    }

    public void excluirAgendamento() {
        MobileElement labelAgendado = buscaElementoPorClassEText("android.view.View", "Agendado");
        Assert.assertTrue(labelAgendado.isEnabled());
        labelAgendado.click();
        MobileElement btnExcluir = buscaElementoPorClassEText("android.view.View", "Excluir");
        Assert.assertTrue(btnExcluir.isEnabled());
        btnExcluir.click();
    }

    public void confirmaCancelamento() {
        esperaElementoFicarVisivel(modalCancelamento);
        MobileElement descricaoModalCancelamento = buscaElementoPorClassEText("android.view.View", "Tem certeza que deseja excluir esse agendamento?");
        Assert.assertTrue(descricaoModalCancelamento.isEnabled());
        Reporter.addScreenCapture("Validando modal de cancelamento.");
        btnConfirmarCancelamento.click();
    }

    public void validaCancelamento() {
        MobileElement descricaoOkAgendamentoCancelado = buscaElementoPorClassEText("android.view.View", "Ok. Agendamento cancelado.");
        Assert.assertTrue(descricaoOkAgendamentoCancelado.isEnabled());
        Reporter.addScreenCapture("Agendamento cancelado com sucesso");
        btnConfirmaOk.click();
    }
}
