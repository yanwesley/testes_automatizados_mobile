package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;

public class VivoTravelPage extends GeralPage {

    @FindBy(id = "confirmaOk")
    public MobileElement btnConfirmarOk;

    @FindBy(id = "tarifasTravel")
    public MobileElement btnFranquiaParaAcessoAInternet;

    public VivoTravelPage(AndroidDriver<MobileElement> driver1) {
        driver = driver1;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void validaSeEstaAtivo() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "O Roaming está ativado.");
        Assert.assertTrue(btn.isEnabled());
    }

    public void validaSeEstaInativo() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Ativar o roaming internacional");
        Assert.assertTrue(btn.isEnabled());
    }

    public void clicarAtivarRoaming() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Ativar o roaming internacional");
        btn.click();
    }

    public void clicarBloquearRoaming() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Bloquear o roaming internacional");
        btn.click();
    }

    public void clicarBtnFranquiaParaAcessoAInternet() throws Throwable {
        scrollDown();
        btnFranquiaParaAcessoAInternet.click();
    }

    public void clicarAcesseOGuiaBolso() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Acesse o Guia de Bolso \uEA3A");
        try {
            scrollDown();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        btn.click();
    }

    public void validaTelaGuiaDeBolso() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Guia de Bolso");
        Assert.assertTrue(btn.isEnabled());
        MobileElement btn1 = buscaElementoPorClassEText("android.view.View", "Como sei se vou conseguir usar o meu celular no país que visitarei?");
        Assert.assertTrue(btn1.isEnabled());
    }

    public void clicarFaleComAVivoNoExterior() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Fale com a Vivo no exterior \uEA3A");
        try {
            scrollDown();
            scrollDown();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        btn.click();
    }

    public void validaTelaFaleComAVivoNoExterior() {
        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Fale com a Vivo no exterior");
        Assert.assertTrue(btn.isEnabled());
        MobileElement btn1 = buscaElementoPorClassEText("android.view.View", "+55 11 3056-8628");
        Assert.assertTrue(btn1.isEnabled());

        MobileElement btn2 = buscaElementoPorClassEText("android.view.View", "Ligue grátis do seu Vivo");
        Assert.assertTrue(btn2.isEnabled());
    }
}


