package pages.meuPlano.servicos;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

import static enums.CONTEXTO.NATIVE_APP;
import static enums.CONTEXTO.WEBVIEW;
import static utils.MobileWebDriver.setContext;

public class PacotesPage extends GeralPage {

    @FindBy(id = "simconfirma")
    public MobileElement btnAceitoModal;

    @FindBy(id = "naoconfirma")
    public MobileElement btnNaoAceitoModal;

    @FindBy(id = "OkEntendi")
    public MobileElement btnOkEntendi;

    @FindBy(id = "menu_internet")
    public MobileElement btnMenuInternet;

    @FindBy(id = "menu_minutos")
    public MobileElement btnMenuMinutos;

    @FindBy(id = "owl-item1")
    public MobileElement modalPacote1;

    @FindBy(id = "main_container")
    public MobileElement modalConfirmacaoPacote100Mb;

    @FindBy(id = "dot1")
    public MobileElement carouselDotsOption1;

    public PacotesPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void validaPaginaDePacotesPre() {
//        MobileElement title = buscaElementoPorClassEText("android.view.View", "Serviços");
//        Assert.assertTrue(title.isEnabled());
//
//        MobileElement labelInternet = buscaElementoPorClassEText("android.view.View", "INTERNET");
//        Assert.assertTrue(labelInternet.isEnabled());
//
//        MobileElement labelMinutos = buscaElementoPorClassEText("android.view.View", "MINUTOS");
//        Assert.assertTrue(labelMinutos.isEnabled());

        setContext(WEBVIEW);

        esperaElementoFicarVisivel(btnMenuInternet);
        esperaElementoFicarVisivel(btnMenuMinutos);

        Assert.assertTrue(btnMenuInternet.isEnabled());
        Assert.assertTrue(btnMenuMinutos.isEnabled());
        setContext(NATIVE_APP);
    }

    public void contratarNovoPacote() {
        MobileElement btnContratar = buscaElementoPorClassEText("android.view.View", "Contratar");
        Assert.assertTrue(btnContratar.isEnabled());

        btnContratar.click();

        Reporter.addScreenCapture("Cliquei para contratar o pacote");

        MobileElement titleModal = buscaElementoPorClassEText("android.view.View", "Termo de Uso");
        Assert.assertTrue(titleModal.isEnabled());
        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "TERMO DE USO");
        Assert.assertTrue(labelDescricao.isEnabled());

        Assert.assertTrue(btnAceitoModal.isEnabled());
        Assert.assertTrue(btnNaoAceitoModal.isEnabled());

        Reporter.addScreenCapture("Validando exibição do modal");

        btnAceitoModal.click();
        Reporter.addScreenCapture("Cliquei no botão aceitar");

        MobileElement titleModalConfirmacao = buscaElementoPorClassEText("android.view.View", "Recebemos seu pedido");
        Assert.assertTrue(titleModalConfirmacao.isEnabled());
        MobileElement labelDescricaoConfirmacao = buscaElementoPorClassEText("android.view.View", "Logo você vai receber um SMS confirmando a compra do seu pacote, ok?");
        Assert.assertTrue(labelDescricaoConfirmacao.isEnabled());

        Assert.assertTrue(btnOkEntendi.isEnabled());

        Reporter.addScreenCapture("Validando exibição da confirmação da contratação");
        btnOkEntendi.click();

    }

    public void validaContratacaoPacote() {
        MobileElement titleMeusPacotes = buscaElementoPorClassEText("android.view.View", "Meus Pacotes");
        Assert.assertTrue(titleMeusPacotes.isEnabled());

        MobileElement btnDesativar = buscaElementoPorClassEText("android.view.View", "Desativar");
        Assert.assertTrue(btnDesativar.isEnabled());


    }

    public void desabilitarPacoteAtivo() {
        MobileElement btnDesativar = buscaElementoPorClassEText("android.view.View", "Desativar");
        Assert.assertTrue(btnDesativar.isEnabled());
        btnDesativar.click();
        Reporter.addScreenCapture("Cliquei para desativar o pacote");


        MobileElement titleModal = buscaElementoPorClassEText("android.view.View", "Tem certeza que deseja cancelar seu pacote?");
        Assert.assertTrue(titleModal.isEnabled());
        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Importante: o serviço será cancelado no momento que você confirmar a solicitação.Confirma o cancelamento do pacote?");
        Assert.assertTrue(labelDescricao.isEnabled());

        Assert.assertTrue(btnAceitoModal.isEnabled());
        Assert.assertTrue(btnNaoAceitoModal.isEnabled());

        Reporter.addScreenCapture("Validando exibição do modal");

        btnAceitoModal.click();

        Reporter.addScreenCapture("Cliquei no botão sim");

        MobileElement titleModalConfirmacao = buscaElementoPorClassEText("android.view.View", "Pronto, cancelamos o pacote.");
        Assert.assertTrue(titleModalConfirmacao.isEnabled());
        MobileElement labelDescricaoConfirmacao = buscaElementoPorClassETextPartial("android.view.View", "Este serviço não está mais disponível na sua linha.");
        Assert.assertTrue(labelDescricaoConfirmacao.isEnabled());

        Assert.assertTrue(btnAceitoModal.isEnabled());

        Reporter.addScreenCapture("Validando exibição da confirmação da desabilitação");
        btnAceitoModal.click();
    }

    public void validaDesabilitacaoDoPacote() {
        MobileElement btnDesativar = null;
        try {
            btnDesativar = buscaElementoPorClassEText("android.view.View", "Desativar");
        } catch (Exception ignored) {
        }
        Assert.assertNull(btnDesativar);
    }

    public void contratarNovoPacotePre() {
        esperaElementoFicarClicavel(carouselDotsOption1);
        carouselDotsOption1.click();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clickPorPosicao(101
                , 302);

        Reporter.addStepLog("Cliquei para exibir os detalhes do plano.");
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        esperaElementoFicarVisivel(modalConfirmacaoPacote100Mb);

        String textoModal = modalConfirmacaoPacote100Mb.getText();

        Assert.assertTrue(
                textoModal.contains("Internet Adicional") &&
                        textoModal.contains("de internet válidos até o final do dia") &&
                        textoModal.contains("R$"), textoModal);

        Reporter.addScreenCapture("Validando exibição da tela de detalhes do pacote.");

        MobileElement btnComprarPacote = buscaElementoPorClassETextPartial("android.view.View", "Comprar pacote");
        Assert.assertTrue(btnComprarPacote.isEnabled());
        btnComprarPacote.click();

        String textoModalStep2 = modalConfirmacaoPacote100Mb.getText();

        Assert.assertTrue(
                textoModalStep2.contains("Você está comprando:") &&
                        textoModalStep2.contains("termos e condições do serviço") &&
                        textoModalStep2.contains("R$ 1,49"), textoModalStep2);

        Reporter.addScreenCapture("Validando tela de confirmação de contratação do pacote.");

        MobileElement btnConfirmarCompra = buscaElementoPorClassETextPartial("android.view.View", "Confirmar compra");
        Assert.assertTrue(btnConfirmarCompra.isEnabled());
        btnConfirmarCompra.click();

        Reporter.addStepLog("Cliquei para confirmar a compra.");
    }

    public void validaContratacaoPacotePre() {
        MobileElement labelTitle = buscaElementoPorClassETextPartial("android.view.View", "Tudo certo! Pacote comprado");
        Assert.assertTrue(labelTitle.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Daqui a pouco você vai receber um SMS de confirmação");
        Assert.assertTrue(labelDescricao.isEnabled());

        MobileElement btnOkObrigado = buscaElementoPorClassETextPartial("android.view.View", "Ok, obrigado");
        Assert.assertTrue(btnOkObrigado.isEnabled());
    }
}
