package pages.meuPlano.servicos;

import enums.DIRECTION;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

public class VivoValorizaPage extends GeralPage {
    public VivoValorizaPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void clicarBtnMeuPerfilNoVivoValoriza() {
        MobileElement btnMeuPerfilVivoValoriza = buscaElementoPorClassEText("android.view.View", "Meu perfil no Vivo Valoriza");
        btnMeuPerfilVivoValoriza.click();
    }


    public void validaPageVivoValorizaParaClientesNaoCadastrados() {
        MobileElement labelOqueE = buscaElementoPorClassEText("android.view.View", "O que é?");
        Assert.assertTrue(labelOqueE.isEnabled());
        Reporter.addScreenCapture("Validado primeira página");
        swipe(DIRECTION.RIGHT);
        MobileElement labelEntretenimento = buscaElementoPorClassEText("android.view.View", "Entretenimento");
        Assert.assertTrue(labelEntretenimento.isEnabled());
        Reporter.addScreenCapture("Validado segunda página");
        swipe(DIRECTION.RIGHT);
        MobileElement labelDescontoEBeneficios = buscaElementoPorClassEText("android.view.View", "Descontos e benefícios");
        Assert.assertTrue(labelDescontoEBeneficios.isEnabled());
        Reporter.addScreenCapture("Validado terceira página");
        swipe(DIRECTION.RIGHT);
        MobileElement labelGastronomia = buscaElementoPorClassEText("android.view.View", "Gastronomia");
        Assert.assertTrue(labelGastronomia.isEnabled());
        Reporter.addScreenCapture("Validado quarta página");
        MobileElement btnParticipar = buscaElementoPorClassEText("android.widget.Button", "Participar");
        Assert.assertTrue(btnParticipar.isEnabled());
        Reporter.addScreenCapture("Validado exibição do botão participar");

    }

    public void validarInformaoesVivoValoriza() {

        MobileElement btn = buscaElementoPorClassEText("android.view.View", "Não obrigado");
        if (btn != null) btn.click();

        MobileElement labelVivoValoriza = buscaElementoPorClassEText("android.view.View", "Vivo Valoriza");
        Assert.assertTrue(labelVivoValoriza.isEnabled());
        Reporter.addScreenCapture("Validando Vivo Valoriza");
        swipe(DIRECTION.DOWN);
        Reporter.addScreenCapture("Validando Vivo Valoriza");
        swipe(DIRECTION.DOWN);
        Reporter.addScreenCapture("Validando Vivo Valoriza");
        swipe(DIRECTION.DOWN);
        Reporter.addScreenCapture("Validando Vivo Valoriza");
    }

    public void validarPerfilMeuVivoValoriza() {
        MobileElement labelPerfilValoriza = buscaElementoPorClassEText("android.view.View", "Perfil Vivo Valoriza");
        Assert.assertTrue(labelPerfilValoriza.isEnabled());

        MobileElement labelMinhaCategoria = buscaElementoPorClassEText("android.view.View", "Minha categoria");
        Assert.assertTrue(labelMinhaCategoria.isEnabled());

        MobileElement labelEntendaAsCategorias = buscaElementoPorClassEText("android.view.View", "Entenda as categorias");
        Assert.assertTrue(labelEntendaAsCategorias.isEnabled());

        MobileElement labelMeusVouchers = buscaElementoPorClassEText("android.view.View", "Meus vouchers");
        Assert.assertTrue(labelMeusVouchers.isEnabled());

        MobileElement labelPesquisas = buscaElementoPorClassEText("android.view.View", "Pesquisas");
        Assert.assertTrue(labelPesquisas.isEnabled());
        Reporter.addScreenCapture("Validado exibição Meu Perfil Valoriza");

        swipe(DIRECTION.DOWN);

        MobileElement labelRegulamento = buscaElementoPorClassEText("android.view.View", "Regulamento");
        Assert.assertTrue(labelRegulamento.isEnabled());
        Reporter.addScreenCapture("Validado exibição Meu Perfil Valoriza");

    }

    public void clicarCategoriaDescontos() {
        MobileElement labelDescontos = buscaElementoPorClassEText("android.view.View", "Descontos");
        labelDescontos.click();
    }

    public void validaPageDeParceiros() {
        MobileElement labelDescontos = buscaElementoPorClassEText("android.view.View", "Descontos");
        Assert.assertTrue(labelDescontos.isEnabled());
        Reporter.addScreenCapture("Validado exibição da categoria Descontos");
        swipe(DIRECTION.DOWN);
        Reporter.addScreenCapture("Validado exibição da categoria Descontos");
        swipe(DIRECTION.DOWN);
        Reporter.addScreenCapture("Validado exibição da categoria Descontos");
        swipe(DIRECTION.DOWN);
        Reporter.addScreenCapture("Validado exibição da categoria Descontos");
    }

    public void clicarPrimeiroParceiro() {
        scrollUp();
        scrollUp();
        scrollUp();
        MobileElement imgPrivalia = buscaElementoPorClassEText("android.view.View", "4");
        imgPrivalia.click();
    }

    public void validaDetalhesDoParceiro() {
        MobileElement textPrivalia = buscaElementoPorClassEText("android.view.View", "R$ 40 de desconto em compras acima de R$ 200");
        Assert.assertTrue(textPrivalia.isEnabled());
        Reporter.addScreenCapture("Validado exibição dos detalhes do parceiro");
        swipe(DIRECTION.DOWN);
        MobileElement linkSite = buscaElementoPorClassEText("android.view.View", "Ir para o site do parceiro");
        Assert.assertTrue(linkSite.isEnabled());
        Reporter.addScreenCapture("Validado exibição do link para site do parceiro");
    }

    public void clicarLinkDoParceiro() {
        MobileElement linkSite = buscaElementoPorClassEText("android.view.View", "Ir para o site do parceiro");
        linkSite.click();

    }

    public void validaSiteParceiro() {
        try {
            WebElement btnNaoObrigado = driver.findElement(By.id("ilabspush-mobile_dontallow_button"));
            if (btnNaoObrigado.isDisplayed()) {
                btnNaoObrigado.click();
            }
        } catch (Exception e) {
        }

        MobileElement linkSite = buscaElementoPorClassEText("android.widget.Image", "Vivo Valoriza");
        Assert.assertTrue(linkSite.isEnabled());
        Reporter.addScreenCapture("Exibiu pagina do parceiro");

    }
}
