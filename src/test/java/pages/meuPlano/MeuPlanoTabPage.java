package pages.meuPlano;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.GeralPage;

public class MeuPlanoTabPage extends GeralPage {

    @FindBy(id = "Conta")
    public MobileElement btnConta;

    @FindBy(id = "Consumo")
    public MobileElement btnConsumo;

    @FindBy(id = "Serviços")
    public MobileElement btnServicos;

    @FindBy(id = "Saldo")
    public MobileElement btnSaldo;

    public MeuPlanoTabPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    public void clicarBtnServicos() {
        esperaElementoFicarClicavel(btnServicos);
        btnServicos.click();
    }

    public void clicarBtnConta() {
        esperaElementoFicarClicavel(btnConta);
        btnConta.click();
    }

    public void clicarBtnSaldo() {
        esperaElementoFicarClicavel(btnSaldo);
        btnSaldo.click();
    }

    public void clicarBtnConsumo() {
        esperaElementoFicarClicavel(btnConsumo);
        btnConsumo.click();
    }
}