package pages.meuPlano.conta;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.GeralPage;
import reporter.Reporter;

import java.util.List;

public class MeuPlanoContaPage extends GeralPage {

    @FindBy(id = "btn-boleto-atualizado")
    public MobileElement btnBoletoAtualizado;

    @FindBy(id = "btn-segunda-via-detalhada")
    public MobileElement btnSegundaViaDetalhada;

    @FindBy(id = "btn-ja-pagou")
    public MobileElement btnJaPagou;

    @FindBy(id = "btn-envia-por-email")
    public MobileElement btnEnviarPorEmail;

    @FindBy(id = "modalconfirma")
    public MobileElement modalConfirmacao;

    @FindBy(id = "confirmaOk")
    public MobileElement btnConfirmaOk;

    @FindBy(id = "naoconfirma")
    public MobileElement btnNaoConfirma;

    @FindBy(id = "boleto_info")
    public MobileElement textBoletoInfo;

    @FindBy(id = "boleto_codigo")
    public MobileElement textBoletoCodigo;

    @FindBy(id = "btn-copia-codigo-barras")
    public MobileElement btnCopiarCodigoDeBarras;

    @FindBy(id = "btnEnviaPorSMS")
    public MobileElement btnEnviarPorSms;

    @FindBy(id = "btn_desativar")
    public MobileElement btnDesativarContaDigital;

    @FindBy(id = "btn_sim")
    public MobileElement btnSim;

    @FindBy(id = "feedback_ajax_sms")
    public MobileElement textFeedbackEnviarSMS;

    @FindBy(id = "email_conta_digital")
    public MobileElement inputEmailContaDigital;

    @FindBy(id = "confirma_email")
    public MobileElement inputConfirmaEmailContaDigital;

    @FindBy(id = "check_termos")
    public MobileElement checkboxAceitoTermosContaDigital;

    @FindBy(id = "ativar_conta")
    public MobileElement btnAtivarContaDigital;

    @FindBy(id = "label_agencia")
    public MobileElement inputCadastroAgencia;

    @FindBy(id = "label_conta")
    public MobileElement inputCadastroConta;

    @FindBy(id = "digito")
    public MobileElement inputCadastroDigito;

    @FindBy(id = "idAtivarDebito")
    public MobileElement btnAtivaDebitoAutomatico;

    @FindBy(id = "botao-conf")
    public MobileElement btnContinuarAlterarEndereco;

    @FindBy(id = "notification")
    public MobileElement labelNotificacao;


    public MeuPlanoContaPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    public void validaExibicaoDeCardComContaAtrasada() {
        MobileElement labelTitle = buscaElementoPorClassEText("android.view.View", "2ª via de contas");
        Assert.assertTrue(labelTitle.isEnabled());

        MobileElement labelBloqueio = buscaElementoPorClassETextPartial("android.view.View", "está bloqueando a sua linha");
        Assert.assertTrue(labelBloqueio.isEnabled());

        MobileElement labelValorAnterior = buscaElementoPorClassETextPartial("android.view.View", "Valor anterior:");
        Assert.assertTrue(labelValorAnterior.isEnabled());

        MobileElement labelVencidaEm = buscaElementoPorClassETextPartial("android.view.View", "Vencida em");
        Assert.assertTrue(labelVencidaEm.isEnabled());

        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        Assert.assertTrue(btnCodigoDeBarras.isEnabled());
    }


    public void validaExibicaoDeCardComContaAtrasadaPlanoControle_Pos() {
        MobileElement labelTitle = buscaElementoPorClassEText("android.view.View", "2ª via de contas");
        Assert.assertTrue(labelTitle.isEnabled());

        MobileElement labelBloqueio = buscaElementoPorClassETextPartial("android.view.View", "está bloqueando a sua linha");
        Assert.assertTrue(labelBloqueio.isEnabled());

        MobileElement labelValorAnterior = buscaElementoPorClassETextPartial("android.view.View", "R$");
        Assert.assertTrue(labelValorAnterior.isEnabled());

        MobileElement labelVencidaEm = buscaElementoPorClassETextPartial("android.view.View", "Vencida em");
        Assert.assertTrue(labelVencidaEm.isEnabled());

        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        Assert.assertTrue(btnCodigoDeBarras.isEnabled());
    }

    public void validaExibicaoDeCardComContaEmAberto() {
        MobileElement labelTitle = buscaElementoPorClassEText("android.view.View", "2ª via de contas");
        Assert.assertTrue(labelTitle.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Sua conta de");
        Assert.assertTrue(labelDescricao.isEnabled());

        MobileElement labelVenceEm = buscaElementoPorClassETextPartial("android.view.View", "Vence em");
        Assert.assertTrue(labelVenceEm.isEnabled());

        MobileElement labelValor = buscaElementoPorClassETextPartial("android.view.View", "R$");
        Assert.assertTrue(labelValor.isEnabled());

        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        Assert.assertTrue(btnCodigoDeBarras.isEnabled());

        MobileElement btnSegundaViaDetalhada = buscaElementoPorClassEText("android.view.View", "2ª via detalhada");
        Assert.assertTrue(btnSegundaViaDetalhada.isEnabled());
    }

    public void validaExibicaoDeCardComContaAtrasadaPlanoFuncionario() {
        MobileElement labelBloqueio = buscaElementoPorClassETextPartial("android.view.View", "está atrasada");
        Assert.assertTrue(labelBloqueio.isEnabled());

        MobileElement labelValorAnterior = buscaElementoPorClassETextPartial("android.view.View", "R$");
        Assert.assertTrue(labelValorAnterior.isEnabled());

        MobileElement labelVencidaEm = buscaElementoPorClassETextPartial("android.view.View", "Vencida em");
        Assert.assertTrue(labelVencidaEm.isEnabled());

        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        Assert.assertTrue(btnCodigoDeBarras.isEnabled());

        MobileElement btnSegundaViaDetalhada = buscaElementoPorClassEText("android.view.View", "2ª via detalhada");
        Assert.assertTrue(btnSegundaViaDetalhada.isEnabled());

    }

    public void validaExibicaoDeCardComContaAtrasadaMultivivo() {
        MobileElement labelTitle = buscaElementoPorClassEText("android.view.View", "2ª via de contas");
        Assert.assertTrue(labelTitle.isEnabled());

        MobileElement labelBloqueio = buscaElementoPorClassETextPartial("android.view.View", "está bloqueando a sua linha");
        Assert.assertTrue(labelBloqueio.isEnabled());

        MobileElement labelValorAnterior = buscaElementoPorClassETextPartial("android.view.View", "R$");
        Assert.assertTrue(labelValorAnterior.isEnabled());

        MobileElement labelVencidaEm = buscaElementoPorClassETextPartial("android.view.View", "Vencida em");
        Assert.assertTrue(labelVencidaEm.isEnabled());

        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        Assert.assertTrue(btnCodigoDeBarras.isEnabled());
    }

    public void clicarMenuDaConta() {
        MobileElement btnMenu = buscaElementoPorClassEText("android.widget.Button", "Menu");
        btnMenu.click();
    }

    public void clicarBtnBoletoAtualizado() {
        esperaElementoFicarClicavel(btnBoletoAtualizado);
        btnBoletoAtualizado.click();
    }

    public void clicarBtnSegundaViaDetalhada() {
        esperaElementoFicarClicavel(btnSegundaViaDetalhada);
        btnSegundaViaDetalhada.click();
    }

    public void clicarBtnSegundaViaDetalhadaContaFuncionarioAtrasada() {
        MobileElement btnSegundaViaDetalhada = buscaElementoPorClassEText("android.view.View", "2ª via detalhada");
        Assert.assertTrue(btnSegundaViaDetalhada.isEnabled());
        btnSegundaViaDetalhada.click();
    }

    public void clicarBtnSegundaViaDetalhadaContaEmAberto() {
        MobileElement btnSegundaViaDetalhada = buscaElementoPorClassEText("android.view.View", "2ª via detalhada");
        Assert.assertTrue(btnSegundaViaDetalhada.isEnabled());
        btnSegundaViaDetalhada.click();
    }

    public void clicarBtnJaPagou() {
        esperaElementoFicarClicavel(btnJaPagou);
        btnJaPagou.click();
    }

    public void clicarBtnEnviarPorEMail() {
        esperaElementoFicarClicavel(btnEnviarPorEmail);
        btnEnviarPorEmail.click();
    }

    public void validaModalJaPaguei() {
        esperaElementoFicarVisivel(modalConfirmacao);
        MobileElement labelDesbloqueio = buscaElementoPorClassEText("android.view.View", "Desbloqueio por pagamento");
        Assert.assertTrue(labelDesbloqueio.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Se o seu pagamento não for identificado ou houver outras contas pendentes");
        Assert.assertTrue(labelDescricao.isEnabled());

        esperaElementoFicarClicavel(btnConfirmaOk);
        esperaElementoFicarClicavel(btnNaoConfirma);

    }

    public void validaModalJaPagueiContaEmAberto() {
        esperaElementoFicarVisivel(modalConfirmacao);
        MobileElement labelDesbloqueio = buscaElementoPorClassEText("android.view.View", "Aguardando confirmação de pagamento");
        Assert.assertTrue(labelDesbloqueio.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "Se você já pagou, basta esperar até 2 dias úteis para identificarmos seu pagamento. Vale lembrar que o informe de pagamento deve ser utilizado em casos de bloqueio de linha.");
        Assert.assertTrue(labelDescricao.isEnabled());

        esperaElementoFicarClicavel(btnConfirmaOk);
    }

    public void validaModalJaPagueiStatusAtrasadoControle() {
        esperaElementoFicarVisivel(modalConfirmacao);
        MobileElement labelDesbloqueio = buscaElementoPorClassEText("android.view.View", "Sua solicitação já foi recebida");
        Assert.assertTrue(labelDesbloqueio.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "Aguarde e sua linha será desbloqueada em até 1 hora.");
        Assert.assertTrue(labelDescricao.isEnabled());

        esperaElementoFicarClicavel(btnConfirmaOk);

    }

    public void validaModalJaPagueiStatusAtrasadoPos() {
        esperaElementoFicarVisivel(modalConfirmacao);
        MobileElement labelDesbloqueio = buscaElementoPorClassEText("android.view.View", "Desbloqueio por pagamento");
        Assert.assertTrue(labelDesbloqueio.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Se o seu pagamento não for identificado ou houver outras contas pendentes");
        Assert.assertTrue(labelDescricao.isEnabled());

        esperaElementoFicarClicavel(btnConfirmaOk);
        esperaElementoFicarClicavel(btnNaoConfirma);
    }

    public void validaModalJaPagueiContaEmAtrasoMultivivo() {
        esperaElementoFicarVisivel(modalConfirmacao);
        MobileElement labelDesbloqueio = buscaElementoPorClassEText("android.view.View", "Aguardando confirmação de pagamento");
        Assert.assertTrue(labelDesbloqueio.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "O desbloqueio de linha está indisponível em todos os canais, sua linha será desbloqueada em até 2 dias úteis após o pagamento.");
        Assert.assertTrue(labelDescricao.isEnabled());

        esperaElementoFicarClicavel(btnConfirmaOk);
    }

    public void clicarBtnCodigoDeBarras() {
        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        btnCodigoDeBarras.click();
    }

    public void validaDetalhesDoCodigoDeBarras() {
        MobileElement labelTitle = buscaElementoPorClassETextPartial("android.view.View", "Sua conta de");
        Assert.assertTrue(labelTitle.isEnabled());

        String boletoInfo = textBoletoInfo.getText();

        Assert.assertTrue(
                boletoInfo.contains("O valor é de") &&
                        boletoInfo.contains("R$") &&
                        boletoInfo.contains("com o vencimento em") &&
                        boletoInfo.contains("/"));

        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Utilize o código de barras abaixo:");
        Assert.assertTrue(labelDescricao.isEnabled());

        int tamanhoDoCodigoDeBarras = 59;

        Assert.assertEquals(textBoletoCodigo.getText().length(), tamanhoDoCodigoDeBarras);
    }

    public void clicaBtnCopiarCodigoDeBarras() {
        esperaElementoFicarClicavel(btnCopiarCodigoDeBarras);
        btnCopiarCodigoDeBarras.click();
    }

    public void validaMensagemCopiadoComSucesso() {
        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "O código de barras foi copiado com sucesso");
        Assert.assertTrue(labelDescricao.isEnabled());
    }

    public void clicarBtnEnviarPorSMS() {
        scrollDown();
        esperaElementoFicarClicavel(btnEnviarPorSms);
        btnEnviarPorSms.click();
    }

    public void validaMensagemSMSEnviadoComSucesso(String numero) {
        esperaElementoFicarVisivel(textFeedbackEnviarSMS);
        Assert.assertEquals(textFeedbackEnviarSMS.getText(), "O SMS foi enviado para o número \n" + numero);
    }

    public void validaExibicaoDeMenuContasJaPagas() {
        scrollDown();
        scrollDown();
        scrollDown();
        scrollDown();
        scroll(0.3, 0.5);
        MobileElement title = buscaElementoPorClassEText("android.view.View", "Contas já pagas");
        Assert.assertTrue(title.isEnabled());

    }

    public void clicarNoMesJaPago(String mes) {
        MobileElement btnMes = buscaElementoPorClassETextPartial("android.view.View", mes);
        Assert.assertTrue(btnMes.isEnabled());
        btnMes.click();
    }

    public void validaExibicaoCardConfigurarMinhaConta() {
        MobileElement title2via = buscaElementoPorClassETextPartial("android.view.View", "2ª via de contas");
        Assert.assertTrue(title2via.isEnabled());

        scrollDown();
        scrollDown();
        scrollDown();
        scrollDown();
        scrollDown();

        MobileElement title = buscaElementoPorClassEText("android.view.View", "Configurar minha conta");
        Assert.assertTrue(title.isEnabled());

        MobileElement btnDebitoAutomatico = buscaElementoPorClassETextPartial("android.view.View", "Débito automático");
        Assert.assertTrue(btnDebitoAutomatico.isEnabled());

        MobileElement btnContaDigital = buscaElementoPorClassETextPartial("android.view.View", "Conta Digital");
        Assert.assertTrue(btnContaDigital.isEnabled());

    }

    public boolean validaSeEstaDesativadoAContaDigital() {
        MobileElement btnContaDigital = buscaElementoPorClassETextPartial("android.view.View", "Conta Digital");
        Assert.assertTrue(btnContaDigital.isEnabled());


        return btnContaDigital.getText().contains("Desativada");
    }

    public void desativarContaDigital() {
        MobileElement btnContaDigital = buscaElementoPorClassETextPartial("android.view.View", "Conta Digital");
        Assert.assertTrue(btnContaDigital.isEnabled());
        btnContaDigital.click();

        MobileElement titleContaDigital = buscaElementoPorClassEText("android.view.View", "Conta Digital");
        Assert.assertTrue(titleContaDigital.isEnabled());

        scrollDown();

        MobileElement btnDesativarConta = buscaElementoPorClassETextPartial("android.view.View", "Desativar Conta Digital");
        Assert.assertTrue(btnDesativarConta.isEnabled());

        btnDesativarConta.click();

        MobileElement titleDesativarContaDigital = buscaElementoPorClassEText("android.view.View", "Desativar Conta Digital");
        Assert.assertTrue(titleDesativarContaDigital.isEnabled());

        scrollDown();

        btnDesativarContaDigital.click();

        MobileElement labelDescricaoModal = buscaElementoPorClassETextPartial("android.view.View", "Tem certeza que deseja desativar a Conta Digital? ");
        Assert.assertTrue(labelDescricaoModal.isEnabled());


        btnSim.click();

        MobileElement labelDesativadoComSucesso = buscaElementoPorClassEText("android.view.View", "Conta Digital desativada com sucesso.");
        Assert.assertTrue(labelDesativadoComSucesso.isEnabled());

    }

    public void validaExibicaoDaContaDigitalDesativada() {
        MobileElement labelCardTitle = buscaElementoPorClassETextPartial("android.view.View", "Ative a Conta Digital e ganhe ");
        Assert.assertTrue(labelCardTitle.isEnabled());
        MobileElement btnAtivarContaDigital = buscaElementoPorClassEText("android.view.View", "Ativar a Conta Digital");
        Assert.assertTrue(btnAtivarContaDigital.isEnabled());
        Reporter.addScreenCapture("Validando card com conta digital desativada");
        scrollDown();
        Reporter.addScreenCapture("Validando card com conta digital desativada");
    }

    public boolean validaSeEstaAtivadaAContaDigital() {
        MobileElement btnContaDigital = buscaElementoPorClassETextPartial("android.view.View", "Conta Digital");
        Assert.assertTrue(btnContaDigital.isEnabled());


        return btnContaDigital.getText().contains("Ativado");
    }

    public void ativarContaDigital(String email) {
        MobileElement btnContaDigital = buscaElementoPorClassETextPartial("android.view.View", "Conta Digital");
        Assert.assertTrue(btnContaDigital.isEnabled());
        btnContaDigital.click();

        MobileElement titleContaDigital = buscaElementoPorClassEText("android.view.View", "Conta Digital");
        Assert.assertTrue(titleContaDigital.isEnabled());

        MobileElement btnAtivarConta = buscaElementoPorClassEText("android.view.View", "Ativar a Conta Digital");
        Assert.assertTrue(btnAtivarConta.isEnabled());

        btnAtivarConta.click();

        MobileElement titleAtivarContaDigital = buscaElementoPorClassEText("android.view.View", "Ativar Conta Digital");
        Assert.assertTrue(titleAtivarContaDigital.isEnabled());

        inputEmailContaDigital.sendKeys(email);

        driver.navigate().back();

        inputConfirmaEmailContaDigital.sendKeys(email);

        driver.navigate().back();

        checkboxAceitoTermosContaDigital.click();

        Reporter.addScreenCapture("Preenchi os dados para ativar a conta digital");
        btnAtivarContaDigital.click();

        MobileElement labelDescricaoModal = buscaElementoPorClassEText("android.view.View", "Ativada");
        Assert.assertTrue(labelDescricaoModal.isEnabled());
    }

    public void validaExibicaoDaContaDigitalAtivada() {
        MobileElement labelCardTitle = buscaElementoPorClassEText("android.view.View", "Ativada");
        Assert.assertTrue(labelCardTitle.isEnabled());

        MobileElement labelDescricao = buscaElementoPorClassEText("android.view.View", "Com a Conta Digital, você ganha 500MB de internet todo mês.");
        Assert.assertTrue(labelDescricao.isEnabled());

        MobileElement btnAlterarEmail = buscaElementoPorClassETextPartial("android.view.View", "Alterar e-mail");
        Assert.assertTrue(btnAlterarEmail.isEnabled());
        Reporter.addScreenCapture("Validando card com conta digital ativada");
        scrollDown();

        MobileElement btnDestativar = buscaElementoPorClassETextPartial("android.view.View", "Desativar Conta Digital");
        Assert.assertTrue(btnDestativar.isEnabled());
        Reporter.addScreenCapture("Validando card com conta digital ativada");
    }

    public void ativarDebitoAutomatico(List<String> dadosBancariosList) {

        MobileElement btnDebitoAutomatico = buscaElementoPorClassETextPartial("android.view.View", "Débito automático");
        Assert.assertTrue(btnDebitoAutomatico.isEnabled());

        btnDebitoAutomatico.click();

        MobileElement titleDebitoAutomatico = buscaElementoPorClassEText("android.view.View", "Débito automático");
        Assert.assertTrue(titleDebitoAutomatico.isEnabled());

        MobileElement labelTitleCard = buscaElementoPorClassEText("android.view.View", "Selecione seu banco e cadastre sua conta");
        Assert.assertTrue(labelTitleCard.isEnabled());


        MobileElement btnBancoDoBrasil = buscaElementoPorClassETextPartial("android.widget.Button", "Banco do Brasil");
        Assert.assertTrue(btnBancoDoBrasil.isEnabled());

        Reporter.addScreenCapture("Estou na tela para selecionar o banco");
        btnBancoDoBrasil.click();
        Reporter.addScreenCapture("Selecionei a opção 'Banco Do Brasil'");


        MobileElement labelBancoSelecionado = buscaElementoPorClassEText("android.view.View", "Banco do Brasil");
        Assert.assertTrue(labelBancoSelecionado.isEnabled());

        inputCadastroAgencia.sendKeys(dadosBancariosList.get(0));
        driver.navigate().back();

        inputCadastroConta.sendKeys(dadosBancariosList.get(1));

        driver.navigate().back();
        inputCadastroDigito.sendKeys(dadosBancariosList.get(2));

        driver.navigate().back();
        Reporter.addScreenCapture("Preenchi os dados bancários");

        btnAtivaDebitoAutomatico.click();

        MobileElement labelPrimeiraEtapa = buscaElementoPorClassETextPartial("android.view.View", "Primeira etapa concluída.");
        Assert.assertTrue(labelPrimeiraEtapa.isEnabled());

    }

    public void desativarDebitoAutomatico() {
        MobileElement title = buscaElementoPorClassEText("android.view.View", "Débito automático");
        Assert.assertTrue(title.isEnabled());

        scrollDown();

        MobileElement btnDesativarDebitoAutomatico = buscaElementoPorClassEText("android.view.View", "Desativar débito automático");
        Assert.assertTrue(btnDesativarDebitoAutomatico.isEnabled());


        btnDesativarDebitoAutomatico.click();


        MobileElement labelModalConfirmacao = buscaElementoPorClassEText("android.view.View", "Deseja realmente desativar o débito automático?");
        Assert.assertTrue(labelModalConfirmacao.isEnabled());

        MobileElement btnSim = buscaElementoPorClassEText("android.view.View", "Sim");
        Assert.assertTrue(btnSim.isEnabled());

        MobileElement btnNao = buscaElementoPorClassEText("android.view.View", "Não");
        Assert.assertTrue(btnNao.isEnabled());

        Reporter.addScreenCapture("Validando modal de confirmação de desativação do debíto automatico");

        btnSim.click();

    }

    public boolean validaSeEstaAtivadoODebitoAutomatico() {

        MobileElement btnDebitoAutomatico = buscaElementoPorClassETextPartial("android.view.View", "Débito automático");
        Assert.assertTrue(btnDebitoAutomatico.isEnabled());

        return btnDebitoAutomatico.getText().contains("Ativado") || btnDebitoAutomatico.getText().contains("Em análise");
    }

    public boolean validaSeEstaDesativadoODebitoAutomatico() {
        MobileElement btnDebitoAutomatico = buscaElementoPorClassETextPartial("android.view.View", "Débito automático");
        Assert.assertTrue(btnDebitoAutomatico.isEnabled());

        return btnDebitoAutomatico.getText().contains("Desativado");
    }

    public void validaExibicaoDoDebitoAutomaticoAtivado() {

        MobileElement title = buscaElementoPorClassEText("android.view.View", "Débito automático");
        Assert.assertTrue(title.isEnabled());

        MobileElement labelBanco = buscaElementoPorClassETextPartial("android.widget.Button", "Banco");
        Assert.assertTrue(labelBanco.isEnabled());

        MobileElement labelAgencia = buscaElementoPorClassETextPartial("android.widget.Button", "Agência");
        Assert.assertTrue(labelAgencia.isEnabled());

        MobileElement labelConta = buscaElementoPorClassETextPartial("android.widget.Button", "Conta-corrente");
        Assert.assertTrue(labelConta.isEnabled());

        Reporter.addScreenCapture("Validando exibição das página de Debito Automatico Ativado");

        scrollDown();

        MobileElement btnAlterarDados = buscaElementoPorClassEText("android.view.View", "Alterar dados bancários");
        Assert.assertTrue(btnAlterarDados.isEnabled());

        MobileElement btnDesativarDebitoAutomatico = buscaElementoPorClassEText("android.view.View", "Desativar débito automático");
        Assert.assertTrue(btnDesativarDebitoAutomatico.isEnabled());
    }

    public void validaExibicaoDoDebitoAutomaticoDesativado() {
        MobileElement titleDebitoAutomatico = buscaElementoPorClassEText("android.view.View", "Débito automático");
        Assert.assertTrue(titleDebitoAutomatico.isEnabled());

        MobileElement labelTitleCard = buscaElementoPorClassEText("android.view.View", "Selecione seu banco e cadastre sua conta");
        Assert.assertTrue(labelTitleCard.isEnabled());


        MobileElement btnBancoDoBrasil = buscaElementoPorClassETextPartial("android.widget.Button", "Banco do Brasil");
        Assert.assertTrue(btnBancoDoBrasil.isEnabled());
    }

    public void clicarBtnDebitoAutomatico() {

        MobileElement btnDebitoAutomatico = buscaElementoPorClassETextPartial("android.view.View", "Débito automático");
        Assert.assertTrue(btnDebitoAutomatico.isEnabled());

        btnDebitoAutomatico.click();
    }

    public void alterarEmail(String novoEmail) {
        MobileElement btnAlterarEmail = buscaElementoPorClassETextPartial("android.view.View", "Alterar e-mail");
        Assert.assertTrue(btnAlterarEmail.isEnabled());
        btnAlterarEmail.click();
        Reporter.addScreenCapture("Cliquei no botão para alterar o email.");

        MobileElement titleAlterarEmail = buscaElementoPorClassETextPartial("android.view.View", "Alterar e-mail");
        Assert.assertTrue(titleAlterarEmail.isEnabled());

        inputEmailContaDigital.clear();
        inputEmailContaDigital.sendKeys(novoEmail);

        driver.navigate().back();

        inputConfirmaEmailContaDigital.sendKeys(novoEmail);

        driver.navigate().back();

        Reporter.addScreenCapture("Preenchi os dados com o novo email.");

        btnAtivarContaDigital.click();
        Reporter.addScreenCapture("Cliquei para confirmar a alteração do email.");


    }

    public void clicarBtnContaDigital() {
        MobileElement btnContaDigital = buscaElementoPorClassETextPartial("android.view.View", "Conta Digital");
        Assert.assertTrue(btnContaDigital.isEnabled());

        btnContaDigital.click();


    }

    public void validaEmailAlteradoComSucesso(String email) {
        MobileElement labelEmailAlteradoComSucesso = buscaElementoPorClassETextPartial("android.view.View", "E-mail alterado com sucesso.");
        Assert.assertTrue(labelEmailAlteradoComSucesso.isEnabled());

        MobileElement labelEmail = buscaElementoPorClassETextPartial("android.view.View", email);
        Assert.assertTrue(labelEmail.isEnabled());

    }

    public void alterarEndereco(String endereco) {

        MobileElement title2via = buscaElementoPorClassETextPartial("android.view.View", "2ª via de contas");
        Assert.assertTrue(title2via.isEnabled());

        scrollDown();
        scrollDown();
        scrollDown();

        MobileElement btnEndereco = buscaElementoPorClassETextPartial("android.view.View", "Endereço");
        Assert.assertTrue(btnEndereco.isEnabled());

        btnEndereco.click();
        Reporter.addScreenCapture("cliquei no botão endereço");


        MobileElement labelEnderecoDeEnvio = buscaElementoPorClassETextPartial("android.view.View", "Endereço de envio");
        Assert.assertTrue(labelEnderecoDeEnvio.isEnabled());

        scrollDown();

        Reporter.addScreenCapture("Valida página de endereço");


        MobileElement btnAlterarEndereco = buscaElementoPorClassETextPartial("android.widget.Button", "Alterar endereço");
        Assert.assertTrue(btnAlterarEndereco.isEnabled());

        btnAlterarEndereco.click();
        Reporter.addScreenCapture("cliquei no botão para alterar o endereço.");

        MobileElement inputBuscarEndereco = buscaElementoPorClassEText("android.view.View", "Endereço e número");
        Assert.assertTrue(inputBuscarEndereco.isEnabled());

        esperaElementoFicarVisivel(inputBuscarEndereco);

        inputBuscarEndereco.sendKeys(endereco);

        driver.navigate().back();

        MobileElement listEndereco = buscaElementoPorClassEText("android.view.View", "R. Benjamin Constant, 100Centro, Passo Fundo - RS, Brasil");
        Assert.assertTrue(listEndereco.isEnabled());

        listEndereco.click();

        Reporter.addScreenCapture("Selecionei o endereço");

        btnContinuarAlterarEndereco.click();

        MobileElement titleConfirmaEndereco = buscaElementoPorClassEText("android.view.View", "Confirmação de endereço");
        Assert.assertTrue(titleConfirmaEndereco.isEnabled());

        MobileElement labelCep = buscaElementoPorClassEText("android.view.View", "CEP");
        Assert.assertTrue(labelCep.isEnabled());

        MobileElement labelEstado = buscaElementoPorClassEText("android.view.View", "Estado");
        Assert.assertTrue(labelEstado.isEnabled());

        Reporter.addScreenCapture("Validando tela de confirmação de alteração de endereço.");

        scrollDown();

        MobileElement btnConfirmaAlteracao = buscaElementoPorClassEText("android.widget.Button", "Confirmar alteração");
        Assert.assertTrue(labelEstado.isEnabled());

        btnConfirmaAlteracao.click();
        Reporter.addScreenCapture("Cliquei para confirmar a alteração de endereço");

    }

    public void validaPaginaDeConfirmacaoDeEnderecoAlterado() {
        MobileElement descricaoConfirmacao = buscaElementoPorClassEText("android.view.View", "O endereço para envio de contas foi atualizado com sucesso.");
        Assert.assertTrue(descricaoConfirmacao.isEnabled());

        MobileElement labelMsg = buscaElementoPorClassEText("android.view.View", "Fique de olho, a sua próxima conta ainda pode chegar no seu endereço antigo.");
        Assert.assertTrue(labelMsg.isEnabled());

        MobileElement btnOkEntendi = buscaElementoPorClassEText("android.view.View", "Ok, entendi");
        Assert.assertTrue(btnOkEntendi.isEnabled());

    }

    public void validaExibicaoDasFaturasDoPlanoAtualEPlanoAnterior() {
        MobileElement labelPlanoAtual = buscaElementoPorClassEText("android.view.View", "Plano Atual");
        Assert.assertTrue(labelPlanoAtual.isEnabled());

        Reporter.addScreenCapture("Valida exibição do 'Plano Atual'");

        meioScrollDown();

        MobileElement labelPlanoAnteriores = buscaElementoPorClassETextPartial("android.view.View", "Plano", "Anterior");
        Assert.assertTrue(labelPlanoAnteriores.isEnabled());
        Reporter.addScreenCapture("Validando exibição do plano Anterior");

    }

    public void validaExibicaoDeCardComContaAtrasadaPlanoAtualControle_Pos() {
        MobileElement labelPlanoAtual = buscaElementoPorClassEText("android.view.View", "Plano Atual");
        Assert.assertTrue(labelPlanoAtual.isEnabled());
        scroll(0.7, 0.6);

        MobileElement labelBloqueio = buscaElementoPorClassETextPartial("android.view.View", "está bloqueando a sua linha");
        Assert.assertTrue(labelBloqueio.isEnabled());

        MobileElement labelValorAnterior = buscaElementoPorClassETextPartial("android.view.View", "R$");
        Assert.assertTrue(labelValorAnterior.isEnabled());

        MobileElement labelVencidaEm = buscaElementoPorClassETextPartial("android.view.View", "Vencida em");
        Assert.assertTrue(labelVencidaEm.isEnabled());

        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        Assert.assertTrue(btnCodigoDeBarras.isEnabled());
    }

    public void validaExibicaoDeCardComContaAbertaPlanoAtualControle_Pos() {

        MobileElement labelPlanoAtual = buscaElementoPorClassEText("android.view.View", "Plano Atual");
        Assert.assertTrue(labelPlanoAtual.isEnabled());
        scroll(0.7, 0.6);

        MobileElement labelDescricao = buscaElementoPorClassETextPartial("android.view.View", "Sua conta de");
        Assert.assertTrue(labelDescricao.isEnabled());

        MobileElement labelVenceEm = buscaElementoPorClassETextPartial("android.view.View", "Vence em");
        Assert.assertTrue(labelVenceEm.isEnabled());

        MobileElement labelValor = buscaElementoPorClassETextPartial("android.view.View", "R$");
        Assert.assertTrue(labelValor.isEnabled());

        MobileElement btnCodigoDeBarras = buscaElementoPorClassEText("android.widget.Button", "Código de barras");
        Assert.assertTrue(btnCodigoDeBarras.isEnabled());

        MobileElement btnSegundaViaDetalhada = buscaElementoPorClassEText("android.view.View", "2ª via detalhada");
        Assert.assertTrue(btnSegundaViaDetalhada.isEnabled());
    }

    public void validaMensagemInformativoDeTrocaDePlano() {
        esperaElementoFicarVisivel(labelNotificacao);
    }

}

