//package pages.web;
//
//import com.hp.lft.sdk.GeneralLeanFtException;
//import com.hp.lft.sdk.web.EditFieldDescription;
//import com.hp.lft.sdk.web.LinkDescription;
//import com.hp.lft.sdk.web.WebElementDescription;
//
//import static reporter.Reporter.addScreenCapture;
//
//public class LoginHybrisPage extends GeralPageWeb {
//
//    private final String urlHybris = "http://10.129.178.142:7001/hmc/hybris";
//
//    EditFieldDescription inputUserDescription = new EditFieldDescription
//            .Builder()
//            .id("Main_user")
//            .type("text")
//            .tagName("input")
//            .build();
//
//    EditFieldDescription inputPasswordDescription = new EditFieldDescription
//            .Builder()
//            .id("Main_password")
//            .tagName("input")
//            .type("password")
//            .build();
//
//    LinkDescription buttonLoginDescription = new LinkDescription
//            .Builder()
//            .id("Main_a")
//            .build();
//
//    WebElementDescription btnOrderDescription = new WebElementDescription
//            .Builder()
//            .id("Tree/GenericExplorerMenuTreeNode[order]_label")
//            .build();
//
//    private String userValue = "admin";
//
//    private String passwordValue = "nimda";
//
//    public LoginHybrisPage() {
//        super();
//    }
//
//    public void efetuarLogin() throws GeneralLeanFtException {
//        try {
//            browser.deleteCookies();
//            browser.clearCache();
//            browser.navigate(urlHybris);
//        } catch (GeneralLeanFtException e) {
//            e.printStackTrace();
//        }
//
//        setValue(inputUserDescription, userValue);
//        setSecureValue(inputPasswordDescription, passwordValue);
//        addScreenCapture("Preenchi os dados de login.");
//        highlightAndClick(buttonLoginDescription);
//        assert elementIsVisible(btnOrderDescription);
//    }
//}
