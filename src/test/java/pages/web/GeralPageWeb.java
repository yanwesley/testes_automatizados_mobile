//package pages.web;
//
//import com.hp.lft.sdk.GeneralLeanFtException;
//import com.hp.lft.sdk.web.*;
//
//import static utils.LFTBrowser.getBrowser;
//
//public class GeralPageWeb {
//    public static final int TIME_WAIT = 30;
//
//    //The Browser instance that will be acted upon
//    public Browser browser = getBrowser();
//
//    public void highlightAndClick(WebElementDescription elementDescription) throws GeneralLeanFtException {
//        WebElement element = browser.describe(WebElement.class, elementDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        element.click();
//    }
//
//    public void highlightAndClick(LinkDescription linkDescription) throws GeneralLeanFtException {
//        Link element = browser.describe(Link.class, linkDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        element.click();
//    }
//
//    public void highlightAndDoubleClick(WebElementDescription elementDescription) throws GeneralLeanFtException {
//        WebElement element = browser.describe(WebElement.class, elementDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        element.doubleClick();
//    }
//
//    public void setValue(EditFieldDescription editFieldDescription, String value) throws GeneralLeanFtException {
//        EditField element = browser.describe(EditField.class, editFieldDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        element.setValue(value);
//    }
//
//    public void setSecureValue(EditFieldDescription editFieldDescription, String value) throws GeneralLeanFtException {
//        EditField element = browser.describe(EditField.class, editFieldDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        element.setSecure(value);
//    }
//
//    public boolean elementIsVisible(WebElementDescription elementDescription) throws GeneralLeanFtException {
//        WebElement element = browser.describe(WebElement.class, elementDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        return element.isVisible();
//    }
//
//    public boolean assertEqualsValue(EditFieldDescription editFieldDescription, String esperado) throws GeneralLeanFtException {
//        EditField element = browser.describe(EditField.class, editFieldDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        return element.getAttribute("value").equals(esperado);
//    }
//
//    public boolean assertEqualsValue(WebElementDescription webElementDescription, String esperado) throws GeneralLeanFtException {
//        WebElement element = browser.describe(WebElement.class, webElementDescription);
//        element.waitUntilVisible();
//        element.highlight();
//        return element.getAttribute("value").equals(esperado);
//    }
//
//
//}
