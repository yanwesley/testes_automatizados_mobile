//package pages.web;
//
//import api.model.Pessoa;
//import com.hp.lft.sdk.GeneralLeanFtException;
//import com.hp.lft.sdk.web.EditFieldDescription;
//import com.hp.lft.sdk.web.LinkDescription;
//import com.hp.lft.sdk.web.WebElementDescription;
//import reporter.Reporter;
//
//public class OrderHybrisPage extends GeralPageWeb {
//
//    //menu
//
//    WebElementDescription btnMenuOrderDescription = new WebElementDescription
//            .Builder()
//            .id("Tree/GenericExplorerMenuTreeNode[order]_label")
//            .build();
//
//
//    LinkDescription linkBtnOrdersMenuDescription = new LinkDescription
//            .Builder()
//            .id("Tree/GenericLeafNode[Order]_label")
//            .build();
//
//    //    Consulta
//    EditFieldDescription inputOrderSearchDescription = new EditFieldDescription
//            .Builder()
//            .id("Content/StringEditor[in Content/GenericCondition[Order.code]]_input")
//            .build();
//
//    WebElementDescription btnSearchDescription = new WebElementDescription
//            .Builder()
//            .id("Content/OrganizerSearch[Order]_searchbutton")
//            .build();
//
//    WebElementDescription lineResultDescription = new WebElementDescription
//            .Builder()
//            .id("Content/ClassificationOrganizerList[Order]_innertable")
//            .build();
//
//
//    //    Detalhes
//
//    WebElementDescription btnOrderDetailsDescription = new WebElementDescription
//            .Builder()
//            .id("Content/EditorTab[Order.tab.order.details]_a")
//            .build();
//
//    EditFieldDescription inputUserDetalhesDescription = new EditFieldDescription
//            .Builder()
//            .id("ajaxinput_Content/AutocompleteReferenceEditor[in Content/Attribute[Order.user]]")
//            .build();
//
//
//    public OrderHybrisPage() {
//        super();
//    }
//
//    public void acessarMenuOrders() throws GeneralLeanFtException {
//        highlightAndClick(btnMenuOrderDescription);
//        highlightAndClick(linkBtnOrdersMenuDescription);
//    }
//
//    public void pesquisarOrderPorNumeroDoPedido(String numeroPedido) throws GeneralLeanFtException {
//        setValue(inputOrderSearchDescription, numeroPedido);
//        Reporter.addScreenCapture("Inseri o número da ordem: " + numeroPedido);
//        highlightAndClick(btnSearchDescription);
//    }
//
//    public void validaResultadoDaConsulta() throws GeneralLeanFtException {
//        assert elementIsVisible(lineResultDescription);
//    }
//
//    public void clicarNoResultadoParaAbrirOsDetalhes(String numeroPedido) throws GeneralLeanFtException {
//        highlightAndDoubleClick(
//                new WebElementDescription
//                        .Builder()
//                        .id("Content/StringDisplay[" + numeroPedido + "]_td3")
//                        .build()
//        );
//    }
//
//    public void validaDetalhesDoPedido(Pessoa pessoa) throws GeneralLeanFtException {
//        highlightAndClick(btnOrderDetailsDescription);
////        assert assertEqualsValue(inputUserDetalhesDescription, pessoa.email);
//    }
//}
