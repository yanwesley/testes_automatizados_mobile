package pages.aura;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.GeralPage;

public class AuraPage extends GeralPage {

    @FindBy(id = "icn_keyboard")
    public MobileElement btnTeclado;

    @FindBy(id = "icn_question")
    public MobileElement btnDuvidas;

    @FindBy(id = "microphone_holder")
    public MobileElement btnMicrofone;

    @FindBy(id = "arrow")
    public MobileElement btnFechar;

    @FindBy(id = "br.com.vivo.enterprise:id/request")
    public MobileElement inputMeDiga;

    @FindBy(id = "responses_container")
    public MobileElement textResponta;

    public AuraPage(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void clicarBtnTeclado() {
        esperaElementoFicarClicavel(btnTeclado);
        btnTeclado.click();
    }

    public void clicarBtnMicrofone() {
        esperaElementoFicarClicavel(btnMicrofone);
        btnMicrofone.click();
    }

    public void clicarBtnDuvidas() {
        esperaElementoFicarClicavel(btnDuvidas);
        btnDuvidas.click();
    }

    public void clicarBtnFechar() {
        esperaElementoFicarClicavel(btnFechar);
        btnFechar.click();
    }

    public void preencherPergunta(String texto) {
        esperaElementoFicarVisivel(inputMeDiga);
//        TouchActions action = new TouchActions(driver);
//        action.longPress(inputMeDiga);
//        action.perform();
        driver.setClipboardText(texto);
//        inputMeDiga.click();
//        KeyEvent keyEvent = new KeyEvent();
//        keyEvent.withKey(AndroidKey.PASTE);
        driver.pressKey(new KeyEvent().withKey(AndroidKey.PASTE));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void validaResposta() {

    }

}
