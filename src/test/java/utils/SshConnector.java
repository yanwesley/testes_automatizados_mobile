package utils;

import com.jcraft.jsch.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

/**
 * This is an example of SSH connector using JSCh library
 * <p>
 * JSch allows you to connect to an sshd server and use port forwarding, X11
 * forwarding, file transfer, etc., and you can integrate its functionality into
 * your own Java programs. JSch is licensed under BSD style license.
 * <p>
 * comando "grep " & linha &" /opt/web/logs/WLS_Hybris_HML/WLS_Hybris_HML_Srv1/WLS_Hybris_HML_Srv1.out"
 * user: qalog
 * password:  V!V*2018@
 * host :10.129.178.142
 * exemplo linha portabilidade: 1195175XXXX
 *
 * @author Deovan Zanol
 */
public class SshConnector {

    // Default logger
    private static final Logger LOG = Logger.getLogger(SshConnector.class.getSimpleName());
    // SSH server host
    private String host = "10.129.178.142";
    // SSH server port
    private int port = 22;
    // User login
    private String user = "qalog";
    // User password
    private String password = "V!V*2018@";
    // Connection timeout
    private int timeout = 50;


    public String buscarCodigoOTPMudarDePlano(String numeroLinha) {
        String command = " grep " + numeroLinha + " /opt/web/logs/WLS_Hybris_HML/WLS_Hybris_HML_Srv1/WLS_Hybris_HML_Srv1.out | grep -o 'Seu código de acesso é ......'| grep -o '[0-9]*$'";
        try {
            String[] otps = executeCommand(command).split("\n");
            return otps[otps.length - 1];
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String buscarCodigoOTPMMigrarDeOperadora(String numeroLinha) {
        String command = " grep " + numeroLinha + " /opt/web/logs/WLS_Hybris_HML/WLS_Hybris_HML_Srv1/WLS_Hybris_HML_Srv1.out | grep -o '<ns1:valor>......' | grep -o '[0-9]*$'";
        try {
            String[] otps = executeCommand(command).split("\n");
            return otps[otps.length - 1];
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String executeCommand(final String command) throws IOException { // Cliente SSH final
        JSch jsch = new JSch();
        Properties props = new Properties();
        props.put("StrictHostKeyChecking", "no");
        String result = "";
        try {
            Session session = jsch.getSession(user, host, 22);
            session.setConfig(props);
            session.setPassword(password);
            session.connect();
            java.util.logging.Logger.getLogger(SshConnector.class.getName())
                    .log(Level.INFO, session.getServerVersion());

            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            // Daqui para baixo é somente para imprimir a saida
            channel.setInputStream(null);

            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();

            channel.connect();

            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) {
                        break;
                    }
                    System.out.print(new String(tmp, 0, i));
                    result = (new String(tmp, 0, i));
                }
                if (channel.isClosed()) {
                    if (in.available() > 0) {
                        continue;
                    }
                    System.out
                            .println("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();

        } catch (JSchException ex) {
            java.util.logging.Logger.getLogger(SshConnector.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

        return result;
    }
}
