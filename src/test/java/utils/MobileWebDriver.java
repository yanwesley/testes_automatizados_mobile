package utils;

import com.google.common.io.Files;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import enums.CONTEXTO;
import gherkin.deps.net.iharder.Base64;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static utils.FileHelper.criarPasta;

public class MobileWebDriver {
    public static String url = "http://127.0.0.1:4723/wd/hub/";
    private static AndroidDriver<MobileElement> driver = null;

    public static AndroidDriver<MobileElement> getDriver() {
        if (driver == null) {
            initialize();
        }
        return driver;
    }

    public static void initialize() {
        try {
            driver = new AndroidDriver<>(new URL(url), getCapabilities());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //set location to SP
        setLocation();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public static void quitDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    public static void fecharApp() {
        driver.navigate().back();
    }

    public static void resetApp() {
        driver.resetApp();
        setLocation();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public static void setContext(CONTEXTO contexto) {
        switch (contexto) {
            case WEBVIEW:
                Set<String> contextNames = driver.getContextHandles();
                for (String contextName : contextNames) {
                    if (contextName.contains("WEBVIEW")) {
                        driver.context(contextName);
                    }
                }
                break;
            case NATIVE_APP:
                driver.context("NATIVE_APP");
                break;
        }
    }

    private static void setLocation() {
        driver.setLocation(new Location(-23.622151, -46.699140, 0));
    }

    public static DesiredCapabilities getCapabilities() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", "420068be01ceb471");//420068be01ceb471 j1 ou 42009ac7f2766465 j5
        caps.setCapability("app", new File("src\\test\\resources\\app-vivoMovelBR-allsdk-enterprise-10.8.29.apk").getAbsolutePath());
        caps.setCapability("noSign", "false");
//        caps.setCapability("platformVersion", "5.1.1");
        caps.setCapability("platformName", "Android");
        caps.setCapability("appPackage", "br.com.vivo.enterprise");
        caps.setCapability("appActivity", "br.com.vivo.MainActivity");
        caps.setCapability("autoGrantPermissions", true);
        caps.setCapability("unlockType", "pin");
        caps.setCapability("unlockKey", "1234");
        caps.setCapability("noReset", false);
        return caps;
    }

    public static void iniciarGravacaoAppium() {
        getDriver()
                .startRecordingScreen(
                        new AndroidStartScreenRecordingOptions()
                );
    }

    public static void encerrarGravacaoAppium(String outputResult, ExtentTest test) throws Throwable {
        String result = getDriver().stopRecordingScreen();
        byte[] decodedVideo = Base64.decode(result);
        String videoNome = "video.mp4";
        criarPasta(outputResult);
        File file = new File(outputResult.concat(videoNome));
        Files.write(decodedVideo, file);
        test.log(LogStatus.INFO, "<h3> Video </h3>" +
                test.addScreencast(videoNome));
    }

    public static String captureScreenShotAppium(String outputResult, Integer contador) throws Throwable {
        File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        criarPasta(outputResult);
        String nomePrint = contador.toString() + ".png";
        File imagemDestino = new File(outputResult + nomePrint);
        FileUtils.copyFile(scrFile, imagemDestino);
        return nomePrint;
    }

}
