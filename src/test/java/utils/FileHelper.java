package utils;

import java.io.File;

public class FileHelper {

    public static void criarPasta(String caminhoPastaCriar) {

        File pasta = new File(caminhoPastaCriar);
        if (!pasta.exists()) {
            pasta.mkdirs();
        }
    }
}
