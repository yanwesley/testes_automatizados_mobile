package core;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import utils.MobileWebDriver;

@CucumberOptions(
        features = ("src/test/java/features/lojaOnline/CN0053-Migração Controle para Pós-pago com aquisição Via meu vivo PF.feature"),
        glue = {"steps/"},
        monochrome = true,
        strict = true,
        plugin = {
                "pretty",
                "junit:target/cucumber-report/junit.xml",
                "json:target/cucumber-report/json.json",
                "html:target/cucumber-report/html",
                "reporter.ExtentCucumberFormatter:"
        }
)
public class RunnerClass extends AbstractTestNGCucumberTests {


}