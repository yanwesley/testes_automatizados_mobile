# language: pt

Funcionalidade: CN0001-Visualizacao do PDF do Roaming de Dados Via App Android

  Cenário: CT0001-Verificar apresentacao do PDF do Roaming de Dados ativo com linha Pos via App Android
  Garantir que o sistema disponibilize um novo layout para a página Vivo Travel Roaming Internacional, onde será possível realizar a Ativação do serviço para o cliente

    Dado que eu esteja logado
      | (11) 9 4127-4089 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Roaming - Vivo Travel'
    Entao verifico se o roaming esta ativo
    E clico em 'Franquia para acesso a internet'
    Entao  deve ser apresentado o PDF com as informacoes da franquia internacional

  Cenário: CT0002-Verificar apresentacao do PDF do Roaming de Dados desativado com linha Multivivo Titular via App Android
  Garantir que o sistema disponibilize um novo layout para a página Vivo Travel Roaming Internacional, onde será possível realizar a Ativação do serviço para o cliente

    Dado que eu esteja logado
      | (11) 9 9729-2377 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Roaming - Vivo Travel'
    Entao verifico se o roaming esta desativado
    E clico em 'Franquia para acesso a internet'
    Entao deve ser apresentado o PDF com as informacoes da franquia internacional
