# language: pt

Funcionalidade: CN0015-Vivo Travel - Ativação via Android

  Cenário: CT0042-Ativar o servico Vivo Travel para o cliente Pos via Android
  Garantir que o sistema disponibilize um novo layout para a página Vivo Travel Roaming Internacional, onde será possível realizar a Ativação do serviço para o cliente

    Dado que eu esteja logado
      | (11) 9 9729-2123 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Roaming - Vivo Travel'
    Entao verifico se o roaming esta desativado
    E clico em 'Ativar o roaming internacional'
    Entao  verifico se o roaming esta ativo

