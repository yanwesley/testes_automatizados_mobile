# language: pt

Funcionalidade:CN0018-Vivo Travel - Fale com a Vivo no Exterior via Android

  Cenário: CT0045-Garantir o Direcionamento para visualizacao da opcao Fale com a Vivo no Exterior via funcionalidade Vivo Travel para o cliente Pos via Android
  Garantir que o sistema disponibilize um novo layout para a página Vivo Travel Roaming Internacional, onde será possivel o acesso ao Fale com a Vivo no Exterior do serviço Vivo Travel para o cliente

    Dado que eu esteja logado
      | (11) 9 9729-2123 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Roaming - Vivo Travel'
    E clico em 'Fale com a Vivo no Exterior'
    Entao verifico carregou corretamente o 'Fale com a Vivo no Exterior'

