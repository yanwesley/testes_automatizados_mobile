# language: pt

Funcionalidade:CN0017-Vivo Travel - Guia de Bolso via Android

  Cenário: T0044-Garantir o Direcionamento para visualizacao do Guia de Bolso via funcionalidade Vivo Travel para o cliente Pos via Android
  Garantir que o sistema disponibilize um novo layout para a página Vivo Travel Roaming Internacional, onde será possivel o acesso ao Guia de Bolso do serviço Vivo Travel para o cliente

    Dado que eu esteja logado
      | (11) 9 9729-2123 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Roaming - Vivo Travel'
    E clico em 'Acesse o Guia de Bolso'
    Entao verifico carregou corretamente o 'Guia de Bolso'

