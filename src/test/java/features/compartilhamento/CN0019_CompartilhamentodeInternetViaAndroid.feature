# language: pt

Funcionalidade: CN0019-Compartilhamento de Internet via Android
  Executar funcionalidade de compartilhamento de Internet

  Esquema do Cenário: <tipo>
    Dado que eu esteja logado com "<numero>" e "<senha>"
    Quando eu clico em 'Envie sua internet disponivel'
    E  compartilho internet numero "<numero_compartilhado>"
    Entao deve exibir a mensagem que foi compartilhado com sucesso "<numero_compartilhado>"
    Exemplos:
      | tipo                                                                                 | numero           | senha  | numero_compartilhado |
      | CT0047-Executar  funcionalidade de compartilhamento de Internet Pre via Android      | (11) 9 9601-2318 | 123456 | (11) 99729-1386      |
      | CT0049-Executar  funcionalidade de compartilhamento de Internet Controle via Android | (11) 9 4127-3255 | 123456 | (11) 99729-1386      |
