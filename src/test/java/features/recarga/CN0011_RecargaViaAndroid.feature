#language: pt

Funcionalidade: CN0011-Recarga via Android

  Cenario: CT0029-Validar Direcionamento a nova página de Recarga via Menu Lateral via Android
  Garantir que o sistema alterou a forma de acesso à página de Recarga, validando que ao acessar a funcionalidade via Menu Lateral o sistema deve direcionar a página do parceiro M4U encapsulada dentro do Site Mobile, direcionando o cliente para contratação de recarga no site parceiro para o cliente
    Dado que eu esteja logado
      | (11) 9 9752-8621 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Faca uma recarga'
    Entao deve exibir as informacoes para efetuar a recarga

  Cenario: CT0030-Validar Direcionamento a nova página de Recarga via Promoções via Android
  Garantir que o sistema alterou a forma de acesso à página de Recarga, validando que ao acessar a funcionalidade via Menu Lateral o sistema deve direcionar a página do parceiro M4U encapsulada dentro do Site Mobile, direcionando o cliente para contratação de recarga no site parceiro para o cliente
    Dado que eu esteja logado
      | (11) 9 7011-0080 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Minha promocao'
    Entao deve exibir as informacoes da minha promocao
    E clico em 'Recarregar agora'
    Entao deve exibir as informacoes para efetuar a recarga