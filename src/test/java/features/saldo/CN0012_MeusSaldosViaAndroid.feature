#language:pt

Funcionalidade: CN0012-Meus Saldos via Android

  Cenario: CT0031-Visualizar Saldo atual disponivel para o cliente Pré via Android
    Dado que eu esteja logado
      | (11) 9 7011-0080 | 123456 |
    Quando acesso a tab saldo
    Entao deve exibir as informacoes do saldo

  Cenario: CT0032-Via Menu Serviços visualizar Saldo atual disponivel para o cliente Controle via Android
    Dado que eu esteja logado
      | (11) 9 9729-2066 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Meus saldos'
    Entao deve exibir as informacoes do saldo via serviços

  Cenario: CT0033-Visualizar Saldo atual Bloqueado para o cliente Pré via Android
    Dado que eu esteja logado
      | (11) 9 9579-5734 | 123456 |
    Quando acesso a tab saldo
    Entao deve exibir as informacoes do saldo bloqueado

  Cenario: CT0034-Via Menu Serviços visualizar Saldo atual Bloqueado para o cliente Controle via Android
  numero invalido verificando com a Dayana um novo numero.
    Dado que eu esteja logado
      | (11) 9 9729-6105 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Meus saldos'
    Entao deve exibir as informacoes do saldo bloqueado via serviços