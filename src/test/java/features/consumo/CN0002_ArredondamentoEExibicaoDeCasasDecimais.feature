#CN0002-Consumo - Arredondamento e exibição de casas decimais

  # language: pt

Funcionalidade: CN0002-Consumo - Arredondamento e exibição de casas decimais

  Esquema do Cenário: <tipo>
    Dado que eu esteja logado com "<numero>" e "<senha>"
    Entao deve exibir o arredondamento conforme o tipo "<tipo_de_dados>"
    Exemplos:
      | tipo                                                                              | numero           | senha  | tipo_de_dados |
#      | CT0001-Validar arredondamento e exibição de casas decimais - Pré (GB)             | (11) 9 4104-8167 | 123456 | GB            |
#      | CT0002-Validar arredondamento e exibição de casas decimais - Pós Legado (GB)      | (11) 9 9729-2440 | 123456 | GB            |
      | CT0003-Validar arredondamento e exibição de casas decimais - Controle Legado (MB) | (11) 9 9729-6105 | 123456 | MB            |


