#language: pt

Funcionalidade: CN0005-Consumo de Internet Franquia Controle via Android

  Cenario: CT0013-Visualizar layout do Consumo Total da Franquia de Internet para o cliente Controle via Android
    Dado que eu esteja logado
      | (11) 9 9729-9616 | 123456 |
    Entao valido a linha que estou acessando
    E acesso a tab consumo
    E valido a exibição do layout de consumo total da franquia
#
#  Cenario: CT0014-Visualizar layout do Consumo Parcial da Franquia de Internet para o cliente Controle via Android
#    Dado que eu esteja logado
#      | (11) 9 9729-9620 | 123456 |
#    Entao valido a linha que estou acessando
#    E acesso a tab consumo
#    E valido a exibição do layout de consumo parcial da franquia

#  Cenario: CT0015-Visualizar layout SEM Consumo da Franquia de Internet para o cliente Controle via Android
#    Dado que eu esteja logado
#      | (11) 9 9729-9608 | 123456 |
#    Entao valido a linha que estou acessando
#    E acesso a tab consumo
#    E valido a exibição do layout de SEM consumo da franquia