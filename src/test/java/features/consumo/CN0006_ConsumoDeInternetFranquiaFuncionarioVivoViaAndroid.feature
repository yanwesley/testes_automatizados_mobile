#language: pt

Funcionalidade: CN0006-Consumo de Internet Franquia Funcionário Vivo via Android

  Cenario: CT0016-Visualizar layout do Consumo da Franquia de Internet para Funcionário Vivo via Android
    Dado que eu esteja logado
      | (11) 9 9729-6724 | 123456 |
    Entao valido a linha que estou acessando
    E acesso a tab consumo
    E valido a exibição do layout de consumo