#language: pt

Funcionalidade: CN0007-Consumo de Internet Franquia Multivivo via Android

  Cenario: CT0017-Visualizar layout do Consumo Parcial da Franquia de Internet para o cliente Multivivo Titular via Android
  | (11) 9 9729-1444 | 123456 |

  Cenario: CT0018-Visualizar layout SEM Consumo da Franquia de Internet para o cliente Multivivo Titular via Android
  | (11) 9 9729-1444 | 123456 |