# language: pt
#
Funcionalidade: CN0005-Gerar Código de Barras - Copiar Código de Barras Em Aberto via Android

  Esquema do Cenario: <cenario>

    Dado que eu esteja logado com "<numero>" e "<senha>"
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao deve exibir o card da conta atual e seu status aberta para Controle/Pos via Android
    Quando eu clico em 'Código de barras'
    Entao deve exibir os detalhes do codigo de barras
    E ao clicar em 'Copiar código de barras'
    Entao deve exibir a mensagem que foi copiado com sucesso

    Exemplos:
      | cenario                                                                   | numero           | senha  |
      | CT0031-Copiar Código de Barras Em Aberto Plano Atual Controle via Android | (11) 9 4127-4953 | 123456 |
      | CT0034-Copiar Código de Barras Em Aberto Plano Atual Pós via Android      | (11) 9 4127-3203 | 123456 |
