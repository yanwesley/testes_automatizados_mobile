# language: pt

Funcionalidade: CN0008-Gerar Código de Barras - Enviar por SMS Atrasada via Android

  Esquema do Cenario: <cenario>

    Dado que eu esteja logado com "<numero>" e "<senha>"
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao deve exibir o card da conta atual e seu status atrasada para Controle/Pos via Android
    Quando eu clico em 'Código de barras'
    Entao deve exibir os detalhes do codigo de barras
    E ao clicar em 'Enviar por SMS'
    Entao deve exibir a mensagem que foi enviado com sucesso para o numero "<numeroSms>"

    Exemplos:
      | cenario                                                            | numero           | senha  | numeroSms       |
      | CT0063-Enviar por SMS Atrasada do Plano Atual Controle via Android | (11) 9 4127-4953 | 123456 | (11) 94127-4953 |
      | CT0065-Enviar por SMS Atrasada do Plano Atual Pós via Android      | (11) 9 4127-3203 | 123456 | (11) 94127-3203 |
