# language: pt
#CN0010-Gerar Código de Barras - Já Pagou - Contestada via Android

Funcionalidade: CN0010-Gerar Código de Barras - Já Pagou - Contestada via Android

  Esquema do Cenario: <cenario>

    Dado que eu esteja logado com "<numero>" e "<senha>"
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao deve exibir o card da conta atual e seu status atrasada para Controle/Pos via Android
    Quando eu informar que já paguei
    Entao deve exibir um modal para confirmação para fatura contestada Controle/Pos

    Exemplos:
      | cenario                                                                 | numero           | senha  |
      | CT0091-Informar Já Pagou Contestada do Plano Atual Controle via Android | (11) 9 4127-4953 | 123456 |
      | CT0094-Informar Já Pagou Contestada do Plano Atual Pós via Android      | (11) 9 4127-3203 | 123456 |

