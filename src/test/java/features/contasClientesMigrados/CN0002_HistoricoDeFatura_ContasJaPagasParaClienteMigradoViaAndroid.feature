# language: pt
#
Funcionalidade: CN0002-Histórico de Fatura - Contas Já Pagas para cliente Migrado via Android

#  Esquema do Cenário: <tipo>
#  Como cliente desejo acessar funcionalidade de Contas Pagas e visualizar as faturas Pagas separadas pela descrição Plano Atual e Plano Anterior e acima no menu principal terá em Contas a visualização das últimas 6 faturas também separadas pela descrição Plano Atual e Plano Anterior.
#
#    Dado que eu esteja logado com "<numero>" e "<senha>"
#    Entao valido a linha que estou acessando
#    Quando acesso a tab conta
#    Entao deve exibir o menu 'Contas já pagas'
#
#    Exemplos:
#      | tipo                                                                                | numero           | senha  |
#      | CT0010-Visualizar histórico de Faturas Pagas do Plano Atual Controle via Android    | (11) 9 4127-4953 | 123456 |
#      | CT0011-Visualizar histórico de Faturas Pagas do Plano Atual Funcionário via Android | (11) 9 4127-3213 | 123456 |
#      | CT0012-Visualizar histórico de Faturas Pagas do Plano Atual Pós via Android         | (11) 9 4127-3203 | 123456 |
#
#
#  Cenario: CT0013-Visualizar histórico de Faturas Pagas do Plano Atual Pré via Menu Plano Anterior via Android
#    Dado  que eu esteja logado
#      | (11) 9 4127-3209 | 123456 |
#    Entao valido a linha que estou acessando
#    Quando acesso a tab servicos
#    E clico em 'Plano anterior'
#    Entao deve exibir o menu 'Contas já pagas'

  Cenario: CT0014-Visualizar histórico de Fatura Paga Contestada do Plano Atual Pré via Menu Plano Anterior via Android
    Dado  que eu esteja logado

      | (11) 9 9729-7456 | 123456 |
#      | (11) 9 4127-4878 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab servicos
    E clico em 'Plano anterior'
    Entao deve exibir o menu 'Contas já pagas'
