# language: pt

Funcionalidade: CN0012-Gerar Código de Barras - Boleto Atualizado - Contestada via Android

  Esquema do Cenario: <cenario>

    Dado que eu esteja logado com "<numero>" e "<senha>"
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao deve exibir o card da conta atual e seu status atrasada para Controle/Pos via Android
    Quando eu solicitar o boleto atualizado
    Entao deve ser gerado o pdf atualizado

    Exemplos:
      | cenario                                                            | numero           | senha  |
      | CT0071-Gerar Boleto Atualizada do Plano Atual Controle via Android | (11) 9 4127-4953 | 123456 |
      | CT0074-Gerar Boleto Atualizada do Plano Atual Pós via Android      | (11) 9 4127-3203 | 123456 |
