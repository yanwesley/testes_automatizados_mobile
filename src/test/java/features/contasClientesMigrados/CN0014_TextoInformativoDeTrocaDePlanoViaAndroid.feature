# language: pt

Funcionalidade: CN0014-Texto Informativo de Troca de Plano via Android
  Garantir que o sistema disponibilizará o nome do Plano do cliente na funcionalidade

  Esquema do Cenário: <tipo>
    Dado que eu esteja logado com "<numero>" e "<senha>"
    Quando acesso a tab conta
    Entao deve exibir o texto informativo apos migracao
#    as vezes é necessário atualizar a página do app, para exibir a notificação
    Exemplos:
      | tipo                                                                               | numero           | senha  |
      | CT0103-Visualizar Texto Informativo após troca do Plano Atual Controle via Android | (11) 9 4127-4953 | 123456 |
      | CT0104-Visualizar Texto Informativo após troca do Plano Atual Pós via Android      | (11) 9 4127-3203 | 123456 |

