# language: pt
#CN0006-Gerar Código de Barras - Enviar por E-mail Atrasada via Android

Funcionalidade: CN0006-Gerar Código de Barras - Enviar por E-mail Atrasada via Android

  Esquema do Cenario: <cenario>

    Dado que eu esteja logado com "<numero>" e "<senha>"
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao deve exibir o card da conta atual e seu status atrasada para Controle/Pos via Android
    Quando eu clico em 'Código de barras'
    Entao deve exibir os detalhes do codigo de barras
    E ao clicar em 'Enviar por Email'
    Entao deve exibir a mensagem que foi copiado com sucesso

    Exemplos:
      | cenario                                                                      | numero           | senha  |
      | CT0047-Enviar 2a via Atrasada por e-mail do Plano Atual Controle via Android | (11) 9 4127-4953 | 123456 |
      | CT0051-Enviar 2a via Atrasada por e-mail do Plano Atual Pós via Android      | (11) 9 4127-3203 | 123456 |
