# language: pt

Funcionalidade: CN0001-Histórico de Fatura - Layout para cliente Migrado via Android
  Garantir que o sistema disponibilizará o nome do Plano do cliente na funcionalidade

  Esquema do Cenário: <tipo>
    Dado que eu esteja logado com "<numero>" e "<senha>"
    Quando acesso a tab conta
    Entao deve exibir as faturas do plano atual e do plano anterior

    Exemplos:
      | tipo                                                                                                | numero           | senha  |
      | CT0001-Visualizar histórico de Faturas para cliente Migrado com Plano Atual Controle via Android    | (11) 9 4127-4953 | 123456 |
      | CT0002-Visualizar histórico de Faturas para cliente Migrado com Plano Atual Funcionário via Android | (11) 9 4127-3213 | 123456 |
      | CT0003-Visualizar histórico de Faturas para cliente Migrado com Plano Atual Pós via Android         | (11) 9 4127-3203 | 123456 |


  Cenario: CT0004-Visualizar histórico de Faturas para cliente Migrado com Plano Atual Pré via Menu Plano Anterior via Android
    Dado  que eu esteja logado
      | (11) 9 4127-3209 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Plano anterior'
    Entao deve exibir as faturas do plano anterior
