# language: pt

Funcionalidade: CN0031-Nome do Plano via funcionalidade no Meu Vivo
  Garantir que o sistema disponibilizará o nome do Plano do cliente na funcionalidade

  Esquema do Cenário: <tipo>
    Dado que eu esteja logado com "<numero>" e "<senha>"
    Quando acesso a tab servicos
    E clico em 'Detalhe do Plano'
    Entao verifico o nome e detalhes do plano "<nomeEsperado>"
    Exemplos:
      | tipo                                                                              | numero           | senha  | nomeEsperado                    |
      | CT0063-Visualizar nome do Plano na funcionalidade do cliente Pré no Meu Vivo      | (11) 9 9579-5735 | 123456 | VIVO PRÉ DIÁRIO                 |
      | CT0064-Visualizar nome do Plano na funcionalidade do cliente Pós no Meu Vivo      | (11) 9 9729-0047 | 123456 | VIVO POS MIG 7GB                |
      | CT0065-Visualizar nome do Plano na funcionalidade do cliente Controle no Meu Vivo | (11) 9 4127-3994 | 123456 | VIVO CONTROLE DIGITAL-5GB ILIM_ |
