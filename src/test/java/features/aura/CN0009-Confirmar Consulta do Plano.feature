# language: pt

Funcionalidade: CN0009-Confirmar Consulta do Plano

#  Cenário: CT0020-Acessar Vivi e realizar consulta do Consumo  para cliente Controle via Android
#    Dado que eu esteja logado
#      | (11) 9 9729-1386 | 123456 |
#    Quando acesso a AURA
#    E pesquiso o que quero saber
#      | meu consumo |
##    Entao deve exibir o consumo do meu plano
#
#  Cenário: CT0012-Acessar Vivi e realizar consulta do Saldo para cliente Pre via Android
#    Dado que eu esteja logado
#      | (11) 9 9579-0028 | 123456 |
#    Quando acesso a AURA
#    E pesquiso o que quero saber
#      | meu saldo |
#    Entao deve exibir o meu saldo

  Cenário: CT0006-Acessar Vivi e realizar consulta do Plano para cliente Pós
    Dado que eu esteja logado
      | (11) 9 9729-2123 | 123456 |
    Quando acesso a AURA
    E pesquiso o que quero saber
      | meu plano |
    Entao deve exibir o meu plano



