# language: pt

Funcionalidade: CN0001-Agendamento via Android
  Garantir que o sistema disponibilize uma nova funcionalidade para agendamento de visitas em lojas Sendo possivel Buscar Lojas mais próximas por geolocalização

  Contexto:
    Dado que eu esteja logado
      | (11) 9 9729-2123 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Agendar atendimento'

  Cenário: CT0001-Visualizar Lojas mais proximas por geolocalizacao para agendamento de visita para o cliente via Android
  Garantir que o sistema disponibilize uma nova funcionalidade para agendamento de visitas em lojas Sendo possivel Buscar Lojas mais próximas por geolocalização
    Entao deve exibir as lojas proximas por geolocalizacao

  Cenário: CT0002-Agendar visita na loja Selecionando o Dia a Hora e Assunto para o cliente via Android
  Garantir que o sistema disponibilize uma nova funcionalidade para agendamento de visitas em lojas Sendo possivel Agendar visita na loja, selecionando dia, hora e assunto
    Entao deve exibir as lojas proximas por geolocalizacao
    E preencho os dados para o agendamento
    Entao deve ser agendando com sucesso

  Cenário: CT0003-Cancelar agendamentos realizados para o cliente via Android
  Garantir que o sistema disponibilize uma nova funcionalidade para agendamento de visitas em lojas Sendo possivel Cancelar agendamentos realizados
    Entao deve exibir as lojas proximas por geolocalizacao
    E visualizo meus agendamentos
    E cancelo o agendamento
    Entao deve ser cancelado com sucesso
