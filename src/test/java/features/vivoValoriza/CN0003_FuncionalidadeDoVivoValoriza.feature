#language:pt

Funcionalidade: CN0003-Funcionalidade do Vivo Valoriza

  Esquema do Cenário: <tipo>
    Dado que eu esteja logado com "<numero>" e "<senha>"
    Quando acesso a tab servicos
    E clico em 'Vivo Valoriza'
    Entao deve exibir as informacoes do vivo valoriza
    E clico em 'Meu perfil no Vivo Valoriza'
    Entao deve exibir os perfil Vivo Valoriza
    Exemplos:
      | tipo                                                                                 | numero           | senha  |
      | CT0008-Garantir o direcionamento para Cliente Controle com Cadastro no Vivo Valoriza | (11) 9 9752-8621 | 123456 |
      | CT0010-Garantir o direcionamento para Cliente Pós-Pago com Cadastro no Vivo Valoriza | (11) 9 9729-0047 | 123456 |

  Cenario: CT0047-Confirmar indisponibilidade do Vivo Valoriza para PRÉ (Negativo)
    Dado que eu esteja logado
      | (11) 9 9579-5734 | 123456 |
    Quando acesso a tab servicos
    Entao nao deve exibir a opcao 'Vivo Valoriza'


  Esquema do Cenário: <tipo>
    Dado que eu esteja logado
      | (11) 9 4127-4089 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Vivo Valoriza'
    Entao deve exibir as informacoes do vivo valoriza para não cadastrados

    Exemplos:
      | tipo                                                                                                 |
      | CT0117-Garantir o direcionamento para Vivo Valoriza com Cliente Sem Cadastro                         |
      | CT0120-Confirarmar a exibição do Tutorial de Boas Vindas para Vivo Valoriza com Cliente Sem Cadastro |

  Cenario: CT0130-Garantir o direcionamento de Parceiros
    Dado que eu esteja logado
      | (11) 9 9752-8621 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Vivo Valoriza'
    Entao deve exibir as informacoes do vivo valoriza
    E clico na categoria 'Descontos'
    Entao deve exibir os parceiros da categoria selecionada
    E clico no primerio parceiro
    Entao  deve exibir os detalhes do parceiro e o link do site
    E clico no link do site
    Entao  deve exibir o site do parceiro






