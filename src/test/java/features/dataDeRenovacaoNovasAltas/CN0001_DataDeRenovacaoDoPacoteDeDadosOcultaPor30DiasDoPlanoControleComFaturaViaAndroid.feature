# language: pt

Funcionalidade: CN0001-Data de Renovação do Pacote de Dados Oculta por 30 Dias do Plano Controle com Fatura via Android

  Cenario: CT0001-Verificar data de renovação do pacote de dados do plano Oculta por 30 dias após Alta da linha Controle com Fatura via Android
    Dado que eu esteja logado
      | (11) 9 4127-3255 | 123456 |
    Entao valido a linha que estou acessando
    E valido que não está sendo exibida a data de renovação do plano


  Cenario: CT0003-Verificar data de renovação do pacote de dados do plano Oculta por 30 dias após migração para Controle com Fatura via Android
    Dado que eu esteja logado
      | (11) 9 4127-3980 | 123456 |
    Entao valido a linha que estou acessando
    E valido que não está sendo exibida a data de renovação do plano