#language:pt
Funcionalidade: CN0001 - Apresentação do Modal e Ícone de Maiores Informações da Regua de Cobrança
  Para o cliente compreender a diferença entre data de renovação de franquia e data de vencimento da conta para gerenciar melhor a utilização dos meus dados e pagamento de faturas, precisamos alterar o jeito que apresentamos as informações para os clientes.

  Deve ser verificado nos 2 serviços abaixo já consumidos pela aplicação se a tag "codigo" a logica já existente para saber se o pacote é do plano ou é um pacote avulso.

  Caso seja um pacote avulso deve ser mantido a formatação atual, caso seja um pacote do plano, deve ser alterado para "Renova em dd/mm (info)" (junto com o ícone)

  Para realizar esta alteração continuaremos chamando os 2 serviços já utilizados hoje

  Cenario: CT0015-Verificar alteração do texto e apresentação do ícone de maiores informações na tela de Consumo do App Android para Pós
    Dado que eu esteja logado
      | (11) 9 9729-2077 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'

  Cenario: CT0016-Verificar apresentação do modal na tela de Consumo do App Android para Multivivo
    Dado que eu esteja logado
      | (11) 9 4127-3347 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'
    E deve exibir os dados de consumo 'Multivivo'

  Cenario: CT0017-Verificar alteração do texto e apresentação do ícone de maiores informações na tela de Consumo do App Android para Multivivo
    Dado que eu esteja logado
      | (11) 9 4127-3347 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'
    E deve exibir os dados de consumo 'Multivivo'
    E eu clico em 'Mais Detalhes'
    Entao deve exibir o modal de maiores informações

  Cenario: CT0021-Verificar apresentação do modal na tela de Consumo do App Android para Pós
    Dado que eu esteja logado
      | (11) 9 4127-3347 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'

  Cenario: CT0032-Verificar alteração do texto e apresentação do ícone de maiores informações na tela de Meus saldos do App Android para Controle
    Dado que eu esteja logado
      | (11) 9 9752-8621 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'

  Cenario:CT0035-Verificar apresentação do modal na tela de Meus saldos para Controle
    Dado que eu esteja logado
      | (11) 9 9729-7231 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'
    E eu clico em 'Mais Detalhes'
    Entao deve exibir o modal de maiores informações

  Cenario: CT0047-Verificar alteração do texto e apresentação do ícone de maiores informações na tela de Meu plano do App Android para Funcionário
    Dado que eu esteja logado
      | (11) 9 9729-1459 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'

  Cenario:CT0053-Verificar apresentação do modal na tela de Meu plano do App Android para Funcionário
    Dado que eu esteja logado
      | (11) 9 4127-3213 | 123456 |
    Entao deve exibir o texto do plano 'Renova em dd/mm'
    E eu clico em 'Mais Detalhes'
    Entao deve exibir o modal de maiores informações
