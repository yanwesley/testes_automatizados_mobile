# language: pt

Funcionalidade: CN0022-Lançamentos Futuros para Promoção via Android

  Contexto:
    Dado que eu esteja logado
      | (11) 9 7202-0181 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab servicos
    E clico em 'Lançamentos Futuros'

  Cenario: CT0060-Visualizar layout Fechado em Lançamentos Futuros referente a Promoção via Android
    Entao deve exibir os lancamentos futuros com layout fechado

  Cenario: CT0061-Visualizar layout Aberto em Lançamentos Futuros referente a Promoção via Android
    Quando expandir os detalhes dos lancamentos futuros da promocao
    Entao deve exibir os lancamentos futuros com layout aberto da promocao

  Cenario: CT0062-Visualizar detalhe dos Lançamentos Futuros referente a Hoje se houver saldo Promoção via Android
    Quando expandir os detalhes dos lancamentos futuros da promocao
    Então deve exibir a label 'Hoje, se houver saldo'
