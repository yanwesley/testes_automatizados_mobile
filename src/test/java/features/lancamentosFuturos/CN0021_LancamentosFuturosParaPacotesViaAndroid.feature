# language: pt

Funcionalidade: CN0021-Lançamentos Futuros para Pacotes via Android

  Contexto:
    Dado que eu esteja logado
      | (11) 9 7011-5446 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab servicos
    E clico em 'Lançamentos Futuros'

  Cenario: CT0057-Visualizar layout Fechado em Lançamentos Futuros referente a Pacotes via Android
    Entao deve exibir os lancamentos futuros com layout fechado

  Cenario: CT0058-Visualizar layout Aberto em Lançamentos Futuros referente a Pacotes via Android
    Quando expandir os detalhes dos lancamentos futuros
    Entao   deve exibir os lancamentos futuros com layout aberto

  Cenario: CT0059-Visualizar detalhe dos Lançamentos Futuros referente a Hoje se houver saldo Pacotes  via Android
    Quando expandir os detalhes dos lancamentos futuros
    Entao   deve exibir os lancamentos futuros com layout aberto