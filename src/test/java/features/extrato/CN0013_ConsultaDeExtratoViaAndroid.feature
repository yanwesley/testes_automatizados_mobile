#language:pt

Funcionalidade: CN0013-Consulta de Extrato via Android

#  Cenario: CT0035-Realizar Consulta de Extrato com saldo disponível para Controle via Android
#    Dado que eu esteja logado
#      | (11) 9 9729-2066 | 123456 |
#    Quando acesso a tab servicos
#    E clico em 'Meus saldos'
#    Entao deve exibir as informacoes do saldo via serviços
#    E clico em 'Ver extrato detalhado'
#    Entao deve exibir as informacoes do extrato
#
#  Cenario: CT0036-Realizar Consulta de Extrato com saldo disponível para Pré via Android
#  Cliente deseja acessar seus saldo e após redirecionamento poder realizar Consulta de seu Extrato demonstrando o saldo disponível para Pré
#    Dado que eu esteja logado
#      | (11) 9 7011-0080 | 123456 |
#    Quando acesso a tab saldo
#    Entao deve exibir as informacoes do saldo
#    E clico em 'Ver extrato detalhado'
#    Entao deve exibir as informacoes do extrato

  Cenario: CT0037-Realizar Consulta de Extrato com saldo Bloqueado para Controle via Android
    Dado que eu esteja logado
      | (11) 9 9729-7436 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Meus saldos'
    Entao deve exibir as informacoes do saldo bloqueado via serviços
    E clico em 'Ver extrato detalhado'
    Entao deve exibir as informacoes do extrato pre com saldo bloqueado

#  Cenario: CT0038-Realizar Consulta de Extrato com saldo Bloqueado para Pré via Android
#    Dado que eu esteja logado
#      | (11) 9 9579-5734 | 123456 |
#    Quando acesso a tab saldo
#    Entao deve exibir as informacoes do saldo bloqueado
#    E clico em 'Ver extrato detalhado'
#    Entao deve exibir as informacoes do extrato pre com saldo bloqueado
#
#
