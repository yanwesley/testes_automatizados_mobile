#language:pt

Funcionalidade: CN0051-Migração Controle sem cadastro na FOPA para Pós-pago sem aquisição Via meu vivo PF
  Validar correto funcionamento da feature de migração de clientes para novo plano

  Cenario: CT0113-Realizar migração de cliente Controle para Pós-pago com aquisição
    Dado que eu esteja logado
      |(11) 9 9601-0628|123456|
    Quando acesso a tab servicos
    Entao clico em 'Troca de plano'
    E valido que possuo um plano controle
    Entao na tela de planos escolho nova opção de plano pós-pago
    E confirmo que quero trocar de plano
    E confirmo os dados da compra
    E valido 'Resumo da Compra'
    Entao escolho endereço de cobrança
    Entao valido tela de conclusão com aquisição pendente

#  Cenario: CT0114-Validar o pedido no Hybris - Web
#    Dado que eu esteja logado no Hybris
#    Quando acesso o menu 'Order'
#    E pesquiso pelo número do pedido
#    Então deve exibir o pedido como resultado da consulta
#    E ao clicar para acessar os detalhes do pedido
#    Entao deve exibir os detalhes do pedido

