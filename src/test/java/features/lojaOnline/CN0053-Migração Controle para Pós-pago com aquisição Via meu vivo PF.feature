#language:pt

Funcionalidade: CN0053-Migração Controle para Pós-pago com aquisição Via meu vivo PF
  Validar correto funcionamento da feature de migração de clientes para novo plano

  Cenario: CT0117-Realizar migração de cliente Controle para Pós-pago com aquisição
    Dado que eu esteja logado
      |(11) 9 9729-3536 | 123456|
    Quando acesso a tab servicos
    Entao clico em 'Troca de aparelho'
    E seleciono um aparelho
    E escolho um plano Pós Pago

#    Entao na tela de planos escolho nova opção de plano pós-pago
#    Entao escolho endereço de cobrança
#    E seleciono a opção 'Trocar de plano Cliente Vivo'
#    Entao na tela de planos escolho nova opção de plano pós-pago
#    E preencho os dados do comprador
#    Entao aceito os termos de uso e concluo a aquisição
#
  Cenario: CT0118-Validar o pedido no Hybris - Web
    Dado que eu esteja logado no Hybris
    Quando acesso o menu 'Order'
    E pesquiso pelo número do pedido
    Então deve exibir o pedido como resultado da consulta
    E ao clicar para acessar os detalhes do pedido
    Entao deve exibir os detalhes do pedido