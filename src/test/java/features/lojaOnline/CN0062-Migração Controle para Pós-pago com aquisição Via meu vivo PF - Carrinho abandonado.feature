#language:pt

Funcionalidade: CN0062-Migração Controle para Pós-pago com aquisição Via meu vivo PF - Carrinho abandonado
  Validar correto funcionamento da feature de migração de clientes abandonando o carrinho de compras

#  Cenario: CT0141-Realizar migração de cliente Controle para Pós-pago com aquisição
#    Dado que eu esteja logado
#      | (11) 9 9601-0455 | 123456 |
#    Entao valido que possuo um plano controle
#    E clico em 'Serviços'
#    Entao clico em 'Troca de plano'
#    Entao realizo cadastro na loja virtual
#    E seleciono a opção 'Trocar de plano Cliente Vivo'
#    Entao na tela de planos escolho nova opção de plano pós-pago
#    E fecho o aplicativo
#    Entao abro o aplicativo e retorno para a tela do carrinho
#    E valido que o item continua no carrinho
#    E preencho os dados do comprador
#    Entao aceito os termos de uso e concluo a aquisição

  Cenario: CT0142-Validar o pedido no Hybris - Web
    Dado que eu esteja logado no Hybris
    Quando acesso o menu 'Order'
    E pesquiso pelo número do pedido
    Então deve exibir o pedido como resultado da consulta
    E ao clicar para acessar os detalhes do pedido
    Entao deve exibir os detalhes do pedido