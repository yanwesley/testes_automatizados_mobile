# language: pt

Funcionalidade: CN0065_Funcionalidade_Sair
  Executar a funcionalidade de Sair do Meu Vivo

  Cenário: CT0163-Executar a funcionalidade de Sair do Meu Vivo
    Dado que eu esteja logado
      | (11) 9 4127-3149 | 123456 |
    Quando realizo o logout
    Entao deve exibir a tela welcome