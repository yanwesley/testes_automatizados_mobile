# language: pt

Funcionalidade: CN0007_LoginMeuVivoMovel
  Essa funcionalidade testa o login do  usuario com diversos tipos de contas

  Esquema do Cenário: <tipo> Deve exibir a página inicial ao logar com usuario
    Dado que eu esteja na tela welcome
    Quando realizo as validacoes para entrar com o "<numero>" do telefone
    E entro com a "<senha>" correta
    Entao deve logar com sucesso

    Exemplos:
      | tipo                                                                   | numero           | senha  |
      | CT0019-Realizar Login por Numero de Linha OTP para cliente Pré         | (11) 9 9601-2298 | 123456 |
#      | CT0020-Realizar Login por Numero de Linha OTP para cliente Controle    | (11) 9 9752-8621 | 123456 |
#      | CT0021-Realizar Login por Numero de Linha OTP para cliente Pós         | (11) 9 9729-0047 | 123456 |
#      | CT0022-Realizar Login por Numero de Linha OTP para cliente Funcionário | (11) 9 4127-3360 | 123456 |
