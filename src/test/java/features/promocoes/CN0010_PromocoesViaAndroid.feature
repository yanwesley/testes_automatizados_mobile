#language:pt

Funcionalidade: CN0010-Promoções via Android

  Contexto:
    Dado que eu esteja logado
      | (11) 9 9501-4426 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab servicos
    E clico em 'Minha promocao'

  Cenario: CT0025-Visualizar benefício Android principais de sua Promoção Vivo Turbo via Android
    Entao deve exibir as informacoes da minha promocao para plano pre

  Cenario: CT0028-Realizar o Cancelamento da Promoção Vivo Turbo via Android
    Entao deve exibir as informacoes da minha promocao para plano pre
    Quando clico em 'Entenda a promoção'
    Entao deve exibir os detalhes da promoção
    E ao cancelar a sua promoção
    Entao deve exibir um modal para confirmar a desativação da promoção
    E ao confirmar o cancelamento
    Entao deve exibir o modal de 'Ok, agora é só aguardar' para cancelamento

  Cenario: CT0026-Realizar a Contratação da Promoção Vivo Turbo via Android
    Quando clico em 'Conheça outras promoções'
    Entao deve exibir as demais promoções
    E ao contratar uma nova promoção
    E ao confirmar a promoção
    Entao deve exibir o modal de 'Ok, agora é só aguardar' para contratação

  Cenario: CT0027-Realizar a Antecipação do pagamento da Promoção Vivo Turbo via Android
    Entao deve exibir as informacoes da minha promocao para plano pre
    Quando clico em 'Antecipar renovação'
    Entao deve exibir o modal de confirmação da antecipação
    E ao confirmar a antecipação
    Entao deve exibir o modal de 'Ok, você vai receber um SMS de confirmação'
