#language: pt

Funcionalidade:CN0032-Menu Conta - Conta Funcionário via Android

  Contexto:
    Dado que eu esteja logado
      | (11) 9 4127-3213 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao deve exibir o card da conta e seu status atrasada para Funcionário via Android

  Cenario: CT0063-Visualizar card da Conta Funcionário via Android
  Esse cenario está  sendo validado no contexto, pois é utilizado para os demais testes.

  Cenario: CT0065-Solicitar 2a via detalhada para Conta Funcionário via Android
    Quando eu solicitar a segunda via detalhada de conta Funcionário atrasada
    Entao deve ser gerado o pdf detalhado
