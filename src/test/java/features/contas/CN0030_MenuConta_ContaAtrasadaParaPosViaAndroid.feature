#language: pt
Funcionalidade: CN0030-Menu Conta - Conta Atrasada para Pós via Android

  Contexto:
    Dado que eu esteja logado
      | (11) 9 9729-2071 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao deve exibir o card da conta e seu status atrasada para Controle/Pos via Android

  Cenario: CT0063-Visualizar card da Conta Atrasada para Pós via Android
  Esse cenario está  sendo validado no contexto, pois é utilizado para os demais testes.

  Cenario: CT0065-Solicitar 2a via detalhada para Conta Atrasada para Pós via Android
    Quando eu solicitar a segunda via detalhada
    Entao deve ser gerado o pdf detalhado

  Cenario: CT0066-Solicitar Já pagou para Conta Atrasada para Pós via Android
    Quando eu informar que já paguei
    Entao  deve exibir um modal para confirmação para fatura em atraso conta Pos

  Cenario: CT0067-Copiar código de barras da Conta Atrasada para Pós via Android
    Quando eu clico em 'Código de barras'
    Entao deve exibir os detalhes do codigo de barras
    E ao clicar em 'Copiar código de barras'
    Entao deve exibir a mensagem que foi copiado com sucesso

  Cenario: CT0068-Enviar por SMS o código de barras da Conta Atrasada para Pós via Android
    Quando eu clico em 'Código de barras'
    Entao deve exibir os detalhes do codigo de barras
    E ao clicar em 'Enviar por SMS'
    Entao deve exibir a mensagem que foi enviado com sucesso
      | (11) 99729-2071 |