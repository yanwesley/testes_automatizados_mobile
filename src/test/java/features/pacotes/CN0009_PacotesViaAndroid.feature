# language: pt

Funcionalidade: CN0009-Pacotes via Android

  Cenario: CT0021-Realizar Contratação de um Pacote de Serviço para Pré via Android
    Dado que eu esteja logado
      | (11) 9 9501-4327 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab servicos
    E clico em 'Pacotes'
    Então deve exibir a pagina de pacotes para pre
    E ao contratar um novo pacote pre
    Entao deve ser contratado com sucesso um novo pacote pre

#  Cenario: CT0022-Realizar Desativação de um Pacote de Serviço para Pré via Android
#    Dado que eu esteja logado
#      | (11) 9 9501-4327 | 123456 |
#    Entao valido a linha que estou acessando
#    Quando acesso a tab servicos
#    E clico em 'Pacotes'
#    Então deve exibir a pagina de pacotes
#    E ao desabilitar um pacote
#    Entao deve ser desabilitado com sucesso
##
#  Cenario: CT0023-Realizar Contratação de um Pacote de Serviço para Controle via Saldo no Android
#    Dado que eu esteja logado
#      | (11) 9 9729-1391 | 123456 |
#    Entao valido a linha que estou acessando
#    Quando acesso a tab servicos
#    E clico em 'Pacotes'
#    Então deve exibir a pagina de pacotes
#    E ao contratar um novo pacote
#    Entao deve ser contratado com sucesso
#
#  Cenario: CT0024-Realizar Contratação de um Pacote de Serviço para Controle via Fatura no Android
#    Dado que eu esteja logado
#      | (11) 9 9729-9273 | 123456 |
#    Entao valido a linha que estou acessando
#    Quando acesso a tab servicos
#    E clico em 'Pacotes'
#    Então deve exibir a pagina de pacotes
#    E ao contratar um novo pacote
#    Entao deve ser contratado com sucesso
#
#  Cenario: CT0025-Realizar desistência na Contratação de um Pacote de Serviço para Controle via Fatura via Android
#    Dado que eu esteja logado
#      | (11)9 9501-4327 | 123456 |
#    Entao valido a linha que estou acessando
#    Quando acesso a tab servicos
#    E clico em 'Pacotes'
#
#  Cenario: CT0026-Realizar Desativação de um Pacote de Serviço para Controle  via Android
#    Dado que eu esteja logado
#      | (11) 9 9729-1391 | 123456 |
#    Entao valido a linha que estou acessando
#    Quando acesso a tab servicos
#    E clico em 'Pacotes'
#    Então deve exibir a pagina de pacotes
#    E ao desabilitar um pacote
#    Entao deve ser desabilitado com sucesso