# language: pt

Funcionalidade: CN0016-Detalhamento do filtro de consumo dos dados por período para Clientes Pos Via App Android

  Cenario: CT0061-Verificar o detalhamento de consumo por periodo para mês atual para cliente Pos via App Android
  Garantir o fluxo de direcionamento da Funcinonalidade Vivo Valoriza na Home do Meu Vivo.
    Dado que eu esteja logado
      | (11) 9 9729-0130 | 123456 |
    Quando clico em 'Detalhamento do consumo de internet' pos
    Entao deve exibir o tooltip informando que o cliente não tem consumo