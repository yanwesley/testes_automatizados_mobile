#language: pt

Funcionalidade: CN0012-Detalhamento do filtro de consumo dos dados por período para  Clientes Controle Fatura Via App Android

  Cenario: CT0053-Verificar o detalhamento de consumo  para cliente  Controle Fatura sem consumo  via App Android
  Garantir o fluxo de direcionamento da Funcinonalidade Vivo Valoriza na Home do Meu Vivo.

    Dado que eu esteja logado
      | (11) 9 9729-2066 | 123456 |
    Quando clico em 'Detalhamento do consumo de internet'
    Entao deve exibir o tooltip informando que o cliente não tem consumo
