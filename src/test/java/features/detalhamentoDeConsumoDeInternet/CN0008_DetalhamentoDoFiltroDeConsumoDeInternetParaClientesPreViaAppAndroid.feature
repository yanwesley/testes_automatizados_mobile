#language: pt

Funcionalidade: CN0008-Detalhamento do filtro de consumo de internet para Clientes Pré Via App Android

  Cenario: CT0034-Verificar a transição detalhamento de consumo para os 3 períodos para cliente Pré  via Android
  Garantir o fluxo de direcionamento da Funcinonalidade Vivo Valoriza na Home do Meu Vivo.
    Dado que eu esteja logado
      | (11) 9 9579-5734 | 123456 |
    Quando clico em 'Detalhamento do consumo de internet'
    Entao deve exibir o tooltip informando que o cliente não tem consumo nos três periodos

  Cenario: CT0035-Verificar detalhamento do consumo do cliente periodo inferior a 7 dias para Cliente Pré Via App  Android
  Garantir o fluxo de direcionamento da Funcinonalidade Vivo Valoriza na Home do Meu Vivo.

    Dado que eu esteja logado
      | (11) 9 9579-5734 | 123456 |
    Quando clico em 'Detalhamento do consumo de internet'
    Entao deve exibir o tooltip informando que o cliente não tem consumo nos três periodos