#language: pt

Funcionalidade: CN0192-Verificação do Step Pagar Conta com linha Controle com bloqueio parcial via App Android

  Cenario: Verificar o funcionamento do botão Pagar Conta com linha Controle com bloqueio parcial via App Android
    Dado que eu esteja logado
      | (11) 9 9729-1394 | 123456 |
    Quando acesso a tab servicos
    E clico em 'Suporte Técnico'
    Entao deve exibir a pagina de 'Suporte Técnico'
    E clico em 'iniciar verificação'
    Então ///erro no fluxo