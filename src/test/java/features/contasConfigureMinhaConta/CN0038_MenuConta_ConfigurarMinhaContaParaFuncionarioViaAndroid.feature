# language: pt
Funcionalidade:CN0038-Menu Conta - Configurar minha conta para Funcionário via Android

  Contexto:
    Dado que eu esteja logado
      | (11) 9 9729-7240 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab conta
    Entao valido exibição do card 'Configurar minha conta'

  Cenario: CT0115-Visualizar Menu com Conta Conta Digital Ativada para Funcionário via Android
    Dado que esteja desativado a conta digital
    Entao eu ativo a conta digital
      | 11997297240@teste.com |
    E valido os detalhes da conta digital ativada

  Cenario:  CT0114-Visualizar Menu Conta com Conta Digital Desativada para Funcionário via Android
    Dado que esteja ativado a conta digital
      | 11997297240@teste.com |
    Entao eu desativo a conta digital
    E valido os detalhes da conta digital desativada

  Cenario: CT0116-Visualizar Menu Conta com Débito Automático Desativada para Funcionário via Android
    Dado que esteja ativo o debito automatico
      | 1212 | 1234 | 5 |
    Entao desativo o debito automático
    E valido os detalhes da debito automatico desativada

  Cenario: CT0117-Visualizar Menu Conta com Débito Automático Ativada para Funcionário via Android
    Dado que esteja desativado o debito automatico
    Entao ativo o debito automático
      | 1212 | 1234 | 5 |
    E valido os detalhes da debito automatico ativado

  Cenario:  CT0119-Realizar Troca de Endereço cadastrado para cliente Funcionário via Android
    Dado que esteja desativado a conta digital
    Entao efetuo a alteração no endereço
      | R. Benjamin Constant, 100 |
    E valido se o endereço foi alterado com sucesso

  Cenario: CT0118-Realizar Troca do Email cadastrado para cliente Funcionário via Android
    Dado que esteja ativado a conta digital
      | 11997297240k@teste.com |
    Entao faço a alteração do email do cliente
      | 987654321@teste.com |
    E valido se o email foi alterado com sucesso
      | 987654321@teste.com |

