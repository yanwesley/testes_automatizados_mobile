# language: pt
Funcionalidade: CN0036-Menu Conta - Contas já pagas para Pós via Android
  Cliente acessa Menu Conta já pagas e visualiza todas as faturas pagas, seu valor e link para baixar 2a via da fatura.

  Contexto:
    Dado que eu esteja logado
      | (11) 9 4127-4089 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab conta

  Cenário: CT0112-Visualizar Contas já pagas para Pós via Android
    Entao deve exibir o menu 'Contas já pagas'

  Cenario: CT0113-Solicitar 2a via detalhada para Contas já pagas para Pós via Android
    Entao deve exibir o menu 'Contas já pagas'
    E clico em um mês disponível
      | Janeiro |
    Entao  deve ser gerado o pdf detalhado