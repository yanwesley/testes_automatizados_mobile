  # language: pt

  Funcionalidade: CN0037-Menu Conta - Contas já pagas para Funcionário via Android
    Como o cliente gostaria de acessar e permitir a visualização do Menu Conta já pagas do cliente.

    Contexto:
      Dado que eu esteja logado
        | (11) 9 4127-4976 | 123456 |
      Entao valido a linha que estou acessando
      Quando acesso a tab conta

    Cenário: CT0112-Visualizar Contas já pagas para Funcionário via Android
      Entao deve exibir o menu 'Contas já pagas'

    Cenario: CT0113-Solicitar 2a via detalhada para Contas já pagas para Funcionário via Android
      Entao deve exibir o menu 'Contas já pagas'
      E clico em um mês disponível
        | Novembro |
      Entao  deve ser gerado o pdf detalhado