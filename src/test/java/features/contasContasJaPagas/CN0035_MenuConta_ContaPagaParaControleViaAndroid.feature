# language: pt

Funcionalidade: CN0035-Menu Conta - Conta Paga para Controle via Android

  Contexto:
    Dado que eu esteja logado
      | (11) 9 9729-7479 | 123456 |
    Entao valido a linha que estou acessando
    Quando acesso a tab conta

  Cenário: CT0112-Visualizar Contas já pagas para Controle via Android
    Entao deve exibir o menu 'Contas já pagas'

  Cenario: CT0113-Solicitar 2a via detalhada para Contas já pagas para Controle via Android
    Entao deve exibir o menu 'Contas já pagas'
    E clico em um mês disponível
      | Abril |
    Entao  deve ser gerado o pdf detalhado