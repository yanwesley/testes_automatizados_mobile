package reporter;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.io.IOException;

import static utils.MobileWebDriver.captureScreenShotAppium;

public class Reporter extends ExtentCucumberFormatter {
    /**
     * Adds an info message to the current step
     *
     * @param status  the Status log
     * @param message The message to be logged to the current step
     */
    public static void addStepLog(LogStatus status, String message) {
        getCurrentStep().log(status, message);
    }

    /**
     * Adds an info message to the current step
     *
     * @param message The message to be logged to the current step
     */
    public static void addStepLog(String message) {
        getCurrentStep().log(LogStatus.PASS, message);
    }

    /**
     * Adds the screenshot from the given path to the current step
     *
     * @param imagePath The image path
     * @throws IOException Exception if imagePath is erroneous
     */
    public static void addScreenCaptureFromPath(String imagePath) throws IOException {
        getCurrentStep().addScreenCapture(imagePath);
    }

    /**
     * Adds the screenshot from the given path with the given title to the current step
     *
     * @param logStatus The status print
     * @param mensagem  The message for the image
     */
    public static void addScreenCapture(LogStatus logStatus, String mensagem) {
        switch (platformTest) {
            case WEB:
                try {
                    Thread.sleep(1000);
                    getCurrentStep().log(logStatus,
//                           getCurrentStep().addScreenCapture(captureScreenShotLFT(outputResult, contador++)) + mensagem);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                break;
            case MOBILE:
                try {
                    Thread.sleep(1000);
                    getCurrentStep().log(logStatus,
                            getCurrentStep().addScreenCapture(captureScreenShotAppium(outputResult, contador++)) + mensagem);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                break;
        }
    }

//    /**
//     * Adds the screenshot from the given path with the given title to the current step
//     *
//     * @param mensagem The message for the image
//     */
//    public static void addScreenCapture(String mensagem) {
//        switch (platformTest) {
//            case WEB:
//                try {
//                    Thread.sleep(1000);
//                    getCurrentStep().log(LogStatus.PASS,
//                            getCurrentStep().addScreenCapture(captureScreenShotLFT(outputResult, contador++)) + mensagem);
//                } catch (Throwable throwable) {
////                    throwable.printStackTrace();
//                }
//                break;
//            case MOBILE:
//                try {
//                    Thread.sleep(1000);
//                    getCurrentStep().log(LogStatus.PASS,
//                            getCurrentStep().addScreenCapture(captureScreenShotAppium(outputResult, contador++)) + mensagem);
//                } catch (Throwable throwable) {
//                    throwable.printStackTrace();
//                }
//                break;
//        }
//    }


    private static ExtentTest getCurrentStep() {
        return ExtentCucumberFormatter.getTest();
    }


