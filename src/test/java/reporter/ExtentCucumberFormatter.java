package reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import enums.PLATFORM_TEST;
import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.*;
//import utils.LFTBrowser;
import utils.MobileWebDriver;

import java.io.File;
import java.util.List;

import static utils.FileHelper.criarPasta;
import static utils.MobileWebDriver.encerrarGravacaoAppium;
import static utils.MobileWebDriver.iniciarGravacaoAppium;

/**
 * A cucumber based reporting reporter which generates the Extent Report
 */
public class ExtentCucumberFormatter implements Reporter, Formatter {
    static ExtentReports extentReporter;
    static ExtentTest test;
    static Integer contador;
    static PLATFORM_TEST platformTest;
    static String outputResult;
    String nomeFeature;

    public static ExtentTest getTest() {
        return test;
    }


    @Override
    public void syntaxError(String s, String s1, List<String> list, String s2, Integer integer) {

    }

    @Override
    public void uri(String s) {

    }

    @Override
    public void feature(Feature feature) {
        nomeFeature = feature.getName();
    }

    @Override
    public void scenarioOutline(ScenarioOutline scenarioOutline) {
    }

    @Override
    public void examples(Examples examples) {

    }

    public void setPlatformTest(String scenarioName) {
        if (scenarioName.endsWith("Web")) {
            platformTest = PLATFORM_TEST.WEB;
        } else {
            platformTest = PLATFORM_TEST.MOBILE;
        }
    }

    private void startScreenRecorder() {
        switch (platformTest) {
            case MOBILE:
                try {
                    iniciarGravacaoAppium();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                break;
            case WEB:
//                Não está disponivel método de gravação no LeanFT
                break;

        }
    }

    private void endScreenRecorder() {
        switch (platformTest) {
            case MOBILE:
                try {
                    encerrarGravacaoAppium(outputResult, test);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                break;
            case WEB:
//                Não está disponivel método de gravação no LeanFT
                break;

        }
    }

//    private void startPlatformEngine() {
//        switch (platformTest) {
//            case WEB:
//                LFTBrowser.launchBrowser();
//                break;
//            case MOBILE:
//                MobileWebDriver.initialize();
//                break;
//        }
//    }

//    private void endPlatformEngine() {
//        switch (platformTest) {
//            case WEB:
//                LFTBrowser.quitBrowser();
//                break;
//            case MOBILE:
//                MobileWebDriver.quitDriver();
//                break;
//        }
//    }

    @Override
    public void startOfScenarioLifeCycle(Scenario scenario) {
        setPlatformTest(scenario.getName());
//        startPlatformEngine();
        startScreenRecorder();
        contador = 0;
        outputResult = "Evidencias" +
                File.separator +
                nomeFeature +
                File.separator +
                scenario.getName() +
                File.separator;

        criarPasta(outputResult);

        extentReporter = new ExtentReports(
                outputResult +
                        "reporter.html",
                true);

        extentReporter.loadConfig(new File("src\\test\\resources\\extent-config.xml"));
        test = extentReporter.startTest(scenario.getName());
    }

    @Override
    public void background(Background background) {
    }

    @Override
    public void scenario(Scenario scenario) {
    }

    @Override
    public void step(Step step) {
    }

    @Override
    public void endOfScenarioLifeCycle(Scenario scenario) {

    }

//    @Override
//    public void endOfScenarioLifeCycle(Scenario scenario) {
//        endScreenRecorder();
//        extentReporter.endTest(test);
//        extentReporter.flush();
//        endPlatformEngine();
//    }

    @Override
    public void done() {

    }

    @Override
    public void close() {
    }

    @Override
    public void eof() {

    }

    @Override
    public void before(Match match, Result result) {

    }

    @Override
    public void result(Result result) {

    }

//    @Override
//    public void result(Result result) {
//        switch (result.getStatus().toLowerCase()) {
//            case "failed":
//                reporter.Reporter.addScreenCapture(LogStatus.FAIL, result.getErrorMessage());
//                break;
//            default:
//                break;
//        }
//    }

    @Override
    public void after(Match match, Result result) {
//        switch (result.getStatus().toLowerCase()) {
//            case "failed":
//                test.getTest().setStatus(LogStatus.FAIL);
//                test.log(LogStatus.FAIL, result.getErrorMessage());
//
//                break;
//            case "skipped":
//                test.getTest().setStatus(LogStatus.SKIP);
//                test.log(LogStatus.SKIP, test.getTest().getLogList().toString());
//                break;
//            case "pending":
//                test.getTest().setStatus(LogStatus.SKIP);
//                test.log(LogStatus.WARNING, result.getErrorMessage());
//                break;
//            case "passed":
//                test.getTest().setStatus(LogStatus.PASS);
//                test.log(LogStatus.PASS, test.getTest().getName());
//                break;
//            default:
//                break;
//        }
    }

    @Override
    public void match(Match match) {

    }

    @Override
    public void embedding(String s, byte[] bytes) {

    }

    @Override
    public void write(String s) {
        test.log(LogStatus.INFO, s);
    }

}
