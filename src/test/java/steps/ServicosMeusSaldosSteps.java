package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import pages.meuPlano.servicos.MeusSaldosPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosMeusSaldosSteps extends Reporter {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());
    MeusSaldosPage meusSaldosPage = new MeusSaldosPage(getDriver());

    @E("^clico em 'Meus saldos'$")
    public void clicoEmMeusSaldos() {
        meuPlanoServicosPage.clicarBtnMeusSaldos();
        addScreenCapture("Cliquei para acessar a tela 'Meus saldos'");
    }

    @Entao("^deve exibir as informacoes do saldo via serviços$")
    public void deveExibirAsInformacoesDoSaldoViaServiços() {
        meusSaldosPage.validaSaldoDisponivel();
        addScreenCapture("Valida saldo disponivel");
    }

    @Entao("^deve exibir as informacoes do saldo bloqueado via serviços$")
    public void deveExibirAsInformacoesDoSaldoBloqueadoViaServiços() {
        meusSaldosPage.validaSaldoBloqueado();
        addScreenCapture("Valida saldo bloqueado");
    }
}
