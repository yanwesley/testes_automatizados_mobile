package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.consumo.MeuPlanoConsumoPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class CompartilhamentoSteps extends Reporter {

    MeuPlanoConsumoPage meuPlanoConsumoPage = new MeuPlanoConsumoPage(getDriver());

    @Entao("^deve exibir a mensagem que foi compartilhado com sucesso \"([^\"]*)\"$")
    public void deveExibirAMensagemQueFoiCompartilhadoComSucesso(String numero) {
        meuPlanoConsumoPage.validaCompartilhamentoComSucesso(numero);
    }

    @E("^compartilho internet numero \"([^\"]*)\"$")
    public void compartilhoInternetNumero(String numero) {
        meuPlanoConsumoPage.preencherCompartilhamento(numero);
    }
}
