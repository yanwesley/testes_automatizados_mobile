package steps;

import com.relevantcodes.extentreports.LogStatus;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.LoginPage;
import pages.TabMenuPage;
import pages.configuracoes.ConfiguracoesPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;


public class LoginMeuVivoMovelSteps extends Reporter {

    LoginPage loginMeuVivoFixoPage = new LoginPage(getDriver());
    TabMenuPage tabMenuPage = new TabMenuPage(getDriver());
    ConfiguracoesPage configuracoesPage = new ConfiguracoesPage(getDriver());

    @Entao("^deve exibir a tela welcome$")
    public void estaNaTelaWelcome() {
        loginMeuVivoFixoPage.validarTelaWelcome();
        addScreenCapture(LogStatus.PASS, "Estou na tela welcome");
    }

    @Dado("^que eu esteja na tela welcome$")
    public void estouNaTelaWelcome() {
        loginMeuVivoFixoPage.validarTelaWelcome();
        addScreenCapture(LogStatus.PASS, "Estou na tela welcome");
    }

    @Quando("^realizo as validacoes para entrar com o \"([^\"]*)\" do telefone$")
    public void preencherNumero(String numero) {
        loginMeuVivoFixoPage.clicarBtnComecarAgora();
        //loginMeuVivoFixoPage.validarTelaDeIdentificacao();
        addScreenCapture(LogStatus.PASS, "Estou na tela de inserir a linha");
        loginMeuVivoFixoPage.preencherNumeroLinha(numero);
        addScreenCapture(LogStatus.PASS, "Preenchi o número da linha");
        loginMeuVivoFixoPage.clicarBtnContinuar();
    }

    @E("^entro com a \"([^\"]*)\" correta$")
    public void preencherSenha(String senha) {
        loginMeuVivoFixoPage.validaTelaDeSenha();
        addScreenCapture(LogStatus.PASS, "Estou na tela de inserir a senha.");
        loginMeuVivoFixoPage.preencherSenha(senha);
        addScreenCapture(LogStatus.PASS, "Preenchi a senha.");
    }

    @Entao("^deve logar com sucesso$")
    public void estouLogado() {
        tabMenuPage.validaPage();
        addScreenCapture(LogStatus.PASS, "Estou logado.");
    }

    @Quando("^realizo o logout$")
    public void relizarLogout() {
        tabMenuPage.clicarBtnConfiguracoes();
        addScreenCapture(LogStatus.PASS, "Estou na tela de configurações");
        configuracoesPage.clicarBtnMaisOpcoes();
        addScreenCapture(LogStatus.PASS, "Cliquei no botão mais opções");
        configuracoesPage.clicarBtnSair();
        addScreenCapture(LogStatus.PASS, "Cliquei no botão de sair");
        configuracoesPage.clicarBtnConfirmarSair();
        addScreenCapture(LogStatus.PASS, "Cliquei no botão para confirmar o logout");
    }


}