package steps;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.meuPlano.MeuPlanoTabPage;
import pages.meuPlano.conta.MeuPlanoContaPage;
import reporter.Reporter;

import java.util.List;

import static utils.MobileWebDriver.getDriver;

public class MeuPlanoContaSteps extends Reporter {

    MeuPlanoContaPage meuPlanoContaPage = new MeuPlanoContaPage(getDriver());
    MeuPlanoTabPage meuPlanoTabPage = new MeuPlanoTabPage(getDriver());

    @Entao("^deve exibir o texto informativo apos migracao$")
    public void deveExibirOTextoInformativoAposMigracao() {
        meuPlanoContaPage.validaMensagemInformativoDeTrocaDePlano();
        addScreenCapture("Validando mensagem informativa de troca de  plano");
    }

    @Entao("^deve exibir o card da conta e seu status atrasada$")
    public void deveExibirOCardDaContaESeuStatusAtrasada() {
        meuPlanoContaPage.validaExibicaoDeCardComContaAtrasada();
        addScreenCapture("Validando exibição do card de conta atrasada");
    }

    @Entao("^deve exibir o card da conta e seu status em aberto$")
    public void deveExibirOCardDaContaESeuStatusEmAberto() {
        meuPlanoContaPage.validaExibicaoDeCardComContaEmAberto();
        addScreenCapture("Validando exibição do card de conta em aberto");
    }

    @Entao("^deve exibir o card da conta e seu status atrasada para Controle/Pos via Android$")
    public void deveExibirOCardDaContaESeuStatusAtrasadaParaControle_Pos_ViaAndroid() {
        meuPlanoContaPage.validaExibicaoDeCardComContaAtrasadaPlanoControle_Pos();
        addScreenCapture("Validando exibição do card de conta atrasada");
    }

    @Entao("^deve exibir o card da conta e seu status atrasada para Funcionário via Android$")
    public void deveExibirOCardDaContaESeuStatusAtrasadaParaFuncionárioViaAndroid() {
        meuPlanoContaPage.validaExibicaoDeCardComContaAtrasadaPlanoFuncionario();
        addScreenCapture("Validando exibição do card de conta atrasada");
    }

    @Entao("^deve exibir o card da conta e seu status atrasada para Multivivo via Android$")
    public void deveExibirOCardDaContaESeuStatusAtrasadaParaMultivivoViaAndroid() {
        meuPlanoContaPage.validaExibicaoDeCardComContaAtrasadaMultivivo();
        addScreenCapture("Validando exibição do card de conta atrasada");
    }

    @Quando("^eu solicitar o boleto atualizado$")
    public void aoSolicitarOBoletoAtualizado() {
        meuPlanoContaPage.clicarMenuDaConta();
        addScreenCapture("Abri o menu de opções");
        meuPlanoContaPage.clicarBtnBoletoAtualizado();
        addScreenCapture("Cliquei para gerar o boleto atualizado");
    }


    @Entao("^deve ser gerado o pdf atualizado$")
    public void deveSerGeradoOPdfAtualizado() {
        meuPlanoContaPage.validarBtnAdicionarAoDriveSemScroll();
        addScreenCapture("Exibe o PDF");
    }

    @Quando("^eu solicitar a segunda via detalhada$")
    public void euSolicitarASegundaViaDetalhada() {
        meuPlanoContaPage.clicarMenuDaConta();
        addScreenCapture("Abri o menu de opções");
        meuPlanoContaPage.clicarBtnSegundaViaDetalhada();
        addScreenCapture("Cliquei para gerar a segunda via detalhada");
    }

    @Quando("^eu solicitar a segunda via detalhada de conta em Aberto$")
    public void euSolicitarASegundaViaDetalhadaDeContaEmAberto() {
        meuPlanoContaPage.clicarBtnSegundaViaDetalhadaContaEmAberto();
        addScreenCapture("Cliquei para gerar a segunda via detalhada");
    }

    @Quando("^eu solicitar a segunda via detalhada de conta Funcionário atrasada$")
    public void euSolicitarASegundaViaDetalhadaDeContaFuncionárioAtrasada() {
        meuPlanoContaPage.clicarBtnSegundaViaDetalhadaContaFuncionarioAtrasada();
        addScreenCapture("Cliquei para gerar a segunda via detalhada");
    }

    @Entao("^deve ser gerado o pdf detalhado$")
    public void deveSerGeradoOPdfDetalhado() {
        meuPlanoContaPage.validarBtnAdicionarAoDrive();
        addScreenCapture("Exibe o PDF");
    }

    @Quando("^eu informar que já paguei$")
    public void euInformarQueJáPaguei() {
        meuPlanoContaPage.clicarMenuDaConta();
        addScreenCapture("Abri o menu de opções");
        meuPlanoContaPage.clicarBtnJaPagou();
        addScreenCapture("Cliquei na opcao 'Já paguei'");
    }

    @Entao("^deve exibir um modal para confirmação$")
    public void deveExibirUmModalParaConfirmação() {
        meuPlanoContaPage.validaModalJaPaguei();
        addScreenCapture("Validando modal de confirmação");
    }

    @Entao("^deve exibir um modal para confirmação para fatura em atraso conta Controle$")
    public void deveExibirUmModalParaConfirmaçãoParaFaturaEmAtrasoContaControle() {
        meuPlanoContaPage.validaModalJaPagueiStatusAtrasadoControle();
        addScreenCapture("Validando modal de confirmação");
    }

    @Entao("^deve exibir um modal para confirmação para fatura em atraso conta Pos$")
    public void deveExibirUmModalParaConfirmaçãoParaFaturaEmAtrasoContaPos() {
        meuPlanoContaPage.validaModalJaPagueiStatusAtrasadoPos();
        addScreenCapture("Validando modal de confirmação");
    }

    @Quando("^eu clico em 'Código de barras'$")
    public void euClicoEmCódigoDeBarras() {
        meuPlanoContaPage.clicarBtnCodigoDeBarras();
        addScreenCapture("Cliquei para acessar o código de barras");
    }

    @Entao("^deve exibir os detalhes do codigo de barras$")
    public void deveExibirOsDetalhesDoCodigoDeBarras() {
        meuPlanoContaPage.validaDetalhesDoCodigoDeBarras();
        addScreenCapture("Validando detalhes do codigo de barras");
    }

    @E("^ao clicar em 'Copiar código de barras'$")
    public void aoClicarEmCopiarCódigoDeBarras() {
        meuPlanoContaPage.clicaBtnCopiarCodigoDeBarras();
    }

    @Entao("^deve exibir a mensagem que foi copiado com sucesso$")
    public void deveExibirAMensagemQueFoiCopiadoComSucesso() {
        meuPlanoContaPage.validaMensagemCopiadoComSucesso();
        addScreenCapture("Validado mensagem de 'O código de barras foi copiado com sucesso'");
    }

    @E("^ao clicar em 'Enviar por SMS'$")
    public void aoClicarEmEnviarPorSMS() {
        meuPlanoContaPage.clicarBtnEnviarPorSMS();
    }

    @Entao("^deve exibir a mensagem que foi enviado com sucesso$")
    public void deveExibirAMensagemQueFoiEnviadoComSucesso(List<String> stringList) {
        meuPlanoContaPage.validaMensagemSMSEnviadoComSucesso(stringList.get(0));
        addScreenCapture("Validando mensagem de SMS enviado com sucesso");
    }

    @Entao("^deve exibir um modal para confirmação para fatura em aberto$")
    public void deveExibirUmModalParaConfirmaçãoParaFaturaEmAberto() {
        meuPlanoContaPage.validaModalJaPagueiContaEmAberto();
        addScreenCapture("Validando modal de confirmação");
    }

    @Entao("^deve exibir um modal para confirmação para fatura em atraso Multivivo$")
    public void deveExibirUmModalParaConfirmaçãoParaFaturaEmAtrasoMultivivo() {
        meuPlanoContaPage.validaModalJaPagueiContaEmAtrasoMultivivo();
        addScreenCapture("Validando modal de confirmação");
    }

    @Entao("^deve exibir o menu 'Contas já pagas'$")
    public void deveExibirOMenuContasJáPagas() {
        meuPlanoContaPage.validaExibicaoDeMenuContasJaPagas();
        addScreenCapture("Valida exibição do menu Contas já pagas");
    }

    @E("^clico em um mês disponível$")
    public void clicoEmUmMêsDisponível(List<String> list) {
        meuPlanoContaPage.clicarNoMesJaPago(list.get(0));
        addScreenCapture("Cliquei para ver a opção do mês já paga");
    }

    @Entao("^valido exibição do card 'Configurar minha conta'$")
    public void validoExibiçãoDoCardConfigurarMinhaConta() {
        meuPlanoContaPage.validaExibicaoCardConfigurarMinhaConta();
        addScreenCapture("Validando exibição do card 'Configurar minha conta'");
    }

    @Dado("^que esteja desativado a conta digital$")
    public void queEstejaDesativadoAContaDigital() {
        boolean estaDesativado = meuPlanoContaPage.validaSeEstaDesativadoAContaDigital();
        if (!estaDesativado) {
            meuPlanoContaPage.desativarContaDigital();
        } else {
            meuPlanoContaPage.clicarBtnContaDigital();
        }
        addScreenCapture("Valido que a Conta digital está desativado");
    }

    @E("^valido os detalhes da conta digital desativada$")
    public void euValidoOsDetalhesDaContaDigitalDesativada() {
        meuPlanoContaPage.validaExibicaoDaContaDigitalDesativada();
    }

    @Dado("^que esteja ativado a conta digital$")
    public void queEstejaAtivadoAContaDigital(List<String> list) {
        boolean estaAtiva = meuPlanoContaPage.validaSeEstaAtivadaAContaDigital();
        if (!estaAtiva) {
            meuPlanoContaPage.ativarContaDigital(list.get(0));
        } else {
            meuPlanoContaPage.clicarBtnContaDigital();
        }

        addScreenCapture("Valido que a Conta digital está ativa");
    }

    @E("^valido os detalhes da conta digital ativada$")
    public void euValidoOsDetalhesDaContaDigitalAtivada() {
        meuPlanoContaPage.validaExibicaoDaContaDigitalAtivada();
    }

    @Dado("^que esteja desativado o debito automatico$")
    public void queEstejaDesativadoODebitoAutomatico() {
        boolean estaDesativado = meuPlanoContaPage.validaSeEstaDesativadoODebitoAutomatico();
        if (!estaDesativado) {
            meuPlanoContaPage.desativarDebitoAutomatico();
        }
        addScreenCapture("Valido que a Conta digital está desativado");
    }

    @E("^valido os detalhes da debito automatico desativada$")
    public void euValidoOsDetalhesDaDebitoAutomaticoDesativada() {
        meuPlanoContaPage.validaExibicaoDoDebitoAutomaticoDesativado();
        addScreenCapture("Validando exibição das página de Debito Automatico Desativado");
    }

    @Dado("^que esteja ativo o debito automatico$")
    public void queEstejaAtivoODebitoAutomatico(List<String> dadosBancariosList) {
        boolean estaAtiva = meuPlanoContaPage.validaSeEstaAtivadoODebitoAutomatico();
        if (!estaAtiva) {
            meuPlanoContaPage.ativarDebitoAutomatico(dadosBancariosList);
        } else {
            meuPlanoContaPage.clicarBtnDebitoAutomatico();
        }
        addScreenCapture("Valido que o Debito Automático está ativa");
    }

    @E("^valido os detalhes da debito automatico ativado$")
    public void euValidoOsDetalhesDaDebitoAutomaticoAtivado() {
        meuPlanoContaPage.validaExibicaoDoDebitoAutomaticoAtivado();
        addScreenCapture("Validando exibição das página de Debito Automatico Ativado");
    }

    @Entao("^faço a alteração do email do cliente$")
    public void facoAAlteracaoDoEmailDoCliente(List<String> stringList) {
        meuPlanoContaPage.alterarEmail(stringList.get(0));
    }

    @Entao("^efetuo a alteração no endereço$")
    public void efetuoAAlteracaoNoEndereço(List<String> list) {
        meuPlanoTabPage.clicarBtnConta();
        meuPlanoContaPage.alterarEndereco(list.get(0));
    }

    @Entao("^eu desativo a conta digital$")
    public void euDesativoAContaDigital() {
        meuPlanoContaPage.desativarContaDigital();
    }

    @Entao("^eu ativo a conta digital$")
    public void euAtivoAContaDigital(List<String> list) {
        meuPlanoContaPage.ativarContaDigital(list.get(0));
        addScreenCapture("Ativei a conta digital");
    }

    @Entao("^desativo o debito automático$")
    public void desativoODebitoAutomático() {
        meuPlanoContaPage.desativarDebitoAutomatico();
    }

    @Entao("^ativo o debito automático$")
    public void ativoODebitoAutomático(List<String> dadosBancariosList) {
        meuPlanoContaPage.ativarDebitoAutomatico(dadosBancariosList);
    }

    @E("^valido se o endereço foi alterado com sucesso$")
    public void validoSeOEndereçoFoiAlteradoComSucesso() {
        meuPlanoContaPage.validaPaginaDeConfirmacaoDeEnderecoAlterado();
        addScreenCapture("O endereço foi alterado com sucesso");
    }

    @E("^valido se o email foi alterado com sucesso$")
    public void validoSeOEmailFoiAlteradoComSucesso(List<String> stringList) {
        meuPlanoContaPage.validaEmailAlteradoComSucesso(stringList.get(0));
    }

    @Entao("^deve exibir as faturas do plano atual e do plano anterior$")
    public void deveExibirAsFaturasDoPlanoAtualEDoPlanoAnterior() {
        meuPlanoContaPage.validaExibicaoDasFaturasDoPlanoAtualEPlanoAnterior();
    }

    @Entao("^deve exibir o card da conta atual e seu status atrasada para Controle/Pos via Android$")
    public void deveExibirOCardDaContaAtualESeuStatusAtrasadaParaControlePosViaAndroid() {
        meuPlanoContaPage.validaExibicaoDeCardComContaAtrasadaPlanoAtualControle_Pos();
        addScreenCapture("Validando exibição do card de conta atrasada");

    }

    @Entao("^deve exibir o card da conta atual e seu status aberta para Controle/Pos via Android$")
    public void deveExibirOCardDaContaAtualESeuStatusAbertaParaControlePosViaAndroid() {
        meuPlanoContaPage.validaExibicaoDeCardComContaAbertaPlanoAtualControle_Pos();
        addScreenCapture("Validando exibição do card de conta atrasada");
    }

    @Entao("^deve exibir a mensagem que foi enviado com sucesso para o numero \"([^\"]*)\"$")
    public void deveExibirAMensagemQueFoiEnviadoComSucessoParaONumero(String numero) {
        meuPlanoContaPage.validaMensagemSMSEnviadoComSucesso(numero);
        addScreenCapture("Validando mensagem de SMS enviado com sucesso");
    }
}
