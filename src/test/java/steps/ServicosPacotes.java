package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import pages.meuPlano.servicos.PacotesPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosPacotes extends Reporter {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());
    PacotesPage pacotesPage = new PacotesPage(getDriver());

    @E("^clico em 'Pacotes'$")
    public void clicoEmPacotes() {
        meuPlanoServicosPage.clicarBtnPacotes();
        addScreenCapture("cliquei para acessar a página de pacotes");
    }

    @Então("^deve exibir a pagina de pacotes$")
    public void deveExibirAPaginaDePacotes() {
        pacotesPage.validaPaginaDePacotesPre();
        addScreenCapture("Estou na página de Pacotes");
    }

    @E("^ao contratar um novo pacote$")
    public void aoContratarUmNovoPacote() {
        pacotesPage.contratarNovoPacote();
    }

    @Entao("^deve ser contratado com sucesso$")
    public void deveSerContratadoComSucesso() {
        pacotesPage.validaContratacaoPacote();
        addScreenCapture("Pacote contratado com sucesso");
    }

    @E("^ao desabilitar um pacote$")
    public void aoDesabilitarUmPacote() {
        pacotesPage.desabilitarPacoteAtivo();
    }

    @Entao("^deve ser desabilitado com sucesso$")
    public void deveSerDesabilitadoComSucesso() {
        pacotesPage.validaDesabilitacaoDoPacote();
    }

    @Então("^deve exibir a pagina de pacotes para pre$")
    public void deveExibirAPaginaDePacotesParaPre() {
        pacotesPage.validaPaginaDePacotesPre();
        addScreenCapture("Estou na página de Pacotes");
    }

    @E("^ao contratar um novo pacote pre$")
    public void aoContratarUmNovoPacotePre() {
        pacotesPage.contratarNovoPacotePre();
    }

    @Entao("^deve ser contratado com sucesso um novo pacote pre$")
    public void deveSerContratadoComSucessoUmNovoPacotePre() {
        pacotesPage.validaContratacaoPacotePre();
        addScreenCapture("Validando contratação do pacote.");
    }
}
