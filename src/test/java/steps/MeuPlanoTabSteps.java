package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import pages.TabMenuPage;
import pages.meuPlano.MeuPlanoTabPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class MeuPlanoTabSteps extends Reporter {
    MeuPlanoTabPage meuPlanoTabPage = new MeuPlanoTabPage(getDriver());
    TabMenuPage tabMenuPage = new TabMenuPage(getDriver());

    @Quando("^acesso a tab servicos$")
    public void acessoATabServicos() {

        tabMenuPage.clicarBtnMeuPlano();
        meuPlanoTabPage.clicarBtnServicos();
        addScreenCapture("Estou na pagina de Serviços");
    }

    @Quando("^acesso a tab conta$")
    public void acessoATabConta() {
        tabMenuPage.clicarBtnMeuPlano();
        meuPlanoTabPage.clicarBtnConta();
        addScreenCapture("Estou na pagina Conta");
    }

    @Quando("^acesso a tab saldo$")
    public void acessoATabSaldo() {
        tabMenuPage.clicarBtnMeuPlano();
        meuPlanoTabPage.clicarBtnSaldo();
        addScreenCapture("Estou na pagina Saldo");
    }

    @E("^acesso a tab consumo$")
    public void acessoATabConsumo() {
        tabMenuPage.clicarBtnMeuPlano();
        meuPlanoTabPage.clicarBtnConsumo();
        addScreenCapture("Estou na pagina Consumo");
    }
}
