package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import pages.meuPlano.servicos.LancamentosFuturosPage;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosLancamentosFuturos extends Reporter {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());
    LancamentosFuturosPage lancamentosFuturosPage = new LancamentosFuturosPage(getDriver());

    @E("^clico em 'Lançamentos Futuros'$")
    public void clicoEmLançamentosFuturos() {
        meuPlanoServicosPage.clicarBtnLancamentosFuturos();
        addScreenCapture("cliquei para acessar os lançamentos futuros");
    }

    @Entao("^deve exibir os lancamentos futuros com layout fechado$")
    public void deveExibirOsLancamentosFuturosComLayoutFechado() {
        lancamentosFuturosPage.validaExibicaoDoCardFechado();
        addScreenCapture("Validando ecxibição do card fechado");
    }

    @Quando("^expandir os detalhes dos lancamentos futuros$")
    public void expandirOsDetalhesDosLancamentosFuturos() {
        lancamentosFuturosPage.clicarExpandirDetalhes();
        addScreenCapture("cliquei para expandir os detalhes");
    }

    @Entao("^deve exibir os lancamentos futuros com layout aberto$")
    public void deveExibirOsLancamentosFuturosComLayoutAberto() {
        lancamentosFuturosPage.validaExibicaoDoCardAberto();
        addScreenCapture("Validando exibicao dos detalhes expandidos");

    }

    @Quando("^expandir os detalhes dos lancamentos futuros da promocao$")
    public void expandirOsDetalhesDosLancamentosFuturosDaPromocao() {
        lancamentosFuturosPage.clicarExpandirDetalhesPromocao();
        addScreenCapture("cliquei para expandir os detalhes");
    }

    @Entao("^deve exibir os lancamentos futuros com layout aberto da promocao$")
    public void deveExibirOsLancamentosFuturosComLayoutAbertoDaPromocao() {
        lancamentosFuturosPage.validaExibicaoDoCardAberto();
        addScreenCapture("Validando exibicao dos detalhes expandidos");
    }

    @Então("^deve exibir a label 'Hoje, se houver saldo'$")
    public void deveExibirALabelHojeSeHouverSaldo() {
        lancamentosFuturosPage.validaExibicaoDoLabelHojeSeHouverSaldo();
        addScreenCapture("Validando exibição do label Hoje, se houver saldo");
    }
}
