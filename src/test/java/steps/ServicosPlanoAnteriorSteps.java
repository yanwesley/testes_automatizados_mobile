package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import pages.meuPlano.servicos.PlanoAnteriorPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosPlanoAnteriorSteps extends Reporter {

    PlanoAnteriorPage planoAnteriorPage = new PlanoAnteriorPage(getDriver());
    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());

    @E("^clico em 'Plano anterior'$")
    public void clicoEmPlanoAnterior() {
        meuPlanoServicosPage.clicarBtnPlanoAnterior();
        addScreenCapture("Cliquei no botão 'Plano Anterior'");
    }

    @Entao("^deve exibir as faturas do plano anterior$")
    public void deveExibirAsFaturasDoPlanoAnterior() {
        planoAnteriorPage.validaExibicaoDeFaturasDoPlanoAnterior();
        addScreenCapture("Validando exibição do 'Histórico de Contas'");
    }
}
