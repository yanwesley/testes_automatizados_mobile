package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.servicos.DetalhesPlanoPage;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import reporter.Reporter;
import utils.MobileWebDriver;

public class ServicosDetalhePlanoSteps {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(MobileWebDriver.getDriver());
    DetalhesPlanoPage detalhesPlanoPage = new DetalhesPlanoPage(MobileWebDriver.getDriver());

    @E("^clico em 'Detalhe do Plano'$")
    public void clicoEmDetalheDoPlano() {
        meuPlanoServicosPage.clicarBtnDetalheDoPlano();
        Reporter.addScreenCapture("Cliquei para acessar os Destalhes do meu Plano");
    }

    @Entao("^verifico o nome e detalhes do plano \"([^\"]*)\"$")
    public void verificoONomeEDetalhesDoPlano(String textoEsperado) {
        detalhesPlanoPage.validaTelaMeuPlano(textoEsperado);
        Reporter.addScreenCapture("Validando Detlhes do Meu Plano");
        detalhesPlanoPage.scrollDown();
        Reporter.addScreenCapture("Validando Detlhes do Meu Plano");
        detalhesPlanoPage.scrollDown();
        Reporter.addScreenCapture("Validando Detlhes do Meu Plano");
    }
}
