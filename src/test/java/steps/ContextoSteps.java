package steps;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import pages.LoginPage;
import pages.TabMenuPage;
import pages.configuracoes.ConfiguracoesPage;
import reporter.Reporter;

import java.util.List;

import static utils.MobileWebDriver.getDriver;

public class ContextoSteps extends Reporter {

    LoginPage loginPage = new LoginPage(getDriver());
    TabMenuPage tabMenuPage = new TabMenuPage(getDriver());
    ConfiguracoesPage configuracoesPage = new ConfiguracoesPage(getDriver());

    @Dado("^que eu esteja logado$")
    public void fazerLogin(List<String> stringList) {
        loginPage = new LoginPage(getDriver());
        tabMenuPage = new TabMenuPage(getDriver());
        loginPage.validarTelaWelcome();
        addScreenCapture("Estou na tela welcome");
        loginPage.clicarBtnComecarAgora();
        loginPage.validarTelaDeIdentificacao();
        addScreenCapture("Estou na tela de inserir a linha");
        loginPage.preencherNumeroLinha(stringList.get(0));
        addScreenCapture("Preenchi o número da linha");
        loginPage.clicarBtnContinuar();
        loginPage.validaTelaDeSenha();
        addScreenCapture("Estou na tela de inserir a senha.");
        loginPage.preencherSenha(stringList.get(1));
        addScreenCapture("Preenchi a senha.");
        tabMenuPage.validaPage();
        addScreenCapture("Estou logado.");
    }

    @Dado("^que eu esteja logado com \"([^\"]*)\" e \"([^\"]*)\"$")
    public void queEuEstejaLogadoComE(String linha, String senha) {
        loginPage = new LoginPage(getDriver());
        tabMenuPage = new TabMenuPage(getDriver());
        loginPage.validarTelaWelcome();
        addScreenCapture("Estou na tela welcome");
        loginPage.clicarBtnComecarAgora();
        loginPage.validarTelaDeIdentificacao();
        addScreenCapture("Estou na tela de inserir a linha");
        loginPage.preencherNumeroLinha(linha);
        addScreenCapture("Preenchi o número da linha");
        loginPage.clicarBtnContinuar();
        loginPage.validaTelaDeSenha();
        addScreenCapture("Estou na tela de inserir a senha.");
        loginPage.preencherSenha(senha);
        addScreenCapture("Preenchi a senha.");
        tabMenuPage.validaPage();
        addScreenCapture("Estou logado.");
    }

    @Entao("^valido a linha que estou acessando$")
    public void validoALinhaQueEstouAcessando() {
        tabMenuPage.clicarBtnConfiguracoes();
        configuracoesPage.validaLinhaCliente();
        addScreenCapture("Validando linha do cliente");

    }
}
