package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import pages.meuPlano.servicos.MinhaPromocaoPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosMinhaPromocaoSteps extends Reporter {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());
    MinhaPromocaoPage minhaPromocaoPage = new MinhaPromocaoPage(getDriver());

    @E("^clico em 'Minha promocao'$")
    public void clicoEmMinhaPromocao() {
        meuPlanoServicosPage.clicaMinhaPromocao();
        addScreenCapture("Cliquei para acessar a page Minha promoção");
    }

    @Entao("^deve exibir as informacoes da minha promocao$")
    public void deveExibirAsInformacoesDaMinhaPromocao() {
        minhaPromocaoPage.validarPageMinhaPromocao();
        addScreenCapture("Estou na tela de Promoções");
    }

    @E("^clico em 'Recarregar agora'$")
    public void clicoEmRecarregarAgora() {
        minhaPromocaoPage.clicarRecarregarAgora();
        addScreenCapture("Cliquei no botão recarregar agora!");

    }

    @Entao("^deve exibir as informacoes da minha promocao para plano pre$")
    public void deveExibirAsInformacoesDaMinhaPromocaoParaPlanoPre() {
        minhaPromocaoPage.validarPageMinhaPromocaoParaPre();
    }

    @Quando("^clico em 'Entenda a promoção'$")
    public void clicoEmEntendaAPromocao() {
        minhaPromocaoPage.cliclarEntendaAPromocao();
        addScreenCapture("Cliquei no botão 'Entenda a promoção'.");
    }

    @Entao("^deve exibir os detalhes da promoção$")
    public void deveExibirOsDetalhesDaPromocao() {
        minhaPromocaoPage.validaDetalhesDaPromocaoContratada();
    }

    @E("^ao cancelar a sua promoção$")
    public void aoCancelarASuaPromocao() {
        minhaPromocaoPage.cancelarPromocao();
        addScreenCapture("cliquei para cancelar a promoção.");
    }

    @Entao("^deve exibir um modal para confirmar a desativação da promoção$")
    public void deveExibirUmModalParaConfirmarADesativacaoDaPromocao() {
        minhaPromocaoPage.validaModalDeCancelamentoDePromocao();
        addScreenCapture("Validando modal de cancelamento.");
    }

    @E("^ao confirmar o cancelamento$")
    public void aoConfirmarOCancelamento() {
        minhaPromocaoPage.clicarConfirmarCancelamento();
        addScreenCapture("Cliquei para confirmar o cancelamento.");
    }

    @Entao("^deve exibir o modal de 'Ok, agora é só aguardar' para cancelamento$")
    public void deveExibirOModalDeOkAgoraESoAguardarParaCancelamento() {
        minhaPromocaoPage.validaModalOkAgoraSoAguardarParaCancelamento();
        addScreenCapture("Validando modal de confirmação de cancelamento.");
    }

    @Quando("^clico em 'Conheça outras promoções'$")
    public void clicoEmConhecaOutrasPromocoes() {
        minhaPromocaoPage.clicarEmConhecaOutrasPromocoes();
        addScreenCapture("Cliquei em 'Conheça outras promoções'.");
    }

    @Entao("^deve exibir as demais promoções$")
    public void deveExibirAsDemaisPromocao() {
        minhaPromocaoPage.validaPageOutrasPromocoes();
    }

    @E("^ao contratar uma nova promoção$")
    public void aoContratarUmaNovaPromocao() {
        minhaPromocaoPage.contratarNovaPromocao();
    }

    @E("^ao confirmar a promoção$")
    public void aoConfirmarAPromocao() {
        minhaPromocaoPage.confirmarContratacaoDeNovaPromocao();

    }

    @Entao("^deve exibir o modal de 'Ok, você vai receber um SMS de confirmação'$")
    public void deveExibirOModalDeOkVoceVaiReceberUmSMSDeConfirmacao() {
        minhaPromocaoPage.validaModalOkVoceVaiReceberUmSMSDeConfirmacao();
        addScreenCapture("Validando confirmacao.");

    }

    @Entao("^deve exibir o modal de 'Ok, agora é só aguardar' para contratação$")
    public void deveExibirOModalDeOkAgoraÉSóAguardarParaContratacao() {
        minhaPromocaoPage.validaModalDeConfirmacaoDeContratacao();
        addScreenCapture("Validando modal de confirmação de contratação.");
    }

    @Quando("^clico em 'Antecipar renovação'$")
    public void clicoEmAnteciparRenovacao() {
        minhaPromocaoPage.clicarEmAnteciparRenovacao();
        addScreenCapture("Cliquei no botão Antecipar renovação.");
    }

    @Entao("^deve exibir o modal de confirmação da antecipação$")
    public void deveExibirOModalDeConfirmacaoDaAntecipacao() {
        minhaPromocaoPage.validaModalDeConfirmacaoDeAntecipacao();
        addScreenCapture("Validando modal de confirmar antecipação.");
    }

    @E("^ao confirmar a antecipação$")
    public void aoConfirmarAAntecipacao() {
        minhaPromocaoPage.confirmarAntecipacaoDaPromocao();
        addScreenCapture("Cliquei para efetuar a antecipação.");
    }
}
