package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.saldo.MeuPlanoSaldoPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class MeuPlanoSaldoSteps extends Reporter {

    MeuPlanoSaldoPage meuPlanoSaldoPage = new MeuPlanoSaldoPage(getDriver());

    @Entao("^deve exibir as informacoes do saldo$")
    public void deveExibirAsInformacoesDoSaldo() {
        meuPlanoSaldoPage.validaSaldoDisponivel();
        addScreenCapture("Validando exibição de saldo");
    }

    @Entao("^deve exibir as informacoes do saldo bloqueado$")
    public void deveExibirAsInformacoesDoSaldoBloqueado() {
        meuPlanoSaldoPage.validaSaldoBloqueado();
        addScreenCapture("Validando exibição de saldo bloqueado");
    }

    @E("^clico em 'Ver extrato detalhado'$")
    public void clicoEmVerExtratoDetalhado() {
        meuPlanoSaldoPage.clicarEmVerExtratoDetalhado();
    }

    @Entao("^deve exibir as informacoes do extrato$")
    public void deveExibirAsInformacoesDoExtrato() {
        meuPlanoSaldoPage.validarTelaDeExtrato();
    }

    @Entao("^deve exibir as informacoes do extrato pre com saldo bloqueado$")
    public void deveExibirAsInformacoesDoExtratoPreComSaldoBloqueado() {
        meuPlanoSaldoPage.validarTelaDeExtratoPreComSaldoBloqueado();
    }
}
