package steps;

import api.model.Pessoa;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.lojaOnline.LojaOnlinePage;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import reporter.Reporter;

import static api.PessoaFake.getPessoa;
import static utils.MobileWebDriver.getDriver;

public class LojaOnlineSteps extends Reporter {

    Pessoa pessoa = getPessoa();

    LojaOnlinePage lojaOnlinePage = new LojaOnlinePage(getDriver());
    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());

    @Entao("^valido que possuo um plano controle$")
    public void validoQuePossuoUmPlanoControle() {

        lojaOnlinePage.validarTelaPlanoControle();
        addScreenCapture("Mostrando informações de linha com plano controle");

    }

    @Entao("^clico em 'Troca de plano'$")
    public void clicoEmTrocaDePlano() {

        meuPlanoServicosPage.clicarBtnTrocaDePlano();
        addStepLog("Cliquei no botão 'Troca de plano'.");
    }

    @Entao("^na tela de planos escolho nova opção de plano pós-pago$")
    public void selecionoPlanoPósPago() throws Throwable {

        Thread.sleep(5000);
        lojaOnlinePage.meioScrollDown();
        addScreenCapture("Visualizando opção de plano Pós");
        lojaOnlinePage.selecionarPlanoPos();
        addStepLog("Selecionando plano desejado para efetuar migração");
        lojaOnlinePage.confirmarEscolhaDoPlano();
        addScreenCapture("Selecionado o botão sim");
    }

    @E("^confirmo os dados da compra$")
    public void confirmoOsDadosDaCompra() throws Throwable {

        Thread.sleep(10000);

        lojaOnlinePage.confirmoDadosCompra();
        addScreenCapture("Visualizando dados do comprador");
    }

    @Entao("^valido que possuo um plano pré-pago$")
    public void validoQuePossuoUmPlanoPréPago() {

        lojaOnlinePage.validarTelaPlanoPrePago();
        addScreenCapture("Mostrando informações de linha com plano pré-pago");

        lojaOnlinePage.clicarBotaoVoltar();
        addStepLog("Cliquei no botão voltar");
    }

    @Entao("^na tela de planos escolho nova opção de plano controle$")
    public void selecionoPlanoControle() {

        addScreenCapture("Visualizando opção de plano Controle");
        lojaOnlinePage.selecionarPlanoControle();
        addStepLog("Selecionando plano desejado para efetuar migração");
    }

    @E("^valido tela de conclusão com aquisição pendente$")
    public void validoTelaDeConclusãoComAquisiçãoPendente() throws Throwable {

        lojaOnlinePage.meioScrollDown();
        Thread.sleep(3000);
        lojaOnlinePage.meioScrollDown();

        lojaOnlinePage.concluirCompra();
        addStepLog("Clicando em conluir compra");

        //lojaOnlinePage.validarTelaDeConclusaoComAquisicaoPendente();

        Thread.sleep(10000);
        addScreenCapture("Visualizando etapa final de migração de plano com aquisição pendente");
    }

    @Entao("^aceito os termos de uso e concluo a aquisição$")
    public void aceitoOsTermosDeUsoEConcluoAAquisição() {

        lojaOnlinePage.concluirAquisicao();
        addScreenCapture("Aquisição de novo plano concluído com sucesso");
    }

    @E("^fecho o aplicativo$")
    public void fechoOAplicativo() {

        lojaOnlinePage.fecharAplicativo();
        addScreenCapture("Visualizando fechamento do aplicativo");
    }

    @Entao("^abro o aplicativo e retorno para a tela do carrinho$")
    public void abroOAplicativoERetornoParaATelaDoCarrinho() {

        lojaOnlinePage.reabrindoOAplicativo();
        addScreenCapture("Reabrindo o aplicativo");

        clicoEmTrocaDePlano();
    }

    @E("^valido que o item continua no carrinho$")
    public void validoQueOItemContinuaNoCarrinho() {

        lojaOnlinePage.validoQueOPlanoContinuaNoCarrinho();
        addScreenCapture("Visualizando que o plano escolhido continua no carrinho");
    }

    @E("^confirmo que quero trocar de plano$")
    public void confirmoQueQueroTrocarDePlano() {

        lojaOnlinePage.confirmarTrocarPlano();
        addStepLog("Confirmando troca de planos e prosseguindo para carrinho de compras");

    }

    @E("^valido 'Resumo da Compra'$")
    public void validoResumoDaCompra() {

        lojaOnlinePage.validarResumoCompra();
        addScreenCapture("Validando tela com plano escolhido para aquisição");

        lojaOnlinePage.confirmoDadosCompra();
        addStepLog("Minimizando tela de dados da compra");
    }

    @Entao("^escolho endereço de cobrança$")
    public void escolhoEndereçoDeCobrança() throws Throwable {

        lojaOnlinePage.meioScrollDown();
        Thread.sleep(3000);
        lojaOnlinePage.meioScrollDown();

        lojaOnlinePage.selecionandoEndereco();
        addScreenCapture("Visualizando endereço selecionado");

        lojaOnlinePage.clicarContinuar();
        addStepLog("Clicando em continuar");
    }


    @Entao("^realizo cadastro na loja virtual$")
    public void realizoCadastroNaLojaVirtual() {

        lojaOnlinePage.realizadoCadastroNaLojaVirtual();
        addStepLog("Confirmando troca de planos e prosseguindo para carrinho de compras");

    }

    @Entao("^clico em 'Troca de aparelho'$")
    public void clicoEmTrocaDeAparelho() {

        lojaOnlinePage.clicoEmTrocardeAparelho();
        addStepLog("Selecionado Troca de aparelho");
    }

    @E("^seleciono um aparelho$")
    public void selecionoUmAparelho() throws InterruptedException {

        lojaOnlinePage.clicoEmVerDetalhes();
        addScreenCapture("selecionado Iphone 4S");
    }

    @E("^escolho um plano Pós Pago$")
    public void escolhoUmPlanoPósPago() {

        lojaOnlinePage.clicoEmComprar();
    }
}

