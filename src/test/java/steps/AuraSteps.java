package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.TabMenuPage;
import pages.aura.AuraPage;
import reporter.Reporter;

import java.util.List;

import static utils.MobileWebDriver.getDriver;

public class AuraSteps extends Reporter {

    TabMenuPage tabMenuPage = new TabMenuPage(getDriver());
    AuraPage auraPage = new AuraPage(getDriver());

    @Quando("^acesso a AURA$")
    public void acessoAAURA() {
        tabMenuPage.clicarBtnAura();
        addScreenCapture("Acesso a AURA");
    }

    @E("^pesquiso o que quero saber$")
    public void pesquisoOQueQueroSaber(List<String> list) {
        auraPage.clicarBtnTeclado();
        addScreenCapture("Abro o campo de pesquisa");
        auraPage.preencherPergunta(list.get(0));
        addScreenCapture("Preenchi com a pesquisa");
    }

    @Entao("^deve exibir o consumo do meu plano plano$")
    public void deveExibirOConsumoDoMeuPlanoPlano() {
    }

    @Entao("^deve exibir o meu saldo$")
    public void deveExibirOMeuSaldo() {

    }

    @Entao("^deve exibir o meu plano$")
    public void deveExibirOMeuPlano() {
    }
}
