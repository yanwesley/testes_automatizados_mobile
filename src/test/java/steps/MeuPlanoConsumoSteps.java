package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.TabMenuPage;
import pages.meuPlano.consumo.MeuPlanoConsumoPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class MeuPlanoConsumoSteps extends Reporter {

    MeuPlanoConsumoPage meuPlanoConsumoPage = new MeuPlanoConsumoPage(getDriver());
    TabMenuPage tabMenuPage = new TabMenuPage(getDriver());

    @Quando("^eu clico em 'Envie sua internet disponivel'$")
    public void euClicoEmEnvieSuaInternetDisponivel() {
        meuPlanoConsumoPage.clicarEnvieSuaInternetDisponivel();
    }

    @Entao("^deve exibir o texto do plano 'Renova em dd/mm'$")
    public void deveExibirOTextoDoPlanoRenovaEmDdMm() {
        meuPlanoConsumoPage.validaExibicaoDataRenovacaoRenovaEmDdMm();
    }

    @E("^deve exibir os dados de consumo 'Multivivo'$")
    public void deveExibirOsDadosDeConsumoMultivivo() {
        meuPlanoConsumoPage.validaConsumoMultivivo();
        addScreenCapture("Validando apresentaçao do modal na tela de consumo");
    }

    @E("^eu clico em 'Mais Detalhes'$")
    public void euClicoEmMaisDetalhes() {
        meuPlanoConsumoPage.clicarNoIconeMaisDetalhes();
        addScreenCapture("Cliquei para exibir 'Mais detalhes'");
    }

    @Entao("^deve exibir o modal de maiores informações$")
    public void deveExibirOModalDeMaioresInformações() {
        meuPlanoConsumoPage.validaModalDeMaisDetalhes();
        addScreenCapture("Validando modal de 'Mais detalhes'.");
    }

    @Quando("^clico em 'Detalhamento do consumo de internet'$")
    public void clicoEmDetalhamentoDoConsumoDeInternet() {
        meuPlanoConsumoPage.clicarEmDetalhamentoDoConsumoDeInternet();
    }

    @Entao("^deve exibir o tooltip informando que o cliente não tem consumo$")
    public void deveExibirOTooltipInformandoQueOClienteNãoTemConsumo() {
        meuPlanoConsumoPage.validaExibibicaoDeTooltipSemConsumo();
    }

    @Entao("^deve exibir o tooltip informando que o cliente não tem consumo nos três periodos$")
    public void deveExibirOTooltipInformandoQueOClienteNãoTemConsumoNosTrêsPeriodos() {
        meuPlanoConsumoPage.validaExibibicaoDeTooltipSemConsumo7_15_30_dias();
    }

    @Quando("^clico em 'Detalhamento do consumo de internet' pos$")
    public void clicoEmDetalhamentoDoConsumoDeInternetPos() {
        meuPlanoConsumoPage.clicarEmDetalhamentoDoConsumoDeInternetPos();
    }

    @E("^valido que não está sendo exibida a data de renovação do plano$")
    public void validoQueNaoEstaSendoExibidaADataDeRenovacaoDoPlano() {
        tabMenuPage.clicarBtnMeuPlano();
        meuPlanoConsumoPage.validaQueNaoEstaExibindoDataDeRenovacao();
        addScreenCapture("Validou que não está exibindo a label de renovação");
    }

    @E("^valido a exibição do layout de consumo$")
    public void validoAExibicaoDoLayoutDeConsumo() {
        meuPlanoConsumoPage.validaExibicaoDoLayoutDeConsumo();
        addScreenCapture("Validando exibição do layout de consumo.");
    }

    @E("^valido a exibição do layout de consumo total da franquia$")
    public void validoAExibiçãoDoLayoutDeConsumoTotalDaFranquia() {
        meuPlanoConsumoPage.validaLayoutConsumoTotalDaFranquia();
        addScreenCapture("Validando exibição do layout de consumo total da franquia.");
    }

    @E("^valido a exibição do layout de consumo parcial da franquia$")
    public void validoAExibiçãoDoLayoutDeConsumoParcialDaFranquia() {

    }

    @E("^valido a exibição do layout de SEM consumo da franquia$")
    public void validoAExibiçãoDoLayoutDeSEMConsumoDaFranquia() {
        meuPlanoConsumoPage.validaLayoutSemConsumoDaFranquia();
        addScreenCapture("Validando exibição do layout SEM consumo da franquia.");
    }

    @Entao("^deve exibir o arredondamento conforme o tipo \"([^\"]*)\"$")
    public void deveExibirOArredondamentoComformeOTipo(String unidadeMedida) {
        int quantidade = meuPlanoConsumoPage.validaExibicaoDoArredondamentoConformeOTipo(unidadeMedida);
        assert quantidade >= 3;
        addScreenCapture("Validando arredondamento conforme o tipo " + unidadeMedida);
    }
}
