package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import pages.meuPlano.servicos.RecargaPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosRecargaSteps {
    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());
    RecargaPage recargaPage = new RecargaPage(getDriver());

    @E("^clico em 'Faca uma recarga'$")
    public void clicoEmFacaUmaRecarga() {
        meuPlanoServicosPage.clicarBtnFacaUmaRecarga();
        Reporter.addScreenCapture("Ciquei para acessar a page de recarga");
    }

    @Entao("^deve exibir as informacoes para efetuar a recarga$")
    public void deveExibirAsInformacoesParaEfetuarARecarga() {
        recargaPage.validaTelaDeRecarga();
    }
}
