package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import pages.meuPlano.servicos.VivoValorizaPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosVivoValorizaSteps {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());
    VivoValorizaPage vivoValorizaPage = new VivoValorizaPage(getDriver());

    @E("^clico em 'Vivo Valoriza'$")
    public void clicoEmVivoValoriza() {
        meuPlanoServicosPage.clicarBtnVivoValoriza();
        Reporter.addScreenCapture("Cliquei para acessar o vivo valoriza");
    }

    @Entao("^deve exibir as informacoes do vivo valoriza para não cadastrados$")
    public void deveExibirAsInformacoesDoVivoValorizaParaNãoCadastrados() {
        vivoValorizaPage.validaPageVivoValorizaParaClientesNaoCadastrados();
    }

    @Entao("^deve exibir as informacoes do vivo valoriza$")
    public void deveExibirAsInformacoesDoVivoValoriza() {
        vivoValorizaPage.validarInformaoesVivoValoriza();
    }

    @Entao("^nao deve exibir a opcao 'Vivo Valoriza'$")
    public void naoDeveExibirAOpcaoVivoValoriza() {
        meuPlanoServicosPage.naoDeveExibirAOpcaoVivoValoriza();
        Reporter.addScreenCapture("Não está exibindo a opção Vivo Valoriza");
    }

    @E("^clico em 'Meu perfil no Vivo Valoriza'$")
    public void clicoEmMeuPerfilNoVivoValoriza() {
        vivoValorizaPage.clicarBtnMeuPerfilNoVivoValoriza();
        Reporter.addScreenCapture("Cliquei para acessar o meu Perfil no Vivo Valoriza");

    }

    @Entao("^deve exibir os perfil Vivo Valoriza$")
    public void deveExibirOsPerfilVivoValoriza() {
        vivoValorizaPage.validarPerfilMeuVivoValoriza();
    }

    @E("^clico na categoria 'Descontos'$")
    public void clicoNaCategoriaDescontos() {
        vivoValorizaPage.clicarCategoriaDescontos();
        Reporter.addScreenCapture("Cliquei para acessar a categoria Descontos");
    }

    @Entao("^deve exibir os parceiros da categoria selecionada$")
    public void deveExibirOsParceirosDaCategoriaSelecionada() {
        vivoValorizaPage.validaPageDeParceiros();
    }

    @E("^clico no primerio parceiro$")
    public void clicoNoPrimerioParceiro() {
        vivoValorizaPage.clicarPrimeiroParceiro();
        Reporter.addScreenCapture("Cliquei para acessar o primeiro parceiro");
    }

    @Entao("^deve exibir os detalhes do parceiro e o link do site$")
    public void deveExibirOsDetalhesDoParceiroEOLinkDoSite() {
        vivoValorizaPage.validaDetalhesDoParceiro();
    }

    @E("^clico no link do site$")
    public void clicoNoLinkDoSite() {
        vivoValorizaPage.clicarLinkDoParceiro();
        Reporter.addScreenCapture("Cliquei para acessar o site do parceiro");

    }

    @Entao("^deve exibir o site do parceiro$")
    public void deveExibirOSiteDoParceiro() {
        vivoValorizaPage.validaSiteParceiro();

    }


}
