package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import reporter.Reporter;

public class ServicosSuporteTecnicoSteps extends Reporter {

    @E("^clico em 'Suporte Técnico'$")
    public void clicoEmSuporteTécnico() {
    }

    @Entao("^deve exibir a pagina de 'Suporte Técnico'$")
    public void deveExibirAPaginaDeSuporteTécnico() {
    }

    @E("^clico em 'iniciar verificação'$")
    public void clicoEmIniciarVerificação() {
    }
}
