//package steps.web;
//
//import com.hp.lft.sdk.GeneralLeanFtException;
//import cucumber.api.java.pt.Dado;
//import pages.web.LoginHybrisPage;
//import reporter.Reporter;
//
//public class LoginHybrisSteps extends Reporter {
//    LoginHybrisPage loginHybrisPage = new LoginHybrisPage();
//
//    @Dado("^que eu esteja logado no Hybris$")
//    public void queEuEstejaLogadoNoHybris() throws GeneralLeanFtException, CloneNotSupportedException, InterruptedException {
//        loginHybrisPage.efetuarLogin();
//        addScreenCapture("Estou logado");
//    }
//}
