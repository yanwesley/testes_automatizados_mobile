//package steps.web;
//
//import api.model.Pessoa;
//import com.hp.lft.sdk.GeneralLeanFtException;
//import cucumber.api.java.pt.E;
//import cucumber.api.java.pt.Entao;
//import cucumber.api.java.pt.Então;
//import cucumber.api.java.pt.Quando;
//import pages.web.OrderHybrisPage;
//import reporter.Reporter;
//
//import static api.PessoaFake.getPessoa;
//
//public class OrderHybrisSteps extends Reporter {
//    OrderHybrisPage orderHybrisPage = new OrderHybrisPage();
//    private Pessoa pessoa = getPessoa();
//
//    @Quando("^acesso o menu 'Order'$")
//    public void acessoOMenuOrder() throws GeneralLeanFtException {
//        orderHybrisPage.acessarMenuOrders();
//        addScreenCapture("Acessei o menu de ordens.");
//    }
//
//    @E("^pesquiso pelo número do pedido$")
//    public void pesquisoPeloNumeroDoPedido() throws GeneralLeanFtException {
//        orderHybrisPage.pesquisarOrderPorNumeroDoPedido(pessoa.numeroPedido);
//        addScreenCapture("Pesquisei pelo número da ordem.");
//    }
//
//    @Então("^deve exibir o pedido como resultado da consulta$")
//    public void deveExibirOPedidoComoResultadoDaConsulta() throws GeneralLeanFtException {
//        orderHybrisPage.validaResultadoDaConsulta();
//        addScreenCapture("Valido que foi exibido o resultado.");
//    }
//
//    @E("^ao clicar para acessar os detalhes do pedido$")
//    public void aoClicarParaAcessarOsDetalhesDoPedido() throws GeneralLeanFtException {
//        orderHybrisPage.clicarNoResultadoParaAbrirOsDetalhes(pessoa.numeroPedido);
//        addStepLog("Cliquei para abrir os detalhes do pedido.");
//    }
//
//    @Entao("^deve exibir os detalhes do pedido$")
//    public void deveExibirOsDetalhesDoPedido() throws GeneralLeanFtException {
//        orderHybrisPage.validaDetalhesDoPedido(pessoa);
//        addScreenCapture("Validando exibição dos detalhes.");
//    }
//}
