package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.servicos.AgendamentosPage;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import reporter.Reporter;

import static utils.MobileWebDriver.getDriver;

public class ServicosAgendamentosSteps extends Reporter {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(getDriver());
    AgendamentosPage agendamentosPage = new AgendamentosPage(getDriver());

    @E("^clico em 'Agendar atendimento'$")
    public void clicoEmAgendarAtendimento() {
        meuPlanoServicosPage.clicarBtnAgendarAtendimento();
        addScreenCapture("Cliquei no botão 'Agendar atendimento'");
    }

    @Entao("^deve exibir as lojas proximas por geolocalizacao$")
    public void deveExibirAsLojasProximasPorGeolocalizacao() {
        agendamentosPage.validaExibicaoDeLojasProximasPorGeolocalizacao();
        addScreenCapture("Validando exibição do mapa");
    }

    @E("^preencho os dados para o agendamento$")
    public void preenchoOsDadosParaOAgendamento() {
        agendamentosPage.preencherDadosAgendamento();
    }

    @Entao("^deve ser agendando com sucesso$")
    public void deveSerAgendandoComSucesso() {
        agendamentosPage.validaAgendamentoComSucesso();
        addScreenCapture("Validado modal de confirmação");
        agendamentosPage.btnOkObrigadoAgendado.click();
//        agendamentosPage.validaMeusAgendamentos();
//        addScreenCapture("Validado pagina de 'Meus agendamentos'");
    }

    @E("^visualizo meus agendamentos$")
    public void visualizoMeusAgendamentos() {
        agendamentosPage.clicarBtnMeusAgendamentos();
        addScreenCapture("Cliquei para acessar a página de 'Meus agendamentos'");
        agendamentosPage.validaMeusAgendamentos();
        addScreenCapture("Validado pagina de 'Meus agendamentos'");
    }

    @E("^cancelo o agendamento$")
    public void canceloOAgendamento() {
        agendamentosPage.excluirAgendamento();
        addScreenCapture("Cliquei no botão 'Excluir'");
        agendamentosPage.confirmaCancelamento();
    }

    @Entao("^deve ser cancelado com sucesso$")
    public void deveSerCanceladoComSucesso() {
        agendamentosPage.validaCancelamento();
    }
}
