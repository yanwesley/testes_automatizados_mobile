package steps;

import com.relevantcodes.extentreports.LogStatus;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pages.meuPlano.servicos.MeuPlanoServicosPage;
import pages.meuPlano.servicos.VivoTravelPage;
import reporter.Reporter;
import utils.MobileWebDriver;

public class ServicosVivoTravelSteps extends Reporter {

    MeuPlanoServicosPage meuPlanoServicosPage = new MeuPlanoServicosPage(MobileWebDriver.getDriver());
    VivoTravelPage vivoTravelPage = new VivoTravelPage(MobileWebDriver.getDriver());

    @E("^clico em 'Roaming - Vivo Travel'$")
    public void clicoEmRoamingVivoTravel() {
        meuPlanoServicosPage.clicarBtnRoamingTravel();
        addScreenCapture(LogStatus.PASS, "Acessei a página Romaing - Vivo Travel");
    }

    @Entao("^verifico se o roaming esta ativo$")
    public void verificoSeORoamingEstaAtivo() {
        vivoTravelPage.validaSeEstaAtivo();
        addScreenCapture(LogStatus.PASS, "Romaing - Vivo Travel está ativo");
    }

    @E("^clico em 'Franquia para acesso a internet'$")
    public void clicoEmFranquiaParaAcessoAInternet() throws Throwable {
        vivoTravelPage.clicarBtnFranquiaParaAcessoAInternet();
        addScreenCapture(LogStatus.PASS, "Clico em Franquia para acesso a internet");
    }

    @Entao("^verifico se o roaming esta desativado$")
    public void verificoSeORoamingEstaDesativado() {
        vivoTravelPage.validaSeEstaInativo();
        addScreenCapture(LogStatus.PASS, "Romaing - Vivo Travel está desativado");
    }

    @E("^clico em 'Ativar o roaming internacional'$")
    public void clicoEmAtivarORoamingInternacional() {
        vivoTravelPage.clicarAtivarRoaming();
        addScreenCapture(LogStatus.PASS, "Cliquei para ativar o roaming");
        vivoTravelPage.esperaElementoFicarVisivel(vivoTravelPage.btnConfirmarOk);
        addScreenCapture(LogStatus.PASS, "Exibiu modal de confirmação");
        vivoTravelPage.btnConfirmarOk.click();

    }

    @E("^clico em 'Bloquear o roaming internacional'$")
    public void clicoEmBloquearORoamingInternacional() {
        vivoTravelPage.clicarBloquearRoaming();
        addScreenCapture(LogStatus.PASS, "Cliquei para bloquear o roaming");
        vivoTravelPage.esperaElementoFicarVisivel(vivoTravelPage.btnConfirmarOk);
        addScreenCapture(LogStatus.PASS, "Exibiu modal de confirmação");
        vivoTravelPage.btnConfirmarOk.click();

    }

    @E("^clico em 'Acesse o Guia de Bolso'$")
    public void clicoEmAcesseOGuiaDeBolso() {
        vivoTravelPage.clicarAcesseOGuiaBolso();
        addScreenCapture("Cliquei para acessar Guia Bolso");
    }

    @Entao("^verifico carregou corretamente o 'Guia de Bolso'$")
    public void verificoCarregouCorretamenteOGuiaDeBolso() {
        vivoTravelPage.validaTelaGuiaDeBolso();
        addScreenCapture("Acessei o Guia Bolso");
        vivoTravelPage.scrollDown();
        addScreenCapture("Acessei o Guia Bolso scroll");
    }

    @E("^clico em 'Fale com a Vivo no Exterior'$")
    public void clicoEmFaleComAVivoNoExterior() {
        vivoTravelPage.clicarFaleComAVivoNoExterior();
        addScreenCapture("Cliquei para acessar Fale Com a Vivo no Exterior");
    }

    @Entao("^verifico carregou corretamente o 'Fale com a Vivo no Exterior'$")
    public void verificoCarregouCorretamenteOFaleComAVivoNoExterior() {
        vivoTravelPage.validaTelaFaleComAVivoNoExterior();
        addScreenCapture("Acessei Fale Com a Vivo no Exterior");
    }

    @Entao("^deve ser apresentado o PDF com as informacoes da franquia internacional$")
    public void deveSerApresentadoOPDFComAsInformacoesDaFranquiaInternacional() {
        vivoTravelPage.validarBtnAdicionarAoDrive();
        addScreenCapture(LogStatus.PASS, "Exibe o PDF");

    }
}
