package api.model;

public class Pessoa {
    public String nomeCompleto;
    public String CPF;
    public String RG;
    public String orgaoEmissor;
    public String UF;
    public String telefoneContato;
    public String dataNascimento;
    public String email;
    public String nomeDaMae;
    public String senha;
    public String CEP;
    public String numeroEndereco;
    public String complementoEndereco;
    public String rua;
    public String bairro;
    public String cidade;
    public String novoNumeroLinha;
    public String diaVencimentoFatura;
    public String numeroPedido = "00892479";
    public String CEPEntrega;
    public String ruaEntrega;
    public String numeroEnderecoEntrega;
    public String bairroEntrega;
    public String cidadeEntrega;
}
