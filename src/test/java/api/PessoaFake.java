package api;

import api.model.Pessoa;
import api.utils.GerarCPF;
import com.github.javafaker.Faker;

import java.text.Normalizer;
import java.util.Locale;

public class PessoaFake {
    private static Pessoa pessoa;

    public static Pessoa getPessoa() {
        if (pessoa == null) {
            pessoa = getFaker();
            return pessoa;
        }
        return pessoa;
    }

    public static void newPessoa() {
        pessoa = getFaker();
    }

    private static String removeAcentos(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str;
    }

    private static String normalizaNome(String name) {
        return name.replace(".", "").replace("-", "");
    }

    private static Pessoa getFaker() {
        Faker faker = new Faker(new Locale("pt-BR"));
        GerarCPF gerarCPF = new GerarCPF();
        String nome = normalizaNome(faker.name().fullName());
        final Pessoa pessoa = new Pessoa();
        pessoa.nomeCompleto = nome;
        pessoa.CPF = gerarCPF.geraCPFFinal();
        pessoa.RG = "05889475208";
        pessoa.orgaoEmissor = "SSP";
        pessoa.UF = "SP";
        pessoa.dataNascimento = "01/01/1990";
        pessoa.nomeDaMae = normalizaNome(faker.name().fullName());
        pessoa.telefoneContato = "99 999999999";
        pessoa.senha = "135791";
        pessoa.email = removeAcentos(nome.trim().split(" ")[0]).concat("" + faker.random().nextDouble()).concat("@automacaodxc.com").toLowerCase();
        pessoa.CEP = "01010-000";
        pessoa.rua = "SAO BENTO";
        pessoa.numeroEndereco = faker.address().streetAddressNumber();
        pessoa.bairro = "CENTRO'";
        pessoa.cidade = "S PAULO";
        pessoa.CEPEntrega = "01010-010";
        pessoa.ruaEntrega = "ANTONIO PRADO";
        pessoa.numeroEnderecoEntrega = faker.address().streetAddressNumber();
        pessoa.bairroEntrega = "CENTRO'";
        pessoa.cidadeEntrega = "S PAULO";
        return pessoa;
    }

}
